import Vue from 'vue'
import router from './router'
import './plugins/axios'
import './plugins/element.js'
import App from './App.vue'
import store from './store'
import './assets/icon/iconfont.css'
import './assets/icon/iconfont.js'
import pageFooter from '@/components/pageFooter.vue'
import common from '@/components/common.vue'
import componentList from './components'
import vueSeamlessScroll from 'vue-seamless-scroll'
import versionTood from '@/assets/js/versionUpdate'

Vue.config.productionTip = false
Vue.component('pageFooter', pageFooter)
Vue.component('common', common)
Vue.use(vueSeamlessScroll)

componentList.forEach(item => {
  Vue.component(item.name, item.component)
})

router.beforeEach(async (to, from, next) => {
  versionTood.isNewVersion()
  next()
})

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
