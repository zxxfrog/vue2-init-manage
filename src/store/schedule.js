import * as mutation from './mutations.js'
import Util from '@/assets/js/util.js'
import Storage from '@/assets/js/storage'

const state = {
  totalScheduleList: {},
  reportData: [],
  rankData: [],
  // 单日赛程按照开赛时间排序
  dateScheduleByDate: {
    perform: [],
    intelligence: [],
    public: []
  },
  activeItem: {
    perform: [],
    intelligence: [],
    public: []
  },
  // 单日赛程按照项目排序
  dateScheduleByItem: {
    perform: {
      WE: []
    },
    intelligence: {
      GO: [],
      CC: [],
      CH: [],
      BR: [],
      GB: [],
      DR: [],
      WE: []
    },
    public: {
      PG: [],
      PC: [],
      PH: [],
      PR: [],
      PB: [],
      PD: [],
      PE: []
    }
  },
  scheduleByDiscipline: {
    perform: [],
    intelligence: [],
    public: []
  },
  dateDetailList: {
    perform: [],
    intelligence: [],
    public: []
  },
  itemAllList: {
    perform: [
      {
        id: '85',
        title: '掼牌（掼蛋）',
        subTitle: 'Date',
        '10-17': '',
        '10-18': '',
        '10-19': '',
        '10-20': '',
        date12: '淮安国缘宾馆'
      }
    ],
    intelligence: [
      {
        title: '围棋',
        id: '70',
        subTitle: 'Date',
        '10-25': '',
        '10-26': '',
        '10-27': '',
        '10-28': '',
        '10-29': '',
        '10-30': '',
        '10-31': '',
        '11-01': '',
        '11-02': '',
        '11-03': '',
        '11-04': '',
        date12: '合肥香格里拉大酒店'
      },
      {
        title: '象棋',
        id: '71',
        subTitle: 'Date',
        '10-25': '',
        '10-26': '',
        '10-27': '',
        '10-28': '',
        '10-29': '',
        '10-30': '',
        '10-31': '',
        '11-01': '',
        '11-02': '',
        '11-03': '',
        '11-04': '',
        date12: '合肥融创永乐半山酒店'
      },
      {
        title: '国际象棋',
        id: '72',
        subTitle: 'Date',
        '10-25': '',
        '10-26': '',
        '10-27': '',
        '10-28': '',
        '10-29': '',
        '10-30': '',
        '10-31': '',
        '11-01': '',
        '11-02': '',
        '11-03': '',
        '11-04': '',
        date12: '合肥天鹅湖大酒店'
      },
      {
        id: '73',
        title: '桥牌',
        subTitle: 'Date',
        '10-25': '',
        '10-26': '',
        '10-27': '',
        '10-28': '',
        '10-29': '',
        '10-30': '',
        '10-31': '',
        '11-01': '',
        '11-02': '',
        '11-03': '',
        '11-04': '',
        date12: '合肥丰大国际大酒店'
      },
      {
        id: '74',
        title: '五子棋',
        subTitle: 'Date',
        '10-25': '',
        '10-26': '',
        '10-27': '',
        '10-28': '',
        '10-29': '',
        '10-30': '',
        '10-31': '',
        '11-01': '',
        '11-02': '',
        '11-03': '',
        '11-04': '',
        date12: '合肥两淮豪生大酒店'
      },
      {
        id: '75',
        title: '国际跳棋',
        subTitle: 'Date',
        '10-25': '',
        '10-26': '',
        '10-27': '',
        '10-28': '',
        '10-29': '',
        '10-30': '',
        '10-31': '',
        '11-01': '',
        '11-02': '',
        '11-03': '',
        '11-04': '',
        date12: '合肥元一希尔顿酒店'
      }
    ],
    public: [
      {
        title: '围棋',
        id: '76',
        subTitle: 'Date',
        '10-26': '',
        '10-30': '',
        '10-31': '',
        '11-01': '',
        '11-02': '',
        '11-03': '',
        date12: '合肥香格里拉大酒店'
      },
      {
        title: '象棋',
        id: '77',
        subTitle: 'Date',
        '10-26': '',
        '10-30': '',
        '10-31': '',
        '11-01': '',
        '11-02': '',
        '11-03': '',
        date12: '合肥融创永乐半山酒店',
        date13: '合肥滨湖国际会展中心'
      },
      {
        title: '国际象棋',
        id: '78',
        subTitle: 'Date',
        '10-26': '',
        '10-30': '',
        '10-31': '',
        '11-01': '',
        '11-02': '',
        '11-03': '',
        date12: '合肥天鹅湖大酒店',
        date13: '合肥滨湖国际会展中心'
      },
      {
        id: '79',
        title: '桥牌',
        subTitle: 'Date',
        '10-26': '',
        '10-30': '',
        '10-31': '',
        '11-01': '',
        '11-02': '',
        '11-03': '',
        date12: '合肥丰大国际大酒店',
        date13: '合肥滨湖国际会展中心'
      },
      {
        id: '80',
        title: '五子棋',
        subTitle: 'Date',
        '10-26': '',
        '10-30': '',
        '10-31': '',
        '11-01': '',
        '11-02': '',
        '11-03': '',
        date12: '合肥两淮豪生大酒店',
        date13: '合肥滨湖国际会展中心'
      },
      {
        id: '81',
        title: '国际跳棋',
        subTitle: 'Date',
        '10-26': '',
        '10-30': '',
        '10-31': '',
        '11-01': '',
        '11-02': '',
        '11-03': '',
        date12: '合肥元一希尔顿酒店',
        date13: '合肥滨湖国际会展中心'
      },
      {
        id: '84',
        title: '掼牌（掼蛋）',
        subTitle: 'Date',
        '10-26': '',
        '10-30': '',
        '10-31': '',
        '11-01': '',
        '11-02': '',
        '11-03': '',
        date12: '合肥滨湖国际会展中心'
      }
    ]
  },
  itemScreenAllList: {
    perform: [
      {
        id: '85',
        title: '掼牌（掼蛋）',
        subTitle: 'Date',
        '10-17': '',
        '10-18': '',
        '10-19': '',
        '10-20': '',
        date12: '淮安国缘宾馆'
      }
    ],
    intelligence: [
      {
        title: '围棋',
        id: '70',
        subTitle: 'Date',
        '10-25': '',
        '10-26': '',
        '10-27': '',
        '10-28': '',
        '10-29': '',
        '10-30': '',
        '10-31': '',
        '11-01': '',
        '11-02': '',
        '11-03': '',
        '11-04': '',
        date12: '合肥香格里拉大酒店'
      },
      {
        title: '象棋',
        id: '71',
        subTitle: 'Date',
        '10-25': '',
        '10-26': '',
        '10-27': '',
        '10-28': '',
        '10-29': '',
        '10-30': '',
        '10-31': '',
        '11-01': '',
        '11-02': '',
        '11-03': '',
        '11-04': '',
        date12: '合肥融创永乐半山酒店'
      },
      {
        title: '国际象棋',
        id: '72',
        subTitle: 'Date',
        '10-25': '',
        '10-26': '',
        '10-27': '',
        '10-28': '',
        '10-29': '',
        '10-30': '',
        '10-31': '',
        '11-01': '',
        '11-02': '',
        '11-03': '',
        '11-04': '',
        date12: '合肥天鹅湖大酒店'
      },
      {
        id: '73',
        title: '桥牌',
        subTitle: 'Date',
        '10-25': '',
        '10-26': '',
        '10-27': '',
        '10-28': '',
        '10-29': '',
        '10-30': '',
        '10-31': '',
        '11-01': '',
        '11-02': '',
        '11-03': '',
        '11-04': '',
        date12: '合肥丰大国际大酒店'
      },
      {
        id: '74',
        title: '五子棋',
        subTitle: 'Date',
        '10-25': '',
        '10-26': '',
        '10-27': '',
        '10-28': '',
        '10-29': '',
        '10-30': '',
        '10-31': '',
        '11-01': '',
        '11-02': '',
        '11-03': '',
        '11-04': '',
        date12: '合肥两淮豪生大酒店'
      },
      {
        id: '75',
        title: '国际跳棋',
        subTitle: 'Date',
        '10-25': '',
        '10-26': '',
        '10-27': '',
        '10-28': '',
        '10-29': '',
        '10-30': '',
        '10-31': '',
        '11-01': '',
        '11-02': '',
        '11-03': '',
        '11-04': '',
        date12: '合肥元一希尔顿酒店'
      }
    ],
    public: [
      {
        title: '围棋',
        id: '76',
        subTitle: 'Date',
        '10-26': '',
        '10-30': '',
        '10-31': '',
        '11-01': '',
        '11-02': '',
        '11-03': '',
        date12: '合肥香格里拉大酒店'
      },
      {
        title: '象棋',
        id: '77',
        subTitle: 'Date',
        '10-26': '',
        '10-30': '',
        '10-31': '',
        '11-01': '',
        '11-02': '',
        '11-03': '',
        date12: '合肥融创永乐半山酒店',
        date13: '合肥滨湖国际会展中心'
      },
      {
        title: '国际象棋',
        id: '78',
        subTitle: 'Date',
        '10-26': '',
        '10-30': '',
        '10-31': '',
        '11-01': '',
        '11-02': '',
        '11-03': '',
        date12: '合肥天鹅湖大酒店',
        date13: '合肥滨湖国际会展中心'
      },
      {
        id: '79',
        title: '桥牌',
        subTitle: 'Date',
        '10-26': '',
        '10-30': '',
        '10-31': '',
        '11-01': '',
        '11-02': '',
        '11-03': '',
        date12: '合肥丰大国际大酒店',
        date13: '合肥滨湖国际会展中心'
      },
      {
        id: '80',
        title: '五子棋',
        subTitle: 'Date',
        '10-26': '',
        '10-30': '',
        '10-31': '',
        '11-01': '',
        '11-02': '',
        '11-03': '',
        date12: '合肥两淮豪生大酒店',
        date13: '合肥滨湖国际会展中心'
      },
      {
        id: '81',
        title: '国际跳棋',
        subTitle: 'Date',
        '10-26': '',
        '10-30': '',
        '10-31': '',
        '11-01': '',
        '11-02': '',
        '11-03': '',
        date12: '合肥元一希尔顿酒店',
        date13: '合肥滨湖国际会展中心'
      },
      {
        id: '84',
        title: '掼牌（掼蛋）',
        subTitle: 'Date',
        '10-26': '',
        '10-30': '',
        '10-31': '',
        '11-01': '',
        '11-02': '',
        '11-03': '',
        date12: '合肥滨湖国际会展中心'
      }
    ]
  },
  dailyGoldData: {
    perform: [],
    intelligence: [],
    public: []
  },
  screenData: {},
  screenSchduleData: {
    围棋: [],
    象棋: [],
    国际象棋: [],
    桥牌: [],
    五子棋: [],
    国际跳棋: [],
    '掼牌（掼蛋）': []
  }
}

const mutations = {
  //clear
  [mutation.GET_SCREEN_SCHDULE_CLEAR](state) {
    state.screenSchduleData = {
      围棋: [],
      象棋: [],
      国际象棋: [],
      桥牌: [],
      五子棋: [],
      国际跳棋: [],
      '掼牌（掼蛋）': []
    }
  },
  [mutation.GET_TOTAL_SCHEDULE_LIST_CLEAR](state) {
    state.totalScheduleList = {}
    state.itemAllList = {
      perform: [
        {
          id: '85',
          title: '掼牌（掼蛋）',
          subTitle: 'Date',
          '10-17': '',
          '10-18': '',
          '10-19': '',
          '10-20': '',
          date12: '淮安国缘宾馆'
        }
      ],
      intelligence: [
        {
          title: '围棋',
          id: '70',
          subTitle: 'Date',
          '10-25': '',
          '10-26': '',
          '10-27': '',
          '10-28': '',
          '10-29': '',
          '10-30': '',
          '10-31': '',
          '11-01': '',
          '11-02': '',
          '11-03': '',
          '11-04': '',
          date12: '合肥香格里拉大酒店'
        },
        {
          title: '象棋',
          id: '71',
          subTitle: 'Date',
          '10-25': '',
          '10-26': '',
          '10-27': '',
          '10-28': '',
          '10-29': '',
          '10-30': '',
          '10-31': '',
          '11-01': '',
          '11-02': '',
          '11-03': '',
          '11-04': '',
          date12: '合肥融创永乐半山酒店'
        },
        {
          title: '国际象棋',
          id: '72',
          subTitle: 'Date',
          '10-25': '',
          '10-26': '',
          '10-27': '',
          '10-28': '',
          '10-29': '',
          '10-30': '',
          '10-31': '',
          '11-01': '',
          '11-02': '',
          '11-03': '',
          '11-04': '',
          date12: '合肥天鹅湖大酒店'
        },
        {
          id: '73',
          title: '桥牌',
          subTitle: 'Date',
          '10-25': '',
          '10-26': '',
          '10-27': '',
          '10-28': '',
          '10-29': '',
          '10-30': '',
          '10-31': '',
          '11-01': '',
          '11-02': '',
          '11-03': '',
          '11-04': '',
          date12: '合肥丰大国际大酒店'
        },
        {
          id: '74',
          title: '五子棋',
          subTitle: 'Date',
          '10-25': '',
          '10-26': '',
          '10-27': '',
          '10-28': '',
          '10-29': '',
          '10-30': '',
          '10-31': '',
          '11-01': '',
          '11-02': '',
          '11-03': '',
          '11-04': '',
          date12: '合肥两淮豪生大酒店'
        },
        {
          id: '75',
          title: '国际跳棋',
          subTitle: 'Date',
          '10-25': '',
          '10-26': '',
          '10-27': '',
          '10-28': '',
          '10-29': '',
          '10-30': '',
          '10-31': '',
          '11-01': '',
          '11-02': '',
          '11-03': '',
          '11-04': '',
          date12: '合肥元一希尔顿酒店'
        }
      ],
      public: [
        {
          title: '围棋',
          id: '76',
          subTitle: 'Date',
          '10-26': '',
          '10-30': '',
          '10-31': '',
          '11-01': '',
          '11-02': '',
          '11-03': '',
          date12: '合肥香格里拉大酒店'
        },
        {
          title: '象棋',
          id: '77',
          subTitle: 'Date',
          '10-26': '',
          '10-30': '',
          '10-31': '',
          '11-01': '',
          '11-02': '',
          '11-03': '',
          date12: '合肥融创永乐半山酒店',
          date13: '合肥滨湖国际会展中心'
        },
        {
          title: '国际象棋',
          id: '78',
          subTitle: 'Date',
          '10-26': '',
          '10-30': '',
          '10-31': '',
          '11-01': '',
          '11-02': '',
          '11-03': '',
          date12: '合肥天鹅湖大酒店',
          date13: '合肥滨湖国际会展中心'
        },
        {
          id: '79',
          title: '桥牌',
          subTitle: 'Date',
          '10-26': '',
          '10-30': '',
          '10-31': '',
          '11-01': '',
          '11-02': '',
          '11-03': '',
          date12: '合肥丰大国际大酒店',
          date13: '合肥滨湖国际会展中心'
        },
        {
          id: '80',
          title: '五子棋',
          subTitle: 'Date',
          '10-26': '',
          '10-30': '',
          '10-31': '',
          '11-01': '',
          '11-02': '',
          '11-03': '',
          date12: '合肥两淮豪生大酒店',
          date13: '合肥滨湖国际会展中心'
        },
        {
          id: '81',
          title: '国际跳棋',
          subTitle: 'Date',
          '10-26': '',
          '10-30': '',
          '10-31': '',
          '11-01': '',
          '11-02': '',
          '11-03': '',
          date12: '合肥元一希尔顿酒店',
          date13: '合肥滨湖国际会展中心'
        },
        {
          id: '84',
          title: '掼牌（掼蛋）',
          subTitle: 'Date',
          '10-26': '',
          '10-30': '',
          '10-31': '',
          '11-01': '',
          '11-02': '',
          '11-03': '',
          date12: '合肥滨湖国际会展中心'
        }
      ]
    }
  },
  [mutation.GET_SCREEN_TOTAL_SCHEDULE_LIST_CLEAR](state) {
    state.itemScreenAllList = {
      perform: [
        {
          id: '85',
          title: '掼牌（掼蛋）',
          subTitle: 'Date',
          '10-17': '',
          '10-18': '',
          '10-19': '',
          '10-20': '',
          date12: '淮安国缘宾馆'
        }
      ],
      intelligence: [
        {
          title: '围棋',
          id: '70',
          subTitle: 'Date',
          '10-25': '',
          '10-26': '',
          '10-27': '',
          '10-28': '',
          '10-29': '',
          '10-30': '',
          '10-31': '',
          '11-01': '',
          '11-02': '',
          '11-03': '',
          '11-04': '',
          date12: '合肥香格里拉大酒店'
        },
        {
          title: '象棋',
          id: '71',
          subTitle: 'Date',
          '10-25': '',
          '10-26': '',
          '10-27': '',
          '10-28': '',
          '10-29': '',
          '10-30': '',
          '10-31': '',
          '11-01': '',
          '11-02': '',
          '11-03': '',
          '11-04': '',
          date12: '合肥融创永乐半山酒店'
        },
        {
          title: '国际象棋',
          id: '72',
          subTitle: 'Date',
          '10-25': '',
          '10-26': '',
          '10-27': '',
          '10-28': '',
          '10-29': '',
          '10-30': '',
          '10-31': '',
          '11-01': '',
          '11-02': '',
          '11-03': '',
          '11-04': '',
          date12: '合肥天鹅湖大酒店'
        },
        {
          id: '73',
          title: '桥牌',
          subTitle: 'Date',
          '10-25': '',
          '10-26': '',
          '10-27': '',
          '10-28': '',
          '10-29': '',
          '10-30': '',
          '10-31': '',
          '11-01': '',
          '11-02': '',
          '11-03': '',
          '11-04': '',
          date12: '合肥丰大国际大酒店'
        },
        {
          id: '74',
          title: '五子棋',
          subTitle: 'Date',
          '10-25': '',
          '10-26': '',
          '10-27': '',
          '10-28': '',
          '10-29': '',
          '10-30': '',
          '10-31': '',
          '11-01': '',
          '11-02': '',
          '11-03': '',
          '11-04': '',
          date12: '合肥两淮豪生大酒店'
        },
        {
          id: '75',
          title: '国际跳棋',
          subTitle: 'Date',
          '10-25': '',
          '10-26': '',
          '10-27': '',
          '10-28': '',
          '10-29': '',
          '10-30': '',
          '10-31': '',
          '11-01': '',
          '11-02': '',
          '11-03': '',
          '11-04': '',
          date12: '合肥元一希尔顿酒店'
        }
      ],
      public: [
        {
          title: '围棋',
          id: '76',
          subTitle: 'Date',
          '10-26': '',
          '10-30': '',
          '10-31': '',
          '11-01': '',
          '11-02': '',
          '11-03': '',
          date12: '合肥香格里拉大酒店'
        },
        {
          title: '象棋',
          id: '77',
          subTitle: 'Date',
          '10-26': '',
          '10-30': '',
          '10-31': '',
          '11-01': '',
          '11-02': '',
          '11-03': '',
          date12: '合肥融创永乐半山酒店',
          date13: '合肥滨湖国际会展中心'
        },
        {
          title: '国际象棋',
          id: '78',
          subTitle: 'Date',
          '10-26': '',
          '10-30': '',
          '10-31': '',
          '11-01': '',
          '11-02': '',
          '11-03': '',
          date12: '合肥天鹅湖大酒店',
          date13: '合肥滨湖国际会展中心'
        },
        {
          id: '79',
          title: '桥牌',
          subTitle: 'Date',
          '10-26': '',
          '10-30': '',
          '10-31': '',
          '11-01': '',
          '11-02': '',
          '11-03': '',
          date12: '合肥丰大国际大酒店',
          date13: '合肥滨湖国际会展中心'
        },
        {
          id: '80',
          title: '五子棋',
          subTitle: 'Date',
          '10-26': '',
          '10-30': '',
          '10-31': '',
          '11-01': '',
          '11-02': '',
          '11-03': '',
          date12: '合肥两淮豪生大酒店',
          date13: '合肥滨湖国际会展中心'
        },
        {
          id: '81',
          title: '国际跳棋',
          subTitle: 'Date',
          '10-26': '',
          '10-30': '',
          '10-31': '',
          '11-01': '',
          '11-02': '',
          '11-03': '',
          date12: '合肥元一希尔顿酒店',
          date13: '合肥滨湖国际会展中心'
        },
        {
          id: '84',
          title: '掼牌（掼蛋）',
          subTitle: 'Date',
          '10-26': '',
          '10-30': '',
          '10-31': '',
          '11-01': '',
          '11-02': '',
          '11-03': '',
          date12: '合肥滨湖国际会展中心'
        }
      ]
    }
  },
  [mutation.GET_REPORT_DATA_CLEAR](state) {
    state.reportData = []
  },
  [mutation.GET_RANK_DATA_CLEAR](state) {
    state.rankData = []
  },
  [mutation.GET_SINGLE_DATE_SCHEDULE_CLEAR](state) {
    state.dateScheduleByDate = {
      perform: [],
      intelligence: [],
      public: []
    }
    state.dateScheduleByItem = {
      intelligence: {
        GO: [],
        CC: [],
        CH: [],
        BR: [],
        GB: [],
        DR: [],
        WE: []
      },
      public: {
        PG: [],
        PC: [],
        PH: [],
        PR: [],
        PB: [],
        PD: [],
        PE: []
      },
      perform: {
        GO: [],
        CC: [],
        CH: [],
        BR: [],
        GB: [],
        DR: [],
        WE: []
      }
    }
    state.activeItem = {
      perform: ['掼牌（掼蛋）'],
      intelligence: [],
      public: []
    }
  },
  [mutation.GET_SCHEDULE_BY_DISCIPLINE_CLEAR](state) {
    state.scheduleByDiscipline = {
      perform: [],
      intelligence: [],
      public: []
    }
    state.dateDetailList = {
      perform: [],
      intelligence: [],
      public: []
    }
  },
  [mutation.GET_DAILY_GOLD_DATA_CLEAR](state) {
    state.dailyGoldData = {
      perform: [],
      intelligence: [],
      public: []
    }
  },
  //
  [mutation.GET_CURRENT_RESULT_CLEAR](state) {
    state.screenData = {}
  },

  //request
  [mutation.GET_TOTAL_SCHEDULE_LIST_REQUEST](state, payload) {
    if (!Util.isEmpty(payload)) {
      state.totalScheduleList = payload.Message
      let allItemGame = payload.Message.GameDateOfDiscipline
      let sportType = Storage.get('activeGroup') || 'intelligence'
      if (sportType === 'intelligence') {
        allItemGame.forEach(game => {
          if (game.SportType === '正式比赛') {
            state.itemAllList.intelligence.forEach(item => {
              if (item.id === game.SportID) {
                if (game.DisciplinePassGold === '0' && game.DisciplineGold === '0') {
                  item[game.SportDate] = '0'
                } else {
                  item[game.SportDate] = game.DisciplinePassGold + '/' + game.DisciplineGold
                }
              }
            })
          }
        })
      } else if (sportType === 'public') {
        allItemGame.forEach(game => {
          if (game.SportType === '大众公开组') {
            state.itemAllList.public.forEach(item => {
              if (item.id === game.SportID) {
                if (game.DisciplinePassGold === '0' && game.DisciplineGold === '0') {
                  item[game.SportDate] = '0'
                } else {
                  item[game.SportDate] = game.DisciplinePassGold + '/' + game.DisciplineGold
                }
              }
            })
          }
        })
      } else if (sportType === 'perform') {
        allItemGame.forEach(game => {
          if (game.SportType === '表演项目') {
            state.itemAllList.perform.forEach(item => {
              if (item.id === game.SportID) {
                if (game.DisciplinePassGold === '0' && game.DisciplineGold === '0') {
                  item[game.SportDate] = '0'
                } else {
                  item[game.SportDate] = game.DisciplinePassGold + '/' + game.DisciplineGold
                }
              }
            })
          }
        })
      }
    }
  },
  [mutation.GET_SCREEN_TOTAL_SCHEDULE_LIST_REQUEST](state, payload) {
    if (!Util.isEmpty(payload)) {
      state.totalScheduleList = payload.Message
      let allItemGame = payload.Message.GameDateOfDiscipline
      let sportType = Storage.get('screenActiveGroup') || 'intelligence'
      if (sportType === 'intelligence') {
        allItemGame.forEach(game => {
          if (game.SportType === '正式比赛') {
            state.itemScreenAllList.intelligence.forEach(item => {
              if (item.id === game.SportID) {
                if (game.DisciplinePassGold === '0' && game.DisciplineGold === '0') {
                  item[game.SportDate] = '0'
                } else {
                  item[game.SportDate] = game.DisciplinePassGold + '/' + game.DisciplineGold
                }
              }
            })
          }
        })
      } else if (sportType === 'public') {
        allItemGame.forEach(game => {
          if (game.SportType === '大众公开组') {
            state.itemScreenAllList.public.forEach(item => {
              if (item.id === game.SportID) {
                if (game.DisciplinePassGold === '0' && game.DisciplineGold === '0') {
                  item[game.SportDate] = '0'
                } else {
                  item[game.SportDate] = game.DisciplinePassGold + '/' + game.DisciplineGold
                }
              }
            })
          }
        })
      } else if (sportType === 'perform') {
        allItemGame.forEach(game => {
          if (game.SportType === '表演项目') {
            state.itemScreenAllList.perform.forEach(item => {
              if (item.id === game.SportID) {
                if (game.DisciplinePassGold === '0' && game.DisciplineGold === '0') {
                  item[game.SportDate] = '0'
                } else {
                  item[game.SportDate] = game.DisciplinePassGold + '/' + game.DisciplineGold
                }
              }
            })
          }
        })
      }
    }
  },
  [mutation.GET_REPORT_DATA_REQUEST](state, payload) {
    if (!Util.isEmpty(payload)) {
      state.reportData = payload.Data
    }
  },
  [mutation.GET_RANK_DATA_REQUEST](state, payload) {
    if (!Util.isEmpty(payload)) {
      payload.Message.forEach(item => {
        item.AthleteName = item.AthleteList.map(data => {
          return data['F_Name']
        }).join('\xa0\xa0\xa0\xa0')
      })
      state.rankData = payload.Message
    }
  },
  [mutation.GET_SINGLE_DATE_SCHEDULE_REQUEST](state, payload) {
    if (!Util.isEmpty(payload)) {
      let allData = payload.Message.SingleDateProtocalMain
      let allFile = payload.Message.SingleDateFile
      allData.forEach(item => {
        if (item.SportType === '五智会' || item.SportType === '正式比赛') {
          let itemList = ['GO', 'CC', 'CH', 'BR', 'GB', 'DR', 'WE']
          if (!state.activeItem.intelligence.includes(item.SportName)) {
            state.activeItem.intelligence.push(item.SportName)
          }
          itemList.forEach(col => {
            let filesByItem = []
            if (col === item.SportCode) {
              allFile.forEach(row => {
                if (item.PID === row.PID) {
                  filesByItem.push(row)
                }
              })
              state.dateScheduleByItem.intelligence[col].push({
                PID: item.PID,
                SportType: item.SportType,
                SportName: item.SportName,
                SportItemCode: item.SportItemCode,
                SportItemName: item.SportItemName,
                GameName: item.GameName,
                BeginTime: item.BeginTime,
                VenueName: item.VenueName,
                BeginDate: item.BeginDate,
                files: filesByItem
              })
            }
          })
          let files = []
          allFile.forEach(row => {
            if (item.PID === row.PID) {
              files.push(row)
            }
          })
          state.dateScheduleByDate.intelligence.push({
            PID: item.PID,
            SportType: item.SportType,
            SportName: item.SportName,
            SportItemCode: item.SportItemCode,
            SportItemName: item.SportItemName,
            GameName: item.GameName,
            BeginTime: item.BeginTime,
            VenueName: item.VenueName,
            BeginDate: item.BeginDate,
            files: files
          })
        } else if (item.SportType === '大众公开组') {
          if (!state.activeItem.public.includes(item.SportName)) {
            state.activeItem.public.push(item.SportName)
          }
          let itemList = ['PG', 'PC', 'PH', 'PR', 'PB', 'PD', 'PE']
          itemList.forEach(col => {
            let filesByItem = []
            if (col === item.SportCode) {
              allFile.forEach(row => {
                if (item.PID === row.PID) {
                  filesByItem.push(row)
                }
              })
              state.dateScheduleByItem.public[col].push({
                PID: item.PID,
                SportType: item.SportType,
                SportName: item.SportName,
                SportItemCode: item.SportItemCode,
                SportItemName: item.SportItemName,
                GameName: item.GameName,
                BeginTime: item.BeginTime,
                VenueName: item.VenueName,
                BeginDate: item.BeginDate,
                files: filesByItem
              })
            }
          })
          let files = []
          allFile.forEach(row => {
            if (item.PID === row.PID) {
              files.push(row)
            }
          })
          state.dateScheduleByDate.public.push({
            PID: item.PID,
            SportType: item.SportType,
            SportName: item.SportName,
            SportItemCode: item.SportItemCode,
            SportItemName: item.SportItemName,
            GameName: item.GameName,
            BeginTime: item.BeginTime,
            VenueName: item.VenueName,
            BeginDate: item.BeginDate,
            files: files
          })
        } else if (item.SportType === '表演项目') {
          let itemList = ['WE']
          itemList.forEach(col => {
            let filesByItem = []
            if (col === item.SportCode) {
              console.log(allFile)
              allFile.forEach(row => {
                if (item.PID === row.PID) {
                  filesByItem.push(row)
                }
              })
              state.dateScheduleByItem.perform[col].push({
                PID: item.PID,
                SportType: item.SportType,
                SportName: item.SportName,
                SportItemCode: item.SportItemCode,
                SportItemName: item.SportItemName,
                GameName: item.GameName,
                BeginTime: item.BeginTime,
                VenueName: item.VenueName,
                BeginDate: item.BeginDate,
                files: filesByItem
              })
              console.log(state.dateScheduleByItem.perform)
            }
          })
          let files = []
          allFile.forEach(row => {
            if (item.PID === row.PID) {
              files.push(row)
            }
          })
          state.dateScheduleByDate.perform.push({
            PID: item.PID,
            SportType: item.SportType,
            SportName: item.SportName,
            SportItemCode: item.SportItemCode,
            SportItemName: item.SportItemName,
            GameName: item.GameName,
            BeginTime: item.BeginTime,
            VenueName: item.VenueName,
            BeginDate: item.BeginDate,
            files: files
          })
        }
      })
    }
  },
  [mutation.GET_SCHEDULE_BY_DISCIPLINE_REQUEST](state, payload) {
    if (!Util.isEmpty(payload)) {
      let allData = payload.Message.SingleDisciplineProtocalMain
      let allFile = payload.Message.SingleDisciplineFile
      allData.forEach(item => {
        if (item.SportType === '五智会' || item.SportType === '正式比赛') {
          if (!state.dateDetailList.intelligence.includes(item.BeginDate)) {
            state.dateDetailList.intelligence.push(item.BeginDate)
          }
          let files = []
          allFile.forEach(row => {
            if (item.PID === row.PID) {
              files.push(row)
            }
          })

          state.scheduleByDiscipline.intelligence.push({
            PID: item.PID,
            SportType: item.SportType,
            SportName: item.SportName,
            SportItemCode: item.SportItemCode,
            SportItemName: item.SportItemName,
            GameName: item.GameName,
            BeginTime: item.BeginTime,
            StartTime: item.StartTime,
            VenueName: item.VenueName,
            BeginDate: item.BeginDate,
            files: files
          })
        } else if (item.SportType === '大众公开组') {
          if (!state.dateDetailList.public.includes(item.BeginDate)) {
            state.dateDetailList.public.push(item.BeginDate)
          }
          let files = []
          allFile.forEach(row => {
            if (item.PID === row.PID) {
              files.push(row)
            }
          })
          state.scheduleByDiscipline.public.push({
            PID: item.PID,
            SportType: item.SportType,
            SportName: item.SportName,
            SportItemCode: item.SportItemCode,
            SportItemName: item.SportItemName,
            GameName: item.GameName,
            BeginTime: item.BeginTime,
            StartTime: item.StartTime,
            VenueName: item.VenueName,
            BeginDate: item.BeginDate,
            files: files
          })
        } else if (item.SportType === '表演项目') {
          if (!state.dateDetailList.perform.includes(item.BeginDate)) {
            state.dateDetailList.perform.push(item.BeginDate)
          }
          let files = []
          allFile.forEach(row => {
            if (item.PID === row.PID) {
              files.push(row)
            }
          })
          state.scheduleByDiscipline.perform.push({
            PID: item.PID,
            SportType: item.SportType,
            SportName: item.SportName,
            SportItemCode: item.SportItemCode,
            SportItemName: item.SportItemName,
            GameName: item.GameName,
            BeginTime: item.BeginTime,
            StartTime: item.StartTime,
            VenueName: item.VenueName,
            BeginDate: item.BeginDate,
            files: files
          })
        }
      })
    }
  },
  [mutation.GET_DAILY_GOLD_DATA_REQUEST](state, payload) {
    if (!Util.isEmpty(payload)) {
      let dateGame = payload.Message.AllGameDate
      state.dailyGoldData = {
        perform: [],
        intelligence: [],
        public: []
      }
      dateGame.forEach(item => {
        if (item.SportType === '正式比赛' && item.Month !== '') {
          let Month = item.Month === 'Oc' ? '10' : '11'
          state.dailyGoldData.intelligence.push({
            PassGold: item.PassGold,
            Gold: item.Gold,
            Date: Month + '-' + item.Date
          })
        } else if (item.SportType === '大众公开组' && item.Month !== '') {
          let Month = item.Month === 'Oc' ? '10' : '11'
          state.dailyGoldData.public.push({
            PassGold: item.PassGold,
            Gold: item.Gold,
            Date: Month + '-' + item.Date
          })
        } else if (item.SportType === '表演项目' && item.Month !== '') {
          let Month = item.Month === 'Oc' ? '10' : '11'
          state.dailyGoldData.perform.push({
            PassGold: item.PassGold,
            Gold: item.Gold,
            Date: Month + '-' + item.Date
          })
        }
      })
    }
  },
  [mutation.GET_CURRENT_RESULT_REQUEST](state, payload) {
    let itemInfoList = {
      围棋: [
        {
          name: '围棋',
          logo: require('@/assets/img/daily/GO.png'),
          flag: true
        }
      ],
      象棋: [
        {
          name: '象棋',
          logo: require('@/assets/img/daily/CC.png'),
          flag: false
        }
      ],
      国际象棋: [
        {
          name: '国际象棋',
          logo: require('@/assets/img/daily/CH.png'),
          flag: false
        }
      ],
      桥牌: [
        {
          name: '桥牌',
          logo: require('@/assets/img/daily/BR.png'),
          flag: false
        }
      ],
      五子棋: [
        {
          name: '五子棋',
          logo: require('@/assets/img/daily/GB.png'),
          flag: false
        }
      ],
      国际跳棋: [
        {
          name: '国际跳棋',
          logo: require('@/assets/img/daily/DR.png'),
          flag: false
        }
      ]
    }
    let currentIndex = Storage.get('currentIndex') || 0
    if (!Util.isEmpty(payload)) {
      if (payload.CurrentResult.length <= currentIndex) {
        currentIndex = 0
      }
      let result = payload.CurrentResult[currentIndex]
      state.screenData = {
        gameTitle: result.SportItemName + ' ' + result.UnitName,
        gameTime: result.Date + ' ' + result.Time,
        reportName: result.ReportName,
        venueName: result.Venue,
        sportName: result.SportName,
        sportCode: result.SportCode,
        showItemInfo: itemInfoList[result.SportName],
        fileName: result.FileName
      }
    }
    Storage.set('currentIndex', currentIndex + 1)
  },
  [mutation.GET_SCREEN_SCHDULE_REQUEST](state, payload) {
    if (!Util.isEmpty(payload)) {
      let allData = payload.Message.SingleDateProtocalMain
      allData.forEach(item => {
        if (item.SportType === '正式比赛') {
          state.screenSchduleData[item.SportName].push(item)
        }
      })
    }
  }
}

const actions = {
  getTotalScheduleList({ commit }, params) {
    return Util.getAction(
      'InfoJson/Schedule/TotalSchedule/Data.json',
      commit,
      mutation.GET_TOTAL_SCHEDULE_LIST_CLEAR,
      mutation.GET_TOTAL_SCHEDULE_LIST_REQUEST
    )
  },
  getReportData({ commit }, params) {
    return Util.getAction(
      'Json/' + params,
      commit,
      mutation.GET_REPORT_DATA_CLEAR,
      mutation.GET_REPORT_DATA_REQUEST
    )
  },
  getRankData({ commit }, params) {
    return Util.getAction(
      'infoJson/Ranking/' + params,
      commit,
      mutation.GET_RANK_DATA_CLEAR,
      mutation.GET_RANK_DATA_REQUEST
    )
  },
  getSingleDateSchedule({ commit }, params) {
    return Util.getAction(
      'infoJson/Schedule/SingleDateSchedule/' + params,
      commit,
      mutation.GET_SINGLE_DATE_SCHEDULE_CLEAR,
      mutation.GET_SINGLE_DATE_SCHEDULE_REQUEST
    )
  },
  getScheduleByDiscipline({ commit }, params) {
    return Util.getAction(
      'infoJson/Schedule/SingleDisciplineSchedule/' + params,
      commit,
      mutation.GET_SCHEDULE_BY_DISCIPLINE_CLEAR,
      mutation.GET_SCHEDULE_BY_DISCIPLINE_REQUEST
    )
  },
  getDailyGoldData({ commit }, params) {
    return Util.getAction(
      'InfoJson/Schedule/TotalSchedule/Data.json',
      commit,
      mutation.GET_DAILY_GOLD_DATA_CLEAR,
      mutation.GET_DAILY_GOLD_DATA_REQUEST
    )
  },
  getScreenData({ commit }, params) {
    return Util.getAction(
      'InfoJson/Schedule/CurrentResult/Data.json',
      commit,
      mutation.GET_CURRENT_RESULT_CLEAR,
      mutation.GET_CURRENT_RESULT_REQUEST
    )
  },
  getScreenSchduleData({ commit }, params) {
    return Util.getAction(
      'infoJson/Schedule/SingleDateSchedule/' + params,
      commit,
      mutation.GET_SCREEN_SCHDULE_CLEAR,
      mutation.GET_SCREEN_SCHDULE_REQUEST
    )
  },
  getScreenTotalScheduleList({ commit }, params) {
    return Util.getAction(
      'InfoJson/Schedule/TotalSchedule/Data.json',
      commit,
      mutation.GET_SCREEN_TOTAL_SCHEDULE_LIST_CLEAR,
      mutation.GET_SCREEN_TOTAL_SCHEDULE_LIST_REQUEST
    )
  }
}

export default {
  state,
  mutations,
  actions
}
