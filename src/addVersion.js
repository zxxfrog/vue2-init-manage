/**
 * npm run build打包前执行此段代码
 *
 * npm run test打包前执行此段代码
 */

const fs = require('fs')

//返回version的json数据
function getVersionJson() {
  //fs读取文件
  let data = fs.readFileSync('./public/static/version.json')
  //转换为json对象
  return JSON.parse(data)
}
//获取version的json
let versionData = getVersionJson()
console.log(versionData, 'versionData+++++++++++++++++++')
//  使用当前时刻的时间搓生成永远不会重复的id
versionData.version = `wezone-${Date.now().toString(36)}`
//用versionData覆盖version.json内容
fs.writeFile('./public/static/version.json', JSON.stringify(versionData, null, '\t'), err => {})
