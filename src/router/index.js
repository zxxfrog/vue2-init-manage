import Vue from 'vue'
import VueRouter from 'vue-router'
import Storage from '@/assets/js/storage.js'

Vue.use(VueRouter)
const originalPush = VueRouter.prototype.push
VueRouter.prototype.push = function push(location) {
  return originalPush.call(this, location).catch(err => err)
}

const routes = [
  {
    path: '/',
    component: resolve => require(['../views/layout.vue'], resolve),
    redirect: { name: 'overview' },
    children: [
      // 总日程
      {
        path: 'overview',
        name: 'overview',
        component: resolve => require(['../views/scheduel/overview.vue'], resolve)
      },

      // 代表团成绩
      {
        name: 'daily',
        path: 'daily',
        component: resolve => require(['../views/scheduel/daily.vue'], resolve)
      },

      // 单项赛程
      {
        path: 'item',
        component: resolve => require(['../views/scheduel/child.vue'], resolve),
        children: [
          {
            name: 'itemDetail',
            path: '',
            component: resolve => require(['../views/scheduel/item.vue'], resolve)
          },
          {
            name: 'itemReport',
            path: 'report',
            component: resolve => require(['../views/scheduel/report.vue'], resolve)
          }
        ]
      }
    ]
  },
  {
    path: '/screen',
    component: resolve => require(['../views/child.vue'], resolve),
    redirect: { name: 'screenHome' },
    children: [
      {
        path: 'home',
        name: 'screenHome',
        component: resolve => require(['../views/screen/home.vue'], resolve)
      },
      {
        path: 'code',
        name: 'screenCode',
        component: resolve => require(['../views/screen/code.vue'], resolve)
      },
      // {
      //   path: 'result',
      //   name: 'screenResult',
      //   component: resolve => require(['../views/screen/screen.vue'], resolve)
      // },
      {
        path: 'schedule',
        name: 'screenSchedule',
        component: resolve => require(['../views/screen/schedule.vue'], resolve)
      },
      {
        path: 'overview',
        name: 'screenOverview',
        component: resolve => require(['../views/screen/overview.vue'], resolve)
      }
    ]
  },
  {
    path: '/error',
    component: resolve => require(['../views/500.vue'], resolve)
  },
  {
    path: '*',
    component: resolve => require(['../views/404.vue'], resolve)
  }
]

const BASE_URL = process.env.VUE_APP_BASE_URL

//配置跳转
const router = new VueRouter({
  mode: 'history',
  base: BASE_URL,
  routes,
  scrollBehavior(to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition
    } else {
      return { x: 0, y: 0 }
    }
  }
})

router.beforeEach((to, from, next) => {
  if (to.path.includes('/item/') && !to.path.includes('/item/report')) {
    router.push({
      path: '/item',
      query: to.query
    })
  }
  next()
})

router.afterEach((to, from, next) => {
  document.querySelector('#app').scrollTop = 0
})

export default router
