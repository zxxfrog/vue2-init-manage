/* eslint-disable */
Date.prototype.format = function (fmt) { //author: meizz
  let o = {
    'M+': this.getMonth() + 1, //月份
    'd+': this.getDate(), //日
    'h+': this.getHours(), //小时
    'm+': this.getMinutes(), //分
    's+': this.getSeconds(), //秒
    'q+': Math.floor((this.getMonth() + 3) / 3), //季度
    'S': this.getMilliseconds() //毫秒
  }
  if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (this.getFullYear() + '').substr(4 - RegExp.$1.length))
  for (let k in o)
    if (new RegExp('(' + k + ')').test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length === 1) ? (o[k]) : (('00' + o[k]).substr(('' + o[k]).length)))
  return fmt
}

export default {
  date: function (value) {
    if (typeof value === 'undefined' || value === null) return value
    return new Date(value).format('yyyy-MM-dd')
  },
  secondToDate: function (value) {
    if (typeof value === 'undefined' || value === null) return value
    let theTime = parseInt(value);// 秒  
    let theTime1 = 0;// 分  
    let theTime2 = 0;// 小时  
    if(theTime > 60) {  
        theTime1 = parseInt(theTime/60);  
        theTime = parseInt(theTime%60);  
        if(theTime1 > 60) {  
            theTime2 = parseInt(theTime1/60);  
            theTime1 = parseInt(theTime1%60);  
        }  
    }  
    return parseInt(theTime2)+':'+(parseInt(theTime1)<10?'0'+parseInt(theTime1): parseInt(theTime1))+':' +(parseInt(theTime1)<10?'0'+parseInt(theTime): parseInt(theTime));  
  },
  dateHourMinute: function (value) {
    if (typeof value === 'undefined' || value === null) return value
    return new Date(value).format('hh:mm')
  },
  dateTime: function (value) {
    if (typeof value === 'undefined' || value === null) return value
    return new Date(value).format('yyyy-MM-dd hh:mm:ss')
  },
  stampTime: function (value) {
    if (typeof value === 'undefined' || value === null) return value
    return new Date(value * 1000).format('yyyy-MM-dd hh:mm:ss')
  },
  dateToStamp: function (value) {
    if (typeof value === 'undefined' || value === null) return value
    return Date.parse(value) / 1000
  },
  formatToCurrentDate: function(value) {
    if (typeof value === 'undefined' || value === null) return value
    let date = new Date().format('yyyy-MM-dd')
    return new Date(date + ' ' + value +':00')
  },
  strToTimeStamp: function(value){
    if (typeof value === 'undefined' || value === null) return value
    return Date.parse(value)
  }
}
