export default {
  RANK: [
    { prop: 'F_Rank', label: '名次' },
    { prop: 'DelegationName', label: '单位' },
    { prop: 'AthleteName', label: '运动员' },
    { prop: 'F_Description', label: '备注' }
  ],

  // 围棋
  MSGO01: [
    { prop: 'TABLE', label: '台号', minWidth: '150' },
    {
      prop: 'NUMBER_B',
      label: '编号',
      headerAlign: 'center',
      align: 'center'
    },
    {
      prop: 'NAME_B',
      label: '姓名',
      headerAlign: 'center',
      align: 'center'
    },
    {
      prop: 'UNIT_B',
      label: '单位',
      headerAlign: 'center',
      align: 'center'
    },
    { prop: 'SCORE_B', key: 'e', label: '积分' },
    {
      prop: '',
      label: '',
      minWidth: '150',
      headerAlign: 'center',
      align: 'center'
    },
    {
      prop: 'NUMBER_W',
      label: '编号',
      headerAlign: 'center',
      align: 'center'
    },
    {
      prop: 'NAME_W',
      label: '姓名',
      headerAlign: 'center',
      align: 'center'
    },
    {
      prop: 'UNIT_W',
      label: '单位',
      headerAlign: 'center',
      align: 'center'
    },
    { prop: 'SCORE_W', key: 'i', label: '积分' }
  ],
  MSGO02: [
    { prop: 'GROUP', key: 'a', label: '组别', minWidth: '75' },
    { prop: 'TABLE', key: 'a1', label: '台号', minWidth: '75' },
    { prop: 'NUMBER_B', key: 'b', label: '编号' },
    { prop: 'NAME_B', key: 'c', label: '姓名' },
    { prop: 'UNIT_B', key: 'd', label: '单位' },
    { prop: 'SCORE_B', key: 'e', label: '积分' },
    { prop: '', label: '', minWidth: '150' },
    { prop: 'NUMBER_W', key: 'f', label: '编号' },
    { prop: 'NAME_W', key: 'g', label: '姓名' },
    { prop: 'UNIT_W', key: 'h', label: '单位' },
    { prop: 'SCORE_W', key: 'i', label: '积分' }
  ],
  MSGO03: [
    { prop: 'TABLE', key: 'a', label: '台号', minWidth: '150' },
    { prop: 'NUMBER_B', key: 'b', label: '编号' },
    { prop: 'UNIT_B', key: 'c', label: '单位/姓名' },
    { prop: 'SCORE_B', key: 'e', label: '积分' },
    { prop: '', label: '', minWidth: '150' },
    { prop: 'NUMBER_W', key: 'f', label: '编号' },
    { prop: 'UNIT_W', key: 'g', label: '单位/姓名' },
    { prop: 'SCORE_W', key: 'i', label: '积分' }
  ],
  MSGO08: [
    { prop: 'F_MatchName', key: 'a', label: '台号', minWidth: '80' },
    { prop: 'F_NameA', key: 'c', label: '姓名' },
    { prop: 'F_DelegationA', key: 'd', label: '单位' },
    { prop: '', label: '', minWidth: '150' },
    { prop: 'F_NameB', key: 'g', label: '姓名' },
    { prop: 'F_DelegationB', key: 'h', label: '单位' }
  ],
  MRGO01: [
    { prop: 'GROUP', key: 'a', label: '组别', minWidth: '150' },
    { prop: 'NUMBER_B', key: 'b', label: '编号' },
    { prop: 'NAME_B', key: 'c', label: '姓名' },
    { prop: 'UNIT_B', key: 'd', label: '单位' },
    { prop: 'SCORE_B', key: 'e', label: '积分' },
    { prop: 'RESULT_B', key: 'e1', label: '场分', minWidth: '90' },
    { prop: 'RESULT_W', key: 'e2', label: '场分', minWidth: '90' },
    { prop: 'SCORE_W', key: 'i', label: '积分' },
    { prop: 'UNIT_W', key: 'h', label: '单位' },
    { prop: 'NAME_W', key: 'g', label: '姓名' },
    { prop: 'NUMBER_W', key: 'f', label: '编号' }
  ],
  MRGO02: [
    { prop: 'GROUP', key: 'a', label: '组别', minWidth: '75' },
    { prop: 'TABLE', key: 'a1', label: '台号', minWidth: '75' },
    { prop: 'NUMBER_B', key: 'b', label: '编号' },
    { prop: 'NAME_B', key: 'c', label: '姓名' },
    { prop: 'UNIT_B', key: 'd', label: '单位' },
    { prop: 'SCORE_B', key: 'e', label: '积分' },
    { prop: 'RESULT_B', key: 'e1', label: '场分', minWidth: '90' },
    { prop: 'RESULT_W', key: 'e2', label: '场分', minWidth: '90' },
    { prop: 'SCORE_W', key: 'i', label: '积分' },
    { prop: 'UNIT_W', key: 'h', label: '单位' },
    { prop: 'NAME_W', key: 'g', label: '姓名' },
    { prop: 'NUMBER_W', key: 'f', label: '编号' }
  ],
  MRGO03: [
    { prop: 'GROUP', key: 'a', label: '组别', minWidth: '150' },
    { prop: 'NUMBER_B', key: 'b', label: '编号' },
    { prop: 'UNIT_B', key: 'd', label: '单位/姓名' },
    { prop: 'SCORE_B', key: 'e', label: '积分' },
    { prop: 'RESULT_B', key: 'e1', label: '场分', minWidth: '90' },
    { prop: 'RESULT_W', key: 'e2', label: '场分', minWidth: '90' },
    { prop: 'SCORE_W', key: 'i', label: '积分' },
    { prop: 'UNIT_W', key: 'h', label: '单位/姓名' },
    { prop: 'NUMBER_W', key: 'f', label: '编号' }
  ],
  MRGO08: [
    { prop: 'F_MatchName', key: 'a', label: '台号' },
    { prop: 'F_NameA', key: 'b', label: 'A方姓名' },
    { prop: 'F_DelegationA', key: 'c', label: 'B方单位' },
    { prop: 'F_NameB', key: 'd', label: 'A方姓名' },
    { prop: 'F_DelegationB', key: 'e', label: 'B方单位' },
    // { prop: 'F_ResultA', key: 'f', label: '黑棋成绩' },
    // { prop: 'F_ResultB', key: 'g', label: '白棋成绩' },
    { prop: 'F_Result', key: 'h', label: '比赛结果' }
    // { prop: 'F_Winner', key: 'a', label: '胜方' },
    // { prop: 'F_Loser', key: 'i', label: '败方' }
  ],
  MRGO05: [
    { prop: 'NUMBER', key: 'a', label: '编号', width: '80' },
    { prop: 'NAME', key: 'b', label: '姓名' },
    { prop: 'TEAM', key: 'c', label: '单位' },
    { prop: 'OPPOAll', key: 'e2', label: '对手分' },
    { prop: 'HB1', key: 'e3', label: '破同分1' },
    { prop: 'HB2', key: 'e4', label: '破同分2' },
    { prop: 'RANK', key: 'e6', label: '名次' },
    { prop: 'MEMO', key: 'e5', label: '备注' }
  ],
  MRGO06: [
    { prop: 'NUMBER', key: 'a', label: '编号', width: '80' },
    { prop: 'NAME', key: 'b', label: '姓名' },
    { prop: 'TEAM', key: 'c', label: '单位' },
    { prop: 'HB1', key: 'e3', label: '对手分' },
    { prop: 'HB2', key: 'e4', label: '总局分' },
    { prop: 'HB3', key: 'e4', label: '破同分' },
    { prop: 'RANK', key: 'e6', label: '名次' },
    { prop: 'MEMO', key: 'e5', label: '备注' }
  ],
  MRGO07: [
    { prop: 'NUMBER', key: 'a', label: '编号' },
    { prop: 'NAME', key: 'b', label: '姓名' },
    { prop: 'TEAM', key: 'c', label: '单位' },
    { prop: 'OPPOAll', key: 'e2', label: '对手分' },
    { prop: 'HB1', key: 'e3', label: '破同分1' },
    { prop: 'HB2', key: 'e4', label: '破同分2' },
    { prop: 'RANK', key: 'e6', label: '名次' },
    { prop: 'MEMO', key: 'e5', label: '备注' }
  ],
  MRGO09: [
    { prop: 'NUMBER', key: 'a', label: '编号' },
    { prop: 'NAME', key: 'b', label: '姓名' },
    { prop: 'TEAM', key: 'c', label: '单位' },
    { prop: 'OPPOAll', key: 'e2', label: '对手分' },
    { prop: 'HB1', key: 'e3', label: '破同分1' },
    { prop: 'HB2', key: 'e4', label: '破同分2' },
    { prop: 'RANK', key: 'e6', label: '名次' },
    { prop: 'MEMO', key: 'e5', label: '备注' }
  ],
  MRGO10: [
    { prop: 'NUMBER', key: 'a', label: '编号', fixed: 'left' },
    { prop: 'NAME', key: 'b', label: '姓名' },
    { prop: 'TEAM', key: 'c', label: '单位' },
    { prop: 'HB1', key: 'e3', label: '对手分' },
    { prop: 'HB2', key: 'e4', label: '总局分' },
    { prop: 'HB3', key: 'e7', label: '破同分' },
    { prop: 'RANK', key: 'e6', label: '名次', fixed: 'right' },
    { prop: 'MEMO', key: 'e5', label: '备注' }
  ],
  MRGO11: [
    { prop: 'NUMBER', key: 'a', label: '编号', fixed: 'left' },
    { prop: 'NAME', key: 'b', label: '姓名' },
    { prop: 'TEAM', key: 'c', label: '单位' },
    { prop: 'OPPOAll', key: 'e2', label: '对手分' },
    { prop: 'HB1', key: 'e3', label: '破同分1' },
    { prop: 'HB2', key: 'e4', label: '破同分2' },
    { prop: 'RANK', key: 'e6', label: '名次', fixed: 'right' },
    { prop: 'MEMO', key: 'e5', label: '备注' }
  ],
  MRGO12: [
    { prop: 'NUMBER', key: 'a', label: '编号', fixed: 'left' },
    { prop: 'NAME', key: 'b', label: '姓名' },
    { prop: 'TEAM', key: 'c', label: '单位' },
    { prop: 'HB2', key: 'e4', label: '总局分' },
    { prop: 'HB1', key: 'e3', label: '对手分' },
    { prop: 'HB3', key: 'e7', label: '破同分' },
    { prop: 'RANK', key: 'e6', label: '名次', fixed: 'right' },
    { prop: 'MEMO', key: 'e5', label: '备注' }
  ],
  MRGO13: [
    { prop: 'NUMBER', key: 'a', label: '编号', fixed: 'left' },
    { prop: 'NAME', key: 'b', label: '姓名' },
    { prop: 'TEAM', key: 'c', label: '单位' },
    { prop: 'OPPOAll', key: 'e2', label: '对手分' },
    { prop: 'HB1', key: 'e3', label: '破同分1' },
    { prop: 'HB2', key: 'e4', label: '破同分2' },
    { prop: 'RANK', key: 'e6', label: '名次', fixed: 'right' },
    { prop: 'MEMO', key: 'e5', label: '备注' }
  ],
  MRGO14: [
    { prop: 'NUMBER', key: 'a', label: '编号', fixed: 'left' },
    { prop: 'NAME', key: 'b', label: '姓名' },
    { prop: 'TEAM', key: 'c', label: '单位' },
    { prop: 'OPPOAll', key: 'e2', label: '对手分' },
    { prop: 'HB1', key: 'e3', label: '破同分1' },
    { prop: 'HB2', key: 'e4', label: '破同分2' },
    { prop: 'RANK', key: 'e6', label: '名次', fixed: 'right' },
    { prop: 'MEMO', key: 'e5', label: '备注' }
  ],
  MRGO15: [
    { prop: 'NUMBER', key: 'a', label: '编号', fixed: 'left' },
    { prop: 'NAME', key: 'b', label: '姓名' },
    { prop: 'TEAM', key: 'c', label: '单位' },
    { prop: 'OPPOAll', key: 'e2', label: '对手分' },
    { prop: 'HB1', key: 'e3', label: '破同分1' },
    { prop: 'HB2', key: 'e4', label: '破同分2' },
    { prop: 'RANK', key: 'e6', label: '名次', fixed: 'right' },
    { prop: 'MEMO', key: 'e5', label: '备注' }
  ],
  MRGO16: [
    { prop: 'NUMBER', key: 'a', label: '编号', fixed: 'left' },
    { prop: 'NAME', key: 'b', label: '姓名' },
    { prop: 'TEAM', key: 'c', label: '单位' },
    { prop: 'OPPOAll', key: 'e2', label: '对手分' },
    { prop: 'HB1', key: 'e3', label: '破同分1' },
    { prop: 'HB2', key: 'e4', label: '破同分2' },
    { prop: 'RANK', key: 'e6', label: '名次', fixed: 'right' },
    { prop: 'MEMO', key: 'e5', label: '备注' }
  ],
  MRGO17: [
    { prop: 'NUMBER', key: 'a', label: '编号', fixed: 'left' },
    { prop: 'NAME', key: 'b', label: '姓名' },
    { prop: 'TEAM', key: 'c', label: '单位' },
    { prop: 'OPPOAll', key: 'e2', label: '对手分' },
    { prop: 'HB1', key: 'e3', label: '破同分1' },
    { prop: 'HB2', key: 'e4', label: '破同分2' },
    { prop: 'RANK', key: 'e6', label: '名次', fixed: 'right' },
    { prop: 'MEMO', key: 'e5', label: '备注' }
  ],
  MRGO18: [
    { prop: 'NUMBER', key: 'a', label: '编号', fixed: 'left' },
    { prop: 'NAME', key: 'b', label: '姓名' },
    { prop: 'TEAM', key: 'c', label: '单位' },
    { prop: 'OPPOAll', key: 'e2', label: '对手分' },
    { prop: 'HB1', key: 'e3', label: '破同分1' },
    { prop: 'HB2', key: 'e4', label: '破同分2' },
    { prop: 'RANK', key: 'e6', label: '名次', fixed: 'right' },
    { prop: 'MEMO', key: 'e5', label: '备注' }
  ],
  MRGO19: [
    { prop: 'GROUP', key: 'z', label: '组别', fixed: 'left' },
    { prop: 'NUMBER', key: 'a', label: '编号' },
    { prop: 'NAME', key: 'b', label: '姓名' },
    { prop: 'TEAM', key: 'c', label: '单位' },
    { prop: 'OPPOAll', key: 'e2', label: '对手分' },
    { prop: 'HB1', key: 'e3', label: '破同分1' },
    { prop: 'HB2', key: 'e4', label: '破同分2' },
    { prop: 'RANK', key: 'e6', label: '名次', fixed: 'right' },
    { prop: 'MEMO', key: 'e5', label: '备注' }
  ],
  MRGO20: [
    { prop: 'NUMBER', key: 'a', label: '编号', fixed: 'left' },
    { prop: 'NAME', key: 'b', label: '姓名' },
    { prop: 'TEAM', key: 'c', label: '单位' },
    { prop: 'HB1', key: 'e3', label: '对手分' },
    { prop: 'HB2', key: 'e4', label: '总局分' },
    { prop: 'HB3', key: 'e7', label: '破同分' },
    { prop: 'RANK', key: 'e6', label: '名次', fixed: 'right' },
    { prop: 'MEMO', key: 'e5', label: '备注' }
  ],

  // 围棋
  MSCC01: [
    { prop: 'TableNo', key: 'a', label: '台次', minWidth: '150' },
    { prop: 'Code_R', key: 'b', label: '编号' },
    { prop: 'Unit_R', key: 'c', label: '单位' },
    { prop: 'Name_R', key: 'd', label: '姓名' },
    { prop: 'Grade_R', key: 'e', label: '前轮积分' },
    { prop: '', label: '', minWidth: '200' },
    { prop: 'Grade_B', key: 'f', label: '前轮积分' },
    { prop: 'Name_B', key: 'g', label: '姓名' },
    { prop: 'Unit_B', key: 'h', label: '单位' },
    { prop: 'Code_B', key: 'i', label: '编号' }
  ],
  MSCC02: [
    { prop: 'TableNo', key: 'a', label: '台次', fixed: 'left', minWidth: '150' },
    { prop: 'Code_R', key: 'b', label: '编号' },
    { prop: 'Unit_R', key: 'c', label: '单位' },
    { prop: 'Grade_R', key: 'e', label: '前轮积分' },
    { prop: '', label: '', minWidth: '200' },
    { prop: 'Grade_B', key: 'f', label: '前轮积分' },
    { prop: 'Unit_B', key: 'h', label: '单位' },
    { prop: 'Code_B', key: 'i', label: '编号' }
  ],
  MRCC01: [
    { prop: 'TableNo', key: 'a', label: '台次', fixed: 'left', minWidth: '150' },
    { prop: 'Code_R', key: 'b', label: '编号' },
    { prop: 'Unit_R', key: 'c', label: '单位' },
    { prop: 'Name_R', key: 'd', label: '姓名' },
    { prop: 'Grade_R', key: 'e', label: '前轮积分' },
    { prop: 'Score_R', key: 'j', label: '红方成绩', minWidth: '100' },
    { prop: 'Score_B', key: 'j1', label: '黑方成绩', minWidth: '100' },
    { prop: 'Grade_B', key: 'f', label: '前轮积分' },
    { prop: 'Name_B', key: 'g', label: '姓名' },
    { prop: 'Unit_B', key: 'h', label: '单位' },
    { prop: 'Code_B', key: 'i', label: '编号' }
  ],
  MRCC02: [
    { prop: 'TableNo', key: 'a', label: '台次', fixed: 'left', minWidth: '150' },
    { prop: 'Code_R', key: 'b', label: '编号' },
    { prop: 'Unit_R', key: 'c', label: '单位' },
    { prop: 'Grade_R', key: 'e', label: '前轮积分' },
    { prop: 'Score_R', key: 'j', label: '红方成绩', minWidth: '100' },
    { prop: 'Score_B', key: 'j1', label: '黑方成绩', minWidth: '100' },
    { prop: 'Grade_B', key: 'f', label: '前轮积分' },
    { prop: 'Unit_B', key: 'h', label: '单位' },
    { prop: 'Code_B', key: 'i', label: '编号' }
  ],
  MRCC03: [
    { prop: 'F_Rank', key: 'a', label: '名次', fixed: 'left' },
    { prop: 'F_Delegation', key: 'b', label: '单位' },
    { prop: 'F_Name', key: 'c', label: '姓名' },
    { prop: 'F_Grade', key: 'd', label: '积分' },
    { prop: 'F_Opponent', key: 'e', label: '对手分' },
    { prop: 'F_Win', key: 'a', label: '胜局' },
    { prop: 'F_Foul', key: 'g', label: '犯规' },
    { prop: 'F_Back', label: '后手局数' },
    { prop: 'F_Swin', label: '后手胜局' },
    { prop: 'F_PreRank', label: '上轮名次' },
    { prop: 'F_Description', key: 'h', label: '备注' }
  ],
  MRCC04: [
    { prop: 'F_Rank', key: 'a', label: '名次' },
    { prop: 'F_Delegation', key: 'b', label: '单位' },
    { prop: 'F_Grade', key: 'd', label: '积分' },
    { prop: 'F_Opponent', key: 'e', label: '对手分' },
    { prop: 'F_All', key: 'f', label: '总局分' },
    { prop: 'F_WinMatch', key: 'f1', label: '胜场' },
    // { prop: 'F_WinC', label: '胜场数' },
    // { prop: 'F_WinJ', label: '胜局数' },
    { prop: 'F_Win', label: '胜局' },
    { prop: 'F_Foul', key: 'g', label: '犯规' },
    { prop: 'F_Description', key: 'h', label: '备注' }
  ],
  MRCC05: [
    { prop: 'F_Rank', key: 'a', label: '名次' },
    { prop: 'F_Delegation', key: 'b', label: '单位' },
    { prop: 'F_Name', key: 'c', label: '姓名' },
    { prop: 'F_Grade', key: 'd', label: '积分' },
    { prop: 'F_Opponent', key: 'e', label: '对手分' },
    { prop: 'F_Win', key: 'a', label: '胜局' },
    { prop: 'F_Foul', key: 'g', label: '犯规' },
    { prop: 'F_Back', label: '后手局数' },
    { prop: 'F_Swin', label: '后手胜局' },
    { prop: 'F_PreRank', label: '上轮名次' },
    { prop: 'F_Description', key: 'h', label: '备注' }
  ],
  MRCC06: [
    { prop: 'F_Rank', key: 'a', label: '名次', fixed: 'left' },
    { prop: 'F_Delegation', key: 'b', label: '单位' },
    { prop: 'F_Name', key: 'c', label: '姓名' },
    { prop: 'F_Grade', key: 'd', label: '积分' },
    { prop: 'F_Opponent', key: 'e', label: '对手分' },
    { prop: 'F_Win', key: 'a', label: '胜局' },
    { prop: 'F_Foul', key: 'g', label: '犯规' },
    { prop: 'F_Back', label: '后手局数' },
    { prop: 'F_Swin', label: '后手胜局' },
    { prop: 'F_PreRank', label: '上轮名次' },
    { prop: 'F_Description', key: 'h', label: '备注' }
  ],
  MRCC07: [
    { prop: 'F_Rank', key: 'a', label: '名次', fixed: 'left' },
    { prop: 'F_Delegation', key: 'b', label: '单位' },
    { prop: 'F_Name', key: 'c', label: '姓名' },
    { prop: 'F_Grade', key: 'd', label: '场分' },
    { prop: 'F_Opponent', key: 'e', label: '对手分' },
    { prop: 'F_All	', key: 'e1', label: '总局分' },
    { prop: 'F_WinMatch', key: 'f1', label: '胜场' },
    { prop: 'F_Win', key: 'a', label: '胜局' },
    { prop: 'F_Foul', key: 'g', label: '犯规' },
    { prop: 'F_Description', key: 'h', label: '备注' }
  ],

  // 国际象棋
  MSCH01: [
    { prop: 'TableNo', key: 'a', label: '桌号', minWidth: '150' },
    { prop: 'BibA', key: 'b', label: '号码' },
    { prop: 'NameA', key: 'c', label: '姓名' },
    { prop: 'TechScoreA', key: 'c1', label: '等级分' },
    { prop: 'TeamA', key: 'e', label: '单位' },
    { prop: 'ScoreA', key: 'k', label: '积分' },
    { prop: '', label: '', minWidth: '200' },
    { prop: 'ScoreB', key: 'l', label: '积分' },
    { prop: 'NameB', key: 'h', label: '姓名' },
    { prop: 'TechScoreB', key: 'c2', label: '等级分' },
    { prop: 'TeamB', key: 'i', label: '单位' },
    { prop: 'BibB', key: 'f', label: '号码' }
  ],
  MSCH02: [
    { prop: 'MatchNo', key: 'a', label: '赛号', minWidth: '150' },
    { prop: 'OrderA', key: 'b', label: '序号' },
    { prop: 'TeamnameA', key: 'c', label: '单位/姓名' },
    { prop: 'TechScoreA', key: 'c1', label: '等级分' },
    { prop: '', label: '', minWidth: '200' },
    { prop: 'OrderB', key: 'e', label: '序号' },
    { prop: 'TeamnameB', key: 'l', label: '单位/姓名' },
    { prop: 'TechScoreB', key: 'c2', label: '等级分' }
  ],
  MSCH03: [
    { prop: 'TableNo', key: 'a', label: '桌号', minWidth: '150' },
    { prop: 'BibA', key: 'b', label: '号码' },
    { prop: 'NameA', key: 'c', label: '姓名' },
    { prop: 'TeamA', key: 'e', label: '单位' },
    { prop: 'ScoreA', key: 'k', label: '积分' },
    { prop: '', label: '', minWidth: '200' },
    { prop: 'ScoreB', key: 'l', label: '积分' },
    { prop: 'NameB', key: 'h', label: '姓名' },
    { prop: 'TeamB', key: 'i', label: '单位' },
    { prop: 'BibB', key: 'f', label: '号码' }
  ],
  MSCH04: [
    { prop: 'MatchNo', key: 'a', label: '赛号', minWidth: '150' },
    { prop: 'OrderA', key: 'b', label: '序号' },
    { prop: 'TeamnameA', key: 'c', label: '单位/姓名' },
    { prop: '', label: '', minWidth: '200' },
    { prop: 'OrderB', key: 'e', label: '序号' },
    { prop: 'TeamnameB', key: 'l', label: '单位/姓名' }
  ],
  MRCH01: [
    { prop: 'TableNo', key: 'a', label: '桌号', minWidth: '150' },
    { prop: 'BibA', key: 'b', label: '号码' },
    { prop: 'NameA', key: 'c', label: '姓名' },
    { prop: 'TechScoreA', key: 'c1', label: '等级分' },
    { prop: 'TeamA', key: 'e', label: '单位' },
    { prop: 'ScoreA', key: 'k', label: '积分' },
    { prop: 'Result', key: 'l1', label: '当前成绩', minWidth: '200' },
    { prop: 'ScoreB', key: 'l', label: '积分' },
    { prop: 'NameB', key: 'h', label: '姓名' },
    { prop: 'TechScoreB', key: 'c2', label: '等级分' },
    { prop: 'TeamB', key: 'i', label: '单位' },
    { prop: 'BibB', key: 'f', label: '号码' }
  ],
  MRCH02: [
    { prop: 'MatchNo', key: 'a', label: '赛号', fixed: 'left' },
    { prop: 'OrderA', key: 'b', label: '序号' },
    { prop: 'TeamnameA', key: 'c', label: '单位/姓名' },
    { prop: 'TechScoreA', key: 'c1', label: '等级分' },
    { prop: 'Result', key: 'l1', label: '当前成绩', minWidth: '200' },
    { prop: 'OrderB', key: 'e', label: '序号' },
    { prop: 'TeamnameB', key: 'l', label: '单位/姓名' },
    { prop: 'TechScoreB', key: 'c2', label: '等级分' }
  ],
  MRCH03: [
    { prop: 'Rank', label: '名次' },
    { prop: 'Team', label: '单位' },
    { prop: 'MatchCount', label: '比赛数' },
    { prop: 'WinCount', label: '胜场数' },
    { prop: 'LostCount', label: '负场数' },
    { prop: 'TieCount', label: '平场数' },
    { prop: 'MatchScore', label: '场分' },
    { prop: 'Score', label: '局分' },
    { prop: 'SB', label: '索伯分' }
  ],
  MRCH04: [
    { prop: 'Rank', label: '名次' },
    { prop: 'SNo', label: '比赛号' },
    { prop: 'Name', label: '姓名' },
    { prop: 'TechScore', label: '等级分' },
    { prop: 'Team', label: '单位' },
    { prop: 'Score', label: '积分' },
    { prop: 'BH1', label: '扣除对手分' },
    { prop: 'BH2', label: '对手分' },
    { prop: 'BH3	', label: '索伯分' }
  ],
  MRCH05: [
    { prop: 'Rank', label: '名次' },
    { prop: 'Team', label: '单位' },
    { prop: 'MatchCount', label: '比赛数' },
    { prop: 'WinCount', label: '胜场数' },
    { prop: 'LostCount', label: '负场数' },
    { prop: 'TieCount', label: '平场数' },
    { prop: 'MatchScore', label: '场分' },
    { prop: 'Score', label: '局分' }
  ],
  MRCH06: [
    { prop: 'Rank', label: '名次' },
    { prop: 'SNo', label: '比赛号' },
    { prop: 'Name', label: '姓名' },
    { prop: 'TechScore', label: '等级分' },
    { prop: 'Team', label: '单位' },
    { prop: 'Score', label: '积分' },
    { prop: 'BH1', label: '扣除对手分' },
    { prop: 'BH2', label: '对手分' }
  ],
  MRCH07: [
    { prop: 'TableNo', key: 'a', label: '桌号', minWidth: '150' },
    { prop: 'BibA', key: 'b', label: '号码' },
    { prop: 'NameA', key: 'c', label: '姓名' },
    { prop: 'TeamA', key: 'e', label: '单位' },
    { prop: 'ScoreA', key: 'k', label: '积分' },
    { prop: 'Result', key: 'l1', label: '当前成绩', minWidth: '200' },
    { prop: 'ScoreB', key: 'l', label: '积分' },
    { prop: 'NameB', key: 'h', label: '姓名' },
    { prop: 'TeamB', key: 'i', label: '单位' },
    { prop: 'BibB', key: 'f', label: '号码' }
  ],
  MRCH08: [
    { prop: 'MatchNo', key: 'a', label: '赛号', fixed: 'left' },
    { prop: 'OrderA', key: 'b', label: '序号' },
    { prop: 'TeamnameA', key: 'c', label: '单位/姓名' },
    { prop: 'Result', key: 'l1', label: '当前成绩', minWidth: '200' },
    { prop: 'OrderB', key: 'e', label: '序号' },
    { prop: 'TeamnameB', key: 'l', label: '单位/姓名' }
  ],
  MRCH09: [
    { prop: 'Rank', label: '名次' },
    { prop: 'SNo', label: '比赛号' },
    { prop: 'Name', label: '姓名' },
    { prop: 'Team', label: '单位' },
    { prop: 'TechScore', label: '等级分' },
    { prop: 'Score', label: '积分' },
    { prop: 'Res', label: 'Res.' }
  ],

  // 桥牌
  MSBR02: [
    { prop: 'Table', key: 'a', label: '桌号', fixed: 'left' },
    { prop: 'Team1', key: 'c', label: '主队' },
    { prop: 'Team2', key: 'd', label: '客队' },
    { prop: 'Round', key: 'b', label: '轮次' },
    { prop: 'MatchOrder', key: 'e', label: '轮内序号' }
  ],
  MSBR03: [
    { prop: 'Table', key: 'a', label: '桌号', fixed: 'left' },
    { prop: 'Home_team', key: 'c', label: '主队' },
    { prop: 'Guest_team', key: 'd', label: '客队' },
    { prop: 'Note', key: 'b', label: '备注' }
  ],
  MSBR04: [
    { prop: 'Group', key: 'a', label: '桌号', fixed: 'left' },
    { prop: 'Team', key: 'c', label: '单位' },
    { prop: 'Bib', key: 'd', label: '号码' },
    { prop: 'Name', key: 'b', label: '姓名' },
    { prop: 'Note', key: 'e', label: '备注' }
  ],
  MSBR05: [
    { prop: 'F_MatchName', key: 'a', label: '桌号', fixed: 'left' },
    { prop: 'F_DelegationA', key: 'c', label: '主队' },
    { prop: 'F_DelegationB', key: 'd', label: '客队' }
    // { prop: 'F_Round', key: 'b', label: '轮次序号' },
    // { prop: 'F_MatchOrder', key: 'e', label: '轮内序号' }
  ],
  MRBR02: [
    { prop: 'Table', key: 'a', label: '桌号', fixed: 'left' },
    { prop: 'Team1', key: 'c', label: '主队' },
    { prop: 'Band_score1', key: 'd1', label: '主队带分' },
    { prop: 'Penalty1', key: 'd4', label: '主队罚分' },
    { prop: 'Score1', key: 'd4', label: '主队总分' },
    { prop: 'Score2', key: 'd6', label: '客队总分' },
    { prop: 'Penalty2', key: 'd7', label: '客队罚分' },
    { prop: 'Band_score2', key: 'd10', label: '客队带分' },
    { prop: 'Team2', key: 'd11', label: '客队' }
  ],
  MRBR03: [
    { prop: 'Table', key: 'a', label: '桌号', fixed: 'left' },
    { prop: 'Home_team', key: 'c', label: '主队' },
    { prop: 'Guest_team', key: 'd', label: '客队' },
    { prop: 'IMPs_home', key: 'e', label: '主队IMPs' },
    { prop: 'IMPs_guest', key: 'f', label: '客队IMPs' },
    { prop: 'VPs_home', key: 'g', label: '主队VPs' },
    { prop: 'VPs_guest', key: 'h', label: '客队VPs' }
  ],
  MRBR04: [
    { prop: 'Ranking', key: 'a', label: '排名', fixed: 'left' },
    { prop: 'Team', key: 'c', label: '单位' },
    { prop: 'Bib', key: 'd', label: '号码' },
    { prop: 'Name', key: 'e', label: '姓名' },
    { prop: 'Gain', key: 'f', label: 'MP' },
    { prop: 'Penalty', key: 'g', label: '百分比' }
  ],
  MRBR05: [
    { prop: 'F_MatchName', key: 'a', label: '桌号', fixed: 'left' },
    { prop: 'F_DelegationA', key: 'c', label: '主队' },
    { prop: 'F_DelegationB', key: 'd', label: '客队' },
    { prop: 'F_ResultA', key: 'e', label: '主队成绩' },
    { prop: 'F_ResultB', key: 'f', label: '客队IMPs' }
    // { prop: 'F_Winner', key: 'g', label: '胜方' },
    // { prop: 'F_Loser', key: 'h', label: '败方' },
    // { prop: 'F_Round', key: 'i', label: '轮次序号' },
    // { prop: 'F_MatchOrder', key: 'j', label: '轮内序号' }
  ],
  MRBR06: [
    { prop: 'Table', key: 'a', label: '桌号', fixed: 'left' },
    { prop: 'Team1', key: 'c', label: '主队' },
    { prop: 'Band_score1', key: 'd1', label: '主队带分' },
    { prop: 'Penalty1', key: 'd4', label: '主队罚分' },
    { prop: 'Score1', key: 'd4', label: '主队总分' },
    { prop: 'Score2', key: 'd6', label: '客队总分' },
    { prop: 'Penalty2', key: 'd7', label: '客队罚分' },
    { prop: 'Band_score2', key: 'd10', label: '客队带分' },
    { prop: 'Team2', key: 'd11', label: '客队' }
  ],
  MRBR01: [
    { prop: 'Ranking', key: 'a', label: '名次', fixed: 'left' },
    { prop: 'Team', key: 'c', label: '单位' },
    { prop: 'VPS', key: 'd', label: 'VPs' },
    { prop: 'Penalty', key: 'e', label: '罚分' },
    { prop: 'Score', key: 'f', label: '累计' },
    { prop: 'Note', key: 'g', label: '备注' }
  ],
  MRBR07: [
    { prop: 'Rank', key: 'a', label: '名次', fixed: 'left' },
    { prop: 'Penalty', key: 'e', label: '平均' },
    { prop: 'Team', key: 'd', label: '单位' },
    { prop: 'Name', key: 'c', label: '姓名' },
    { prop: 'Penalty1', key: 'f', label: '第1场' },
    { prop: 'Penalty2', key: 'g', label: '第2场' },
    { prop: 'Penalty3', key: 'h', label: '第3场' },
    { prop: 'Penalty4', key: 'i', label: '第4场' }
  ],
  MRBR08: [
    { prop: 'Rank', key: 'a', label: '名次', fixed: 'left' },
    { prop: 'Penalty', key: 'e', label: '平均' },
    { prop: 'Team', key: 'd', label: '单位' },
    { prop: 'Name', key: 'c', label: '姓名' },
    { prop: 'Penalty1', key: 'f', label: '第1场' },
    { prop: 'Penalty2', key: 'g', label: '第2场' },
    { prop: 'Penalty3', key: 'h', label: '第3场' },
    { prop: 'Penalty4', key: 'i', label: '第4场' },
    { prop: 'Penalty5', key: 'j', label: '第5场' },
    { prop: 'Penalty6', key: 'k', label: '第6场' }
  ],
  MRBR09: [
    { prop: 'Rank', key: 'a', label: '名次', fixed: 'left' },
    { prop: 'Penalty', key: 'e', label: '平均' },
    { prop: 'Team', key: 'd', label: '单位' },
    { prop: 'Name', key: 'c', label: '姓名' },
    { prop: 'Penalty1', key: 'f', label: '第1场' },
    { prop: 'Penalty2', key: 'g', label: '第2场' },
    { prop: 'Penalty3', key: 'h', label: '第3场' },
    { prop: 'Penalty4', key: 'i', label: '第4场' },
    { prop: 'Penalty5', key: 'j', label: '第5场' }
  ],
  MRBR10: [
    { prop: 'Rank', key: 'a', label: '名次', fixed: 'left' },
    { prop: 'Penalty', key: 'e', label: '平均' },
    { prop: 'Team', key: 'd', label: '单位' },
    { prop: 'Name', key: 'c', label: '姓名' },
    { prop: 'Penalty1', key: 'f', label: '第1轮' },
    { prop: 'Penalty2', key: 'g', label: '第2轮' },
    { prop: 'Penalty3', key: 'h', label: '第3轮' },
    { prop: 'Penalty4', key: 'i', label: '第4轮' },
    { prop: 'Penalty5', key: 'j', label: '第5轮' },
    { prop: 'Penalty6', key: 'k', label: '第6轮' },
    { prop: 'Penalty7', key: 'l', label: '第7轮' },
    { prop: 'Penalty8', key: 'm', label: '第8轮' },
    { prop: 'Penalty9', key: 'n', label: '第9轮' },
    { prop: 'Penalty10', key: 'o', label: '第10轮' }
  ],
  MRBR11: [
    { prop: 'Rank', key: 'a', label: '名次', fixed: 'left' },
    { prop: 'Penalty', key: 'e', label: '平均' },
    { prop: 'Team', key: 'd', label: '单位' },
    { prop: 'Name', key: 'c', label: '姓名' },
    { prop: 'Penalty1', key: 'f', label: '第1轮' },
    { prop: 'Penalty2', key: 'g', label: '第2轮' },
    { prop: 'Penalty3', key: 'h', label: '第3轮' },
    { prop: 'Penalty4', key: 'i', label: '第4轮' },
    { prop: 'Penalty5', key: 'j', label: '第5轮' },
    { prop: 'Penalty6', key: 'k', label: '第6轮' },
    { prop: 'Penalty7', key: 'l', label: '第7轮' },
    { prop: 'Penalty8', key: 'm', label: '第8轮' },
    { prop: 'Penalty9', key: 'n', label: '第9轮' },
    { prop: 'Penalty10', key: 'o', label: '第10轮' },
    { prop: 'Penalty11', key: 'p', label: '第11轮' }
  ],
  MRBR12: [
    { prop: 'Rank', key: 'a', label: '名次', fixed: 'left' },
    { prop: 'Penalty', key: 'e', label: '平均' },
    { prop: 'Team', key: 'd', label: '单位' },
    { prop: 'Name', key: 'c', label: '姓名' },
    { prop: 'Penalty1', key: 'f', label: '第1轮' },
    { prop: 'Penalty2', key: 'g', label: '第2轮' },
    { prop: 'Penalty3', key: 'h', label: '第3轮' },
    { prop: 'Penalty4', key: 'i', label: '第4轮' },
    { prop: 'Penalty5', key: 'j', label: '第5轮' },
    { prop: 'Penalty6', key: 'k', label: '第6轮' },
    { prop: 'Penalty7', key: 'l', label: '第7轮' },
    { prop: 'Penalty8', key: 'm', label: '第8轮' },
    { prop: 'Penalty9', key: 'n', label: '第9轮' },
    { prop: 'Penalty10', key: 'o', label: '第10轮' },
    { prop: 'Penalty11', key: 'p', label: '第11轮' },
    { prop: 'Penalty12', key: 'q', label: '第12轮' }
  ],
  MRBR13: [
    { prop: 'Rank', key: 'a', label: '名次', fixed: 'left' },
    { prop: 'Penalty', key: 'e', label: '平均' },
    { prop: 'Team', key: 'd', label: '单位' },
    { prop: 'Name', key: 'c', label: '姓名' },
    { prop: 'Penalty1', key: 'f', label: '第1轮' },
    { prop: 'Penalty2', key: 'g', label: '第2轮' },
    { prop: 'Penalty3', key: 'h', label: '第3轮' },
    { prop: 'Penalty4', key: 'i', label: '第4轮' },
    { prop: 'Penalty5', key: 'j', label: '第5轮' },
    { prop: 'Penalty6', key: 'k', label: '第6轮' },
    { prop: 'Penalty7', key: 'l', label: '第7轮' }
  ],
  MRBR14: [
    { prop: 'Rank', key: 'a', label: '名次', fixed: 'left' },
    { prop: 'Penalty', key: 'e', label: '平均' },
    { prop: 'Team', key: 'd', label: '单位' },
    { prop: 'Name', key: 'c', label: '姓名' },
    { prop: 'Penalty1', key: 'f', label: '第1轮' },
    { prop: 'Penalty2', key: 'g', label: '第2轮' },
    { prop: 'Penalty3', key: 'h', label: '第3轮' },
    { prop: 'Penalty4', key: 'i', label: '第4轮' },
    { prop: 'Penalty5', key: 'j', label: '第5轮' },
    { prop: 'Penalty6', key: 'k', label: '第6轮' },
    { prop: 'Penalty7', key: 'l1', label: '第7轮' },
    { prop: 'Penalty8', key: 'l2', label: '第8轮' },
    { prop: 'Penalty9', key: 'l3', label: '第9轮' },
    { prop: 'Penalty10', key: 'l4', label: '第10轮' },
    { prop: 'Penalty11', key: 'l5', label: '第11轮' },
    { prop: 'Penalty12', key: 'l6', label: '第12轮' },
    { prop: 'Penalty13', key: 'l7', label: '第13轮' }
  ],
  MRBR15: [
    { prop: 'Rank', key: 'a', label: '名次', fixed: 'left' },
    { prop: 'MP', key: 'd1', label: 'MP' },
    { prop: 'Penalty', key: 'e', label: '%' },
    { prop: 'Team', key: 'd', label: '单位' },
    { prop: 'Name', key: 'c', label: '姓名' }
  ],
  MRBR16: [
    { prop: 'Rank', key: 'a', label: '名次', fixed: 'left' },
    { prop: 'Penalty', key: 'e', label: '%' },
    { prop: 'Team', key: 'd', label: '单位' },
    { prop: 'Name', key: 'c', label: '姓名' },
    { prop: 'Final', key: 'd1', label: '决赛' },
    { prop: 'Extra', key: 'd2', label: '带分' }
  ],

  // 五子棋
  MSGB01: [
    { prop: 'TableNo', key: 'a', label: '台号', fixed: 'left', minWidth: '150' },
    { prop: 'Order-A', key: 'b', label: '编号' },
    { prop: 'Name-A', key: 'c', label: '姓名' },
    { prop: 'Team-A', key: 'd', label: '单位' },
    { prop: 'Score-A', key: 'e', label: '上轮积分' },
    { prop: '', key: 'j', label: '', minWidth: '200' },
    { prop: 'Score-B', key: 'i', label: '上轮积分' },
    { prop: 'Team-B', key: 'h', label: '单位' },
    { prop: 'Name-B', key: 'g', label: '姓名' },
    { prop: 'Order-B', key: 'f', label: '编号' }
  ],
  MSGB02: [
    { prop: 'TableNo', key: 'a', label: '台号', fixed: 'left', minWidth: '150' },
    { prop: 'Order-A', key: 'b', label: '编号' },
    { prop: 'Team-A', key: 'd', label: '单位' },
    { prop: 'Score-A', key: 'e', label: '上轮积分' },
    { prop: '', key: 'j', label: '当前成绩', minWidth: '200' },
    { prop: 'Score-B', key: 'i', label: '上轮积分' },
    { prop: 'Team-B', key: 'h', label: '单位' },
    { prop: 'Order-B', key: 'f', label: '编号' }
  ],
  MSGB03: [
    { prop: 'TableNo', key: 'a', label: '台号', fixed: 'left', minWidth: '150' },
    { prop: 'Order-A', key: 'b', label: '编号' },
    { prop: 'Name-A', key: 'c', label: '姓名' },
    { prop: 'Team-A', key: 'd', label: '单位' },
    { prop: '', key: 'j', label: '', minWidth: '200' },
    { prop: 'Team-B', key: 'h', label: '单位' },
    { prop: 'Name-B', key: 'g', label: '姓名' },
    { prop: 'Order-B', key: 'f', label: '编号' }
  ],
  MRGB01: [
    { prop: 'TableNo', key: 'a', label: '台号', minWidth: '150' },
    { prop: 'Order-A', key: 'b', label: '编号' },
    { prop: 'Name-A', key: 'c', label: '姓名' },
    { prop: 'Team-A', key: 'd', label: '单位' },
    { prop: 'Score-A', key: 'e', label: '上轮积分' },
    { prop: 'Result', key: 'j', label: '当前成绩', minWidth: '200' },
    { prop: 'Score-B', key: 'i', label: '上轮积分' },
    { prop: 'Team-B', key: 'h', label: '单位' },
    { prop: 'Name-B', key: 'g', label: '姓名' },
    { prop: 'Order-B', key: 'f', label: '编号' }
  ],
  MRGB02: [
    { prop: 'TableNo', key: 'a', label: '台号', minWidth: '150' },
    { prop: 'Order-A', key: 'b', label: '编号' },
    { prop: 'Team-A', key: 'd', label: '单位' },
    { prop: 'Score-A', key: 'e', label: '上轮积分' },
    { prop: 'Result', key: 'j', label: '当前成绩', minWidth: '200' },
    { prop: 'Score-B', key: 'i', label: '上轮积分' },
    { prop: 'Team-B', key: 'h', label: '单位' },
    { prop: 'Order-B', key: 'f', label: '编号' }
  ],
  MRGB03: [
    { prop: 'TableNo', key: 'a', label: '台号', minWidth: '150' },
    { prop: 'Order-A', key: 'b', label: '编号' },
    { prop: 'Team-A', key: 'd', label: '单位/姓名' },
    { prop: 'Score-A', key: 'e', label: '上轮积分' },
    { prop: 'Result', key: 'j', label: '', minWidth: '200' },
    { prop: 'Score-B', key: 'i', label: '上轮积分' },
    { prop: 'Team-B', key: 'h', label: '单位/姓名' },
    { prop: 'Order-B', key: 'f', label: '编号' }
  ],
  MRGB04: [
    { prop: 'TableNo', key: 'a', label: '台号', minWidth: '150' },
    { prop: 'Order-A', key: 'b', label: '编号' },
    { prop: 'Name-A', key: 'c', label: '姓名' },
    { prop: 'Team-A', key: 'd', label: '单位' },
    { prop: 'Result', key: 'j', label: '当前成绩', minWidth: '200' },
    { prop: 'Team-B', key: 'h', label: '单位' },
    { prop: 'Name-B', key: 'g', label: '姓名' },
    { prop: 'Order-B', key: 'f', label: '编号' }
  ],
  MRGB05: [
    { prop: 'Rank', key: 'a', label: '名次', fixed: 'left' },
    { prop: 'Name', key: 'c', label: '姓名' },
    { prop: 'Order', key: 'd', label: '编号' },
    { prop: 'Team', key: 'e', label: '单位' },
    { prop: 'Score', key: 'f', label: '积分' },
    { prop: 'HB1', key: 'g', label: '对手分' },
    { prop: 'HB2', key: 'h', label: '中间对手分' },
    { prop: 'WinCount', key: 'i', label: '胜局数' }
  ],
  MRGB06: [
    { prop: 'Rank', key: 'a', label: '名次', fixed: 'left' },
    { prop: 'Order', key: 'd', label: '编号' },
    { prop: 'Team', key: 'e', label: '单位' },
    { prop: 'Score', key: 'f', label: '积分' },
    { prop: 'HB1', key: 'g', label: '总局分' },
    { prop: 'HB2', key: 'h', label: '伯格分' }
  ],
  MRGB07: [
    { prop: 'Rank', key: 'a1', label: '名次', fixed: 'left' },
    { prop: 'Order', key: 'a', label: '编号' },
    { prop: 'Name', key: 'b', label: '姓名' },
    { prop: 'Team', key: 'c', label: '单位' },
    { prop: 'Score', key: 'e2', label: '积分' },
    { prop: 'HB1', key: 'e3', label: '对手分' },
    { prop: 'HB2', key: 'e4', label: '中间对手分' },
    { prop: 'WinCount', key: 'e5', label: '胜局数' }
  ],
  MRGB08: [
    { prop: 'Rank', key: 'a1', label: '名次', fixed: 'left' },
    { prop: 'Order', key: 'a', label: '编号' },
    { prop: 'Name', key: 'b', label: '姓名' },
    { prop: 'Team', key: 'c', label: '单位' },
    { prop: 'Score', key: 'e2', label: '积分' },
    { prop: 'HB1', key: 'e3', label: '对手分' },
    { prop: 'HB2', key: 'e4', label: '中间对手分' },
    { prop: 'WinCount', key: 'e5', label: '胜局数' }
  ],
  MRGB09: [
    { prop: 'Rank', key: 'a1', label: '名次', fixed: 'left' },
    { prop: 'Order', key: 'a', label: '编号' },
    { prop: 'Team', key: 'c', label: '单位/姓名' },
    { prop: 'Score', key: 'e2', label: '积分' },
    { prop: 'HB1', key: 'e3', label: '总局分' },
    { prop: 'HB2', key: 'e4', label: '伯格分' }
  ],
  MRGB10: [
    { prop: 'Rank', key: 'a1', label: '名次', fixed: 'left' },
    { prop: 'Order', key: 'a', label: '编号' },
    { prop: 'Name', key: 'b', label: '姓名' },
    { prop: 'Team', key: 'c', label: '单位' },
    { prop: 'Score', key: 'e2', label: '积分' },
    { prop: 'HB1', key: 'e3', label: '对手分' },
    { prop: 'HB2', key: 'e4', label: '中间对手分' },
    { prop: 'WinCount', key: 'e5', label: '胜局数' }
  ],
  MRGB11: [
    { prop: 'Rank', key: 'a1', label: '名次', fixed: 'left' },
    { prop: 'Order', key: 'a', label: '编号' },
    { prop: 'Team', key: 'c', label: '单位/姓名' },
    { prop: 'Score', key: 'e2', label: '积分' },
    { prop: 'HB1', key: 'e3', label: '总局分' },
    { prop: 'HB2', key: 'e4', label: '伯格分' }
  ],
  MRGB12: [
    { prop: 'Rank', key: 'a1', label: '名次', fixed: 'left' },
    { prop: 'Order', key: 'a', label: '编号' },
    { prop: 'Team', key: 'c', label: '单位/姓名' },
    { prop: 'Score', key: 'e2', label: '积分' },
    { prop: 'HB2', key: 'e3', label: '对手分' },
    { prop: 'HB3', key: 'e4', label: '中间对手分' },
    { prop: 'HB1', key: 'e4', label: '总局分' }
  ],

  // 国际跳棋
  MSDR01: [
    { prop: 'table', key: 'a', label: '台号', fixed: 'left', minWidth: '150' },
    { prop: 'Number1', key: 'b', label: '编号' },
    { prop: 'Name1', key: 'c', label: '姓名' },
    { prop: 'Group1', key: 'd', label: '单位' },
    { prop: 'Grade1', key: 'e', label: '上轮积分' },
    { prop: '', key: 'j', label: '', minWidth: '200' },
    { prop: 'Grade2', key: 'i', label: '上轮积分' },
    { prop: 'Group2', key: 'h', label: '单位' },
    { prop: 'Name2', key: 'g', label: '姓名' },
    { prop: 'Number2', key: 'f', label: '编号' }
  ],
  MSDR02: [
    { prop: 'table', key: 'a', label: '台次', fixed: 'left', minWidth: '150' },
    { prop: 'Team1', key: 'b', label: '队编号' },
    { prop: 'GroupName1', key: 'c', label: '单位/姓名' },
    { prop: '', key: 'j', label: '', minWidth: '200' },
    { prop: 'GroupName2', key: 'g', label: '单位/姓名' },
    { prop: 'Team2', key: 'f', label: '队编号' }
  ],
  MSDR03: [
    { prop: 'table', key: 'a', label: '台号', fixed: 'left', minWidth: '150' },
    { prop: 'Number1', key: 'b', label: '编号' },
    { prop: 'Name1', key: 'c', label: '姓名' },
    { prop: 'Group1', key: 'd', label: '单位' },
    { prop: '', key: 'j', label: '', minWidth: '200' },
    { prop: 'Group2', key: 'h', label: '单位' },
    { prop: 'Name2', key: 'g', label: '姓名' },
    { prop: 'Number2', key: 'f', label: '编号' }
  ],
  MRDR01: [
    { prop: 'table', key: 'a', label: '台号', fixed: 'left', minWidth: '150' },
    { prop: 'Number1', key: 'b', label: '编号' },
    { prop: 'Name1', key: 'c', label: '姓名' },
    { prop: 'Group1', key: 'd', label: '单位' },
    { prop: 'Grade1', key: 'e', label: '上轮积分' },
    { prop: 'Result', key: 'a1', label: '当前积分', minWidth: '200' },
    { prop: 'Grade2', key: 'i', label: '上轮积分' },
    { prop: 'Group2', key: 'h', label: '单位' },
    { prop: 'Name2', key: 'g', label: '姓名' },
    { prop: 'Number2', key: 'f', label: '编号' }
  ],
  MRDR02: [
    { prop: 'table', key: 'a', label: '台次', fixed: 'left', minWidth: '150' },
    { prop: 'Team1', key: 'b', label: '队编号' },
    { prop: 'GroupName1', key: 'c', label: '单位/姓名' },
    { prop: 'Result', key: 'a1', label: '当前积分', minWidth: '200' },
    { prop: 'GroupName2', key: 'g', label: '单位/姓名' },
    { prop: 'Team2', key: 'f', label: '队编号' }
  ],
  MRDR03: [
    { prop: 'Number', key: 'a', label: '编号', fixed: 'left' },
    { prop: 'Group', key: 'c', label: '单位' },
    { prop: 'Name', key: 'd', label: '姓名' },
    { prop: 'Grade', key: 'e', label: '积分' },
    { prop: 'Middle', key: 'f', label: '中间对手分' },
    { prop: 'Opponent01', key: 'g', label: '截断法' },
    { prop: 'Opponent02', key: 'h', label: '对手分' },
    { prop: 'Opponent03', key: 'i', label: '赢棋数' },
    { prop: 'Ranking', key: 'j', label: '名次' }
  ],
  MRDR04: [
    { prop: 'Number', key: 'a', label: '编号', fixed: 'left' },
    { prop: 'Group', key: 'c', label: '单位' },
    { prop: 'SelfGrade', key: 'd', label: '场分' },
    { prop: 'OppoGrade', key: 'e', label: '对手场分' },
    { prop: 'SumGrade', key: 'f', label: '总局分' },
    { prop: 'EachGrade', key: 'f1', label: '逐台局分' },
    { prop: 'Ranking', key: 'j', label: '名次' }
  ],
  MRDR05: [
    { prop: 'table', key: 'a', label: '台号', fixed: 'left', minWidth: '150' },
    { prop: 'Number1', key: 'b', label: '编号' },
    { prop: 'Name1', key: 'c', label: '姓名' },
    { prop: 'Group1', key: 'd', label: '单位' },
    { prop: 'Result', key: 'a1', label: '当前积分', minWidth: '200' },
    { prop: 'Group2', key: 'h', label: '单位' },
    { prop: 'Name2', key: 'g', label: '姓名' },
    { prop: 'Number2', key: 'f', label: '编号' }
  ],
  MRDR06: [
    { prop: 'Number', label: '编号' },
    { prop: 'Group', label: '单位' },
    { prop: 'SelfGrade', label: '场分' },
    { prop: 'SumGrade', label: '总局分' },
    { prop: 'WinOppo', label: '直胜' },
    { prop: 'FirstGrade', label: '首台局分' },
    { prop: 'EachGrade', label: '逐台局分' },
    { prop: 'Ranking', label: '名次' }
  ],

  // 掼蛋
  MSWE01: [
    { prop: 'Table', key: 'a', label: '桌号', fixed: 'left', minWidth: '150' },
    { prop: 'HomeNo', key: 'c', label: '编号' },
    { prop: 'HomeTeam', key: 'd', label: '单位' },
    { prop: 'HomeName', key: 'e', label: '姓名' },
    { prop: 'GuestNo', key: 'h', label: '编号' },
    { prop: 'GuestTeam', key: 'i', label: '单位' },
    { prop: 'GuestName', key: 'j', label: '姓名' }
  ],
  MRWE01: [
    { prop: 'Table', key: 'a', label: '桌号', fixed: 'left', minWidth: '150' },
    { prop: 'HomeNo', key: 'c', label: '编号' },
    { prop: 'HomeTeam', key: 'd', label: '单位' },
    { prop: 'HomeName', key: 'e', label: '姓名' },
    { prop: 'HomeResult', key: 'l', label: '本轮成绩' },
    { prop: 'GuestResult', key: 'm', label: '本轮成绩' },
    { prop: 'GuestNo', key: 'h', label: '编号' },
    { prop: 'GuestTeam', key: 'i', label: '单位' },
    { prop: 'GuestName', key: 'j', label: '姓名' }
  ],
  MRWE02: [
    { prop: 'Rank', key: 'a', label: '名次', fixed: 'left' },
    { prop: 'No', key: 'e', label: '编号' },
    { prop: 'Team', key: 'd', label: '单位' },
    { prop: 'Name', key: 'c', label: '姓名' },
    { prop: 'Score', key: 'k1', label: '总积分' },
    { prop: 'MPPerc', key: 'l', label: 'MP%' },
    { prop: 'IfMatch', key: 'm', label: '相遇' },
    { prop: 'WinCount', key: 'n', label: '赢轮数' },
    { prop: 'TieCount', key: 'o', label: '平轮数' },
    { prop: 'LevelScore', key: 'p', label: '级分' },
    { prop: 'OppScore', key: 'q', label: '对手分' }
  ],
  MRWE03: [
    { prop: 'Rank', key: 'a', label: '名次', fixed: 'left' },
    { prop: 'No', key: 'e', label: '编号' },
    { prop: 'Team', key: 'd', label: '单位' },
    { prop: 'Name', key: 'c', label: '姓名' },
    { prop: 'Score', key: 'k1', label: '总积分' },
    { prop: 'MPPerc', key: 'l', label: 'MP%' },
    { prop: 'IfMatch', key: 'm', label: '相遇' },
    { prop: 'WinCount', key: 'n', label: '赢轮数' },
    { prop: 'TieCount', key: 'o', label: '平轮数' },
    { prop: 'LevelScore', key: 'p', label: '级分' },
    { prop: 'OppScore', key: 'q', label: '对手分' }
  ],
  MRWE04: [
    { prop: 'Rank', key: 'a', label: '名次', fixed: 'left' },
    { prop: 'No', key: 'e', label: '编号' },
    { prop: 'Team', key: 'd', label: '单位' },
    { prop: 'Name', key: 'c', label: '姓名' },
    { prop: 'Score', key: 'k1', label: '总积分' },
    { prop: 'MPPerc', key: 'l', label: 'MP%' },
    { prop: 'IfMatch', key: 'm', label: '相遇' },
    { prop: 'WinCount', key: 'n', label: '赢轮数' },
    { prop: 'TieCount', key: 'o', label: '平轮数' },
    { prop: 'LevelScore', key: 'p', label: '级分' },
    { prop: 'OppScore', key: 'q', label: '对手分' }
  ],
  MRWE05: [
    { prop: 'Rank', key: 'a', label: '名次', fixed: 'left' },
    { prop: 'No', key: 'e', label: '编号' },
    { prop: 'Team', key: 'd', label: '单位' },
    { prop: 'Score', key: 'k1', label: '总积分' },
    { prop: 'MPPerc', key: 'l', label: 'MP%' },
    { prop: 'IfMatch', key: 'm', label: '相遇' },
    { prop: 'WinCount', key: 'n', label: '赢轮数' },
    { prop: 'TieCount', key: 'o', label: '平轮数' },
    { prop: 'LevelScore', key: 'p', label: '级分' },
    { prop: 'OppScore', key: 'q', label: '对手分' }
  ],
  MRWE06: [
    { prop: 'Rank', key: 'a', label: '名次', fixed: 'left' },
    { prop: 'No', key: 'e', label: '编号' },
    { prop: 'Team', key: 'd', label: '单位' },
    { prop: 'Name', key: 'c', label: '姓名' },
    { prop: 'Score', key: 'k4', label: '总积分' },
    { prop: 'MPPerc', key: 'l', label: 'MP%' },
    { prop: 'IfMatch', key: 'm', label: '相遇' },
    { prop: 'WinCount', key: 'n', label: '赢轮数' },
    { prop: 'TieCount', key: 'o', label: '平轮数' },
    { prop: 'LevelScore', key: 'p', label: '级分' },
    { prop: 'OppScore', key: 'q', label: '对手分' }
  ]
}
