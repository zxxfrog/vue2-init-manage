import store from 'store2'
/**
 * This is a tool for store objects
 */
export default {
  set (key, value) {
    store.set(key, value)
  },
  get (key, defaultValue) {
    let localValue = store.get(key)
    let sessionValue = store.session.get(key)
    let value = localValue === null ? sessionValue : localValue
    if (value === null) {
      return defaultValue
    }
    return value
  },
  session (key, value) {
    store.session.set(key, value)
  },
  remove (key) {
    store.remove(key)
    store.session.remove(key)
  },
  clearAll () {
    store.clearAll()
    store.session.clearAll()
  }
}
