const requireComponent = require.context(
  // 其组件目录的相对路径
  './',
  // 是否查询其子目录,递归查询
  true,
  // 匹配基础组件文件名的正则表达式
  /.(vue)$/
)

var componentList = []

requireComponent.keys().forEach(item => {
  const componentConfig = requireComponent(item)
  let componentName = item.split('/')[item.split('/').length - 1].split('.')[0]
  if (componentConfig.default) {
    componentList.push({
      name: componentName,
      component: componentConfig.default
    })
  }
})

export default componentList
