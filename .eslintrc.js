module.exports = {
  root: true,
  env: {
    node: true
  },
  extends: ['plugin:vue/essential', 'eslint:recommended', '@vue/prettier'],
  parserOptions: {
    parser: 'babel-eslint'
  },
  rules: {
    'no-console': process.env.VUE_APP_ENV_TYPE === 'production' ? 'error' : 'off',
    'no-debugger': process.env.VUE_APP_ENV_TYPE === 'production' ? 'error' : 'off',
    'no-unused-vars': 'off',
    'no-useless-escape': 'off',
    quotes: [0, 'single', 'double'] //取消单引号，双引号的告警
  }
}
