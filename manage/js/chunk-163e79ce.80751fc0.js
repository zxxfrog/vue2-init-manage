;(window['webpackJsonp'] = window['webpackJsonp'] || []).push([
  ['chunk-163e79ce'],
  {
    '036b': function(e, a, l) {
      'use strict'
      l('8a72')
    },
    '0c94': function(e, a, l) {
      'use strict'
      var t = l('64f5'),
        n = l('4326'),
        m = l('50ec'),
        o = n('species')
      e.exports = function(e) {
        return (
          m >= 51 ||
          !t(function() {
            var a = [],
              l = (a.constructor = {})
            return (
              (l[o] = function() {
                return { foo: 1 }
              }),
              1 !== a[e](Boolean).foo
            )
          })
        )
      }
    },
    '0f81': function(e, a, l) {
      'use strict'
      l('e620')
    },
    1351: function(e, a, l) {
      'use strict'
      l('256e')
    },
    '1baf': function(e, a, l) {},
    '1bf8': function(e, a, l) {
      'use strict'
      l('65a0')
    },
    '1ecb': function(e, a, l) {
      'use strict'
      var t = l('50c2'),
        n = l('a968'),
        m = l('b2d5'),
        o = l('64f5'),
        s = o(function() {
          m(1)
        })
      t(
        { target: 'Object', stat: !0, forced: s },
        {
          keys: function(e) {
            return m(n(e))
          }
        }
      )
    },
    '256e': function(e, a, l) {},
    2674: function(e, a, l) {
      'use strict'
      var t = l('50c2'),
        n = l('ee9c'),
        m = l('a78a'),
        o = l('85ca'),
        s = l('2412'),
        i = l('91c2')
      t(
        { target: 'Object', stat: !0, sham: !n },
        {
          getOwnPropertyDescriptors: function(e) {
            var a,
              l,
              t = o(e),
              n = s.f,
              r = m(t),
              u = {},
              c = 0
            while (r.length > c) (l = n(t, (a = r[c++]))), void 0 !== l && i(u, a, l)
            return u
          }
        }
      )
    },
    '2e1c': function(e, a, l) {
      'use strict'
      l('fa04')
    },
    '429b': function(e, a, l) {
      'use strict'
      l('6dac')
    },
    4989: function(e, a, l) {},
    '49d8': function(e, a, l) {
      'use strict'
      l('ad65')
    },
    '53ed': function(e, a, l) {
      e.exports = l.p + 'img/CC.4c6a671d.png'
    },
    '5d38': function(e, a, l) {
      e.exports = l.p + 'img/GO.5c606467.png'
    },
    '5d77': function(e, a, l) {
      'use strict'
      l.r(a)
      var t = function() {
          var e = this,
            a = e._self._c
          return a(
            'div',
            [
              a('common'),
              a(
                'div',
                { class: ['component-style', 'item'] },
                [
                  '对阵表' === e.sportFileName && '围棋' === e.activeItemName
                    ? [a('GO')]
                    : '对阵表' === e.sportFileName && '国际象棋' === e.activeItemName
                    ? [a('CH')]
                    : '对阵表' === e.sportFileName && '象棋' === e.activeItemName
                    ? [a('CC')]
                    : '对阵表' === e.sportFileName && '国际跳棋' === e.activeItemName
                    ? [a('DR')]
                    : '对阵表' === e.sportFileName && '桥牌' === e.activeItemName
                    ? [a('BR')]
                    : '对阵表' === e.sportFileName && '五子棋' === e.activeItemName
                    ? [a('GB')]
                    : '成绩公告' === e.sportFileName && '围棋' === e.activeItemName
                    ? [a('result-g-o')]
                    : '成绩公告' === e.sportFileName && '国际象棋' === e.activeItemName
                    ? [a('result-c-h')]
                    : '成绩公告' === e.sportFileName && '象棋' === e.activeItemName
                    ? [a('result-c-c')]
                    : '成绩公告' === e.sportFileName && '国际跳棋' === e.activeItemName
                    ? [a('result-d-r')]
                    : '成绩公告' === e.sportFileName && '桥牌' === e.activeItemName
                    ? [a('result-b-r')]
                    : '成绩公告' === e.sportFileName && '五子棋' === e.activeItemName
                    ? [a('result-g-b')]
                    : e._e()
                ],
                2
              )
            ],
            1
          )
        },
        n = [],
        m = l('edb5'),
        o = l('f0a4'),
        s =
          (l('bbd5'),
          l('68fe'),
          l('b8f8'),
          function() {
            var e = this,
              a = e._self._c
            return a('div', [
              a(
                'div',
                { staticClass: 'item-info' },
                [
                  e._l(e.singleItemList, function(l, t) {
                    return a(
                      'div',
                      { key: t, class: ['single-item', l.flag ? 'active-item' : ''] },
                      [
                        a('div', [e._v(e._s(l.name))]),
                        a('div', { staticClass: 'item-logo' }, [
                          a('img', { attrs: { src: l.logo, alt: '' } })
                        ])
                      ]
                    )
                  }),
                  a('div', { staticClass: 'single-item-info' }, [
                    a('div', { staticClass: 'info-line1' }, [
                      a('div', [e._v(e._s(e.gameTitle))]),
                      a('div', { staticClass: 'venue' }, [e._v(e._s(e.venueName))])
                    ]),
                    a('div', { staticClass: 'info-line2' }, [
                      a('div', { staticClass: 'report-name' }, [e._v('成绩公告')]),
                      a('div', [e._v(e._s(e.gameTime))])
                    ])
                  ])
                ],
                2
              ),
              a(
                'div',
                { staticClass: 'game-info' },
                [
                  a('el-col', { attrs: { span: 3 } }, [
                    a('div', [e._v(e._s(e.tableData.column1))])
                  ]),
                  a('el-col', { attrs: { span: 9 } }, [
                    a('div', { staticClass: 'game-a' }, [e._v(e._s(e.tableData.column2))])
                  ]),
                  a('el-col', { attrs: { span: 3 } }, [
                    a('div', { staticClass: 'game-vs' }, [e._v(e._s(e.tableData.column3))])
                  ]),
                  a('el-col', { attrs: { span: 9 } }, [
                    a('div', { staticClass: 'game-b' }, [
                      e._v(' ' + e._s(e.tableData.column4) + ' ')
                    ])
                  ])
                ],
                1
              ),
              a(
                'div',
                { staticClass: 'game-table-data' },
                [
                  a(
                    'el-row',
                    { attrs: { gutter: 24 } },
                    [
                      a(
                        'el-col',
                        { staticClass: 'col-style-1', attrs: { span: 3 } },
                        [
                          a(
                            'el-table',
                            { staticStyle: { width: '100%' }, attrs: { data: e.startList } },
                            [
                              a('el-table-column', { attrs: { prop: 'GROUP', label: '组别' } }),
                              a('el-table-column', { attrs: { prop: 'TABLE', label: '桌号' } })
                            ],
                            1
                          )
                        ],
                        1
                      ),
                      a(
                        'el-col',
                        { staticClass: 'col-style', attrs: { span: 9 } },
                        [
                          a(
                            'el-table',
                            { staticStyle: { width: '100%' }, attrs: { data: e.startList } },
                            [
                              a('el-table-column', { attrs: { prop: 'NUMBER_B', label: '编号' } }),
                              a('el-table-column', {
                                attrs: { label: '姓名', 'min-width': '130%' },
                                scopedSlots: e._u([
                                  {
                                    key: 'default',
                                    fn: function(l) {
                                      return [
                                        a('div', { staticStyle: { color: 'rgba(230,24,71,1)' } }, [
                                          e._v(' ' + e._s(l.row.NAME_B) + ' ')
                                        ])
                                      ]
                                    }
                                  }
                                ])
                              }),
                              a('el-table-column', { attrs: { prop: 'UNIT_B', label: '单位' } }),
                              a('el-table-column', {
                                attrs: {
                                  prop: 'SCORE_B',
                                  label: '积分',
                                  'header-align': 'center',
                                  align: 'center'
                                }
                              })
                            ],
                            1
                          )
                        ],
                        1
                      ),
                      a(
                        'el-col',
                        { staticClass: 'col-style', attrs: { span: 3 } },
                        [
                          a(
                            'el-table',
                            {
                              staticStyle: { width: '100%' },
                              attrs: { data: e.startList, 'header-align': 'center' }
                            },
                            [
                              a('el-table-column', {
                                attrs: { prop: '', 'header-align': 'center' },
                                scopedSlots: e._u([
                                  {
                                    key: 'header',
                                    fn: function(e) {
                                      return [a('div', { staticClass: 'game-vs-logo' })]
                                    }
                                  }
                                ])
                              })
                            ],
                            1
                          )
                        ],
                        1
                      ),
                      a(
                        'el-col',
                        { staticClass: 'col-style-2', attrs: { span: 9 } },
                        [
                          a(
                            'el-table',
                            { staticStyle: { width: '100%' }, attrs: { data: e.startList } },
                            [
                              a('el-table-column', {
                                attrs: {
                                  prop: 'SCORE_W',
                                  label: '积分',
                                  'header-align': 'center',
                                  align: 'center'
                                }
                              }),
                              a('el-table-column', { attrs: { prop: 'NUMBER_W', label: '编号' } }),
                              a('el-table-column', {
                                attrs: { prop: 'NAME_W', label: '姓名', 'min-width': '130%' },
                                scopedSlots: e._u([
                                  {
                                    key: 'default',
                                    fn: function(l) {
                                      return [
                                        a('div', { staticStyle: { color: 'rgba(60,135,247,1)' } }, [
                                          e._v(' ' + e._s(l.row.NAME_W) + ' ')
                                        ])
                                      ]
                                    }
                                  }
                                ])
                              }),
                              a('el-table-column', { attrs: { prop: 'UNIT_W', label: '单位' } })
                            ],
                            1
                          )
                        ],
                        1
                      )
                    ],
                    1
                  )
                ],
                1
              )
            ])
          }),
        i = [],
        r = {
          created: function() {
            ;(this.venueName = this.$route.query.venueName),
              (this.gameTime = this.$route.query.gameTime),
              (this.gameTitle = this.$route.query.gameTitle)
          },
          props: {},
          data: function() {
            return {
              tableData: { column1: '对阵', column2: '黑棋', column3: 'VS', column4: '白棋' },
              venueName: '',
              gameTime: '',
              gameTitle: '',
              startList: [
                {
                  GROUP: 'A',
                  TABLE: '1',
                  NUMBER_B: '1',
                  NAME_B: '李轩豪 ',
                  SCORE_B: '4',
                  UNIT_B: '重庆',
                  SCORE_W: '4',
                  NUMBER_W: '5',
                  NAME_W: '张  强 ',
                  UNIT_W: '西藏'
                },
                {
                  GROUP: '',
                  TABLE: '2',
                  NUMBER_B: '11',
                  NAME_B: ' 柁嘉熹 ',
                  SCORE_B: '4',
                  UNIT_B: '黑龙江',
                  SCORE_W: '4',
                  NUMBER_W: '14',
                  NAME_W: ' 范廷钰 ',
                  UNIT_W: '山东'
                },
                {
                  GROUP: '',
                  TABLE: '3',
                  NUMBER_B: '15',
                  NAME_B: ' 李  喆 ',
                  SCORE_B: '4',
                  UNIT_B: '湖北',
                  SCORE_W: '4',
                  NUMBER_W: '23',
                  NAME_W: ' 韩一洲 ',
                  UNIT_W: '北京'
                },
                {
                  GROUP: '',
                  TABLE: '4',
                  NUMBER_B: '2',
                  NAME_B: ' 李建宇 ',
                  SCORE_B: '2',
                  UNIT_B: '河北',
                  SCORE_W: '4',
                  NUMBER_W: '26',
                  NAME_W: ' 李钦诚 ',
                  UNIT_W: '浙江'
                },
                {
                  GROUP: '',
                  TABLE: '5',
                  NUMBER_B: '6',
                  NAME_B: ' 王星昊 ',
                  SCORE_B: '2',
                  UNIT_B: '上海',
                  SCORE_W: '2',
                  NUMBER_W: '3',
                  NAME_W: ' 韦一博 ',
                  UNIT_W: '厦门'
                },
                {
                  GROUP: '',
                  TABLE: '6',
                  NUMBER_B: '9',
                  NAME_B: ' 柯  洁 ',
                  SCORE_B: '2',
                  UNIT_B: '云南',
                  SCORE_W: '2',
                  NUMBER_W: '7',
                  NAME_W: ' 陈一纯 ',
                  UNIT_W: '西藏'
                },
                {
                  GROUP: '',
                  TABLE: '7',
                  NUMBER_B: '10',
                  NAME_B: ' 廖元赫 ',
                  SCORE_B: '2',
                  UNIT_B: '四川',
                  SCORE_W: '2',
                  NUMBER_W: '16',
                  NAME_W: ' 焦士维 ',
                  UNIT_W: '云南'
                },
                {
                  GROUP: '',
                  TABLE: '8',
                  NUMBER_B: '17',
                  NAME_B: ' 甘思阳 ',
                  SCORE_B: '2',
                  UNIT_B: '深圳',
                  SCORE_W: '2',
                  NUMBER_W: '20',
                  NAME_W: ' 陈  贤 ',
                  UNIT_W: '江苏'
                },
                {
                  GROUP: '',
                  TABLE: '9',
                  NUMBER_B: '18',
                  NAME_B: ' 李雨昂 ',
                  SCORE_B: '2',
                  UNIT_B: '辽宁',
                  SCORE_W: '2',
                  NUMBER_W: '21',
                  NAME_W: ' 杨宗煜 ',
                  UNIT_W: '北京'
                },
                {
                  GROUP: '',
                  TABLE: '10',
                  NUMBER_B: '22',
                  NAME_B: ' 马逸超 ',
                  SCORE_B: '2',
                  UNIT_B: '四川',
                  SCORE_W: '2',
                  NUMBER_W: '27',
                  NAME_W: ' 程宏昊 ',
                  UNIT_W: '广东'
                },
                {
                  GROUP: '',
                  TABLE: '11',
                  NUMBER_B: '4',
                  NAME_B: ' 张家尧 ',
                  SCORE_B: '0',
                  UNIT_B: '内蒙古',
                  SCORE_W: '2',
                  NUMBER_W: '28',
                  NAME_W: ' 张亚博 ',
                  UNIT_W: '陕西'
                },
                {
                  GROUP: '',
                  TABLE: '12',
                  NUMBER_B: '8',
                  NAME_B: ' 张浩伟 ',
                  SCORE_B: '0',
                  UNIT_B: '安徽',
                  SCORE_W: '0',
                  NUMBER_W: '12',
                  NAME_W: ' 陈昱森 ',
                  UNIT_W: '广东'
                },
                {
                  GROUP: '',
                  TABLE: '13',
                  NUMBER_B: '13',
                  NAME_B: ' 黄春棋 ',
                  SCORE_B: '0',
                  UNIT_B: '河南',
                  SCORE_W: '0',
                  NUMBER_W: '19',
                  NAME_W: ' 薛冠华 ',
                  UNIT_W: '河北'
                },
                {
                  GROUP: '',
                  TABLE: '14',
                  NUMBER_B: '24',
                  NAME_B: '王音灵水',
                  SCORE_B: '0',
                  UNIT_B: '新疆',
                  SCORE_W: '0',
                  NUMBER_W: '25',
                  NAME_W: ' 徐泽鑫',
                  UNIT_W: '煤矿体协'
                },
                {
                  GROUP: 'B',
                  TABLE: '1',
                  NUMBER_B: '4',
                  NAME_B: '时  越 ',
                  SCORE_B: '4',
                  UNIT_B: '上海',
                  SCORE_W: '4',
                  NUMBER_W: '5',
                  NAME_W: '石豫来 ',
                  UNIT_W: '陕西'
                },
                {
                  GROUP: '',
                  TABLE: '2',
                  NUMBER_B: '13',
                  NAME_B: ' 舒一笑 ',
                  SCORE_B: '4',
                  UNIT_B: '煤矿体协',
                  SCORE_W: '4',
                  NUMBER_W: '18',
                  NAME_W: ' 何语涵 ',
                  UNIT_W: '重庆'
                },
                {
                  GROUP: '',
                  TABLE: '3',
                  NUMBER_B: '20',
                  NAME_B: ' 柳琪峰 ',
                  SCORE_B: '4',
                  UNIT_B: '云南',
                  SCORE_W: '4',
                  NUMBER_W: '26',
                  NAME_W: ' 江维杰 ',
                  UNIT_W: '山东'
                },
                {
                  GROUP: '',
                  TABLE: '4',
                  NUMBER_B: '1',
                  NAME_B: ' 陈梓健 ',
                  SCORE_B: '2',
                  UNIT_B: '山东',
                  SCORE_W: '4',
                  NUMBER_W: '28',
                  NAME_W: ' 赵晨宇 ',
                  UNIT_W: '江苏'
                },
                {
                  GROUP: '',
                  TABLE: '5',
                  NUMBER_B: '3',
                  NAME_B: ' 陈  浩 ',
                  SCORE_B: '2',
                  UNIT_B: '西藏',
                  SCORE_W: '2',
                  NUMBER_W: '6',
                  NAME_W: ' 谢  科 ',
                  UNIT_W: '浙江'
                },
                {
                  GROUP: '',
                  TABLE: '6',
                  NUMBER_B: '7',
                  NAME_B: ' 李成森 ',
                  SCORE_B: '2',
                  UNIT_B: '煤矿体协',
                  SCORE_W: '2',
                  NUMBER_W: '9',
                  NAME_W: ' 刘兆哲 ',
                  UNIT_W: '河南'
                },
                {
                  GROUP: '',
                  TABLE: '7',
                  NUMBER_B: '8',
                  NAME_B: ' 许瀚文 ',
                  SCORE_B: '2',
                  UNIT_B: '广东',
                  SCORE_W: '2',
                  NUMBER_W: '12',
                  NAME_W: ' 陈豪鑫 ',
                  UNIT_W: '厦门'
                },
                {
                  GROUP: '',
                  TABLE: '8',
                  NUMBER_B: '15',
                  NAME_B: ' 唐韦星 ',
                  SCORE_B: '2',
                  UNIT_B: '江苏',
                  SCORE_W: '2',
                  NUMBER_W: '19',
                  NAME_W: ' 杨文铠 ',
                  UNIT_W: '北京'
                },
                {
                  GROUP: '',
                  TABLE: '9',
                  NUMBER_B: '16',
                  NAME_B: ' 郭信驿 ',
                  SCORE_B: '2',
                  UNIT_B: '新疆',
                  SCORE_W: '2',
                  NUMBER_W: '21',
                  NAME_W: ' 严在明 ',
                  UNIT_W: '黑龙江'
                },
                {
                  GROUP: '',
                  TABLE: '10',
                  NUMBER_B: '22',
                  NAME_B: ' 陈  磊 ',
                  SCORE_B: '2',
                  UNIT_B: '陕西',
                  SCORE_W: '2',
                  NUMBER_W: '23',
                  NAME_W: ' 杨鼎新 ',
                  UNIT_W: '重庆'
                },
                {
                  GROUP: '',
                  TABLE: '11',
                  NUMBER_B: '2',
                  NAME_B: ' 冯  昊 ',
                  SCORE_B: '0',
                  UNIT_B: '河北',
                  SCORE_W: '2',
                  NUMBER_W: '24',
                  NAME_W: ' 童梦成 ',
                  UNIT_W: '浙江'
                },
                {
                  GROUP: '',
                  TABLE: '12',
                  NUMBER_B: '10',
                  NAME_B: ' 黄明宇 ',
                  SCORE_B: '0',
                  UNIT_B: '上海',
                  SCORE_W: '0',
                  NUMBER_W: '11',
                  NAME_W: ' 黄一鸣 ',
                  UNIT_W: '深圳'
                },
                {
                  GROUP: '',
                  TABLE: '13',
                  NUMBER_B: '14',
                  NAME_B: ' 周玉川 ',
                  SCORE_B: '0',
                  UNIT_B: '大连',
                  SCORE_W: '0',
                  NUMBER_W: '17',
                  NAME_W: ' 陈必森 ',
                  UNIT_W: '海南'
                },
                {
                  GROUP: '',
                  TABLE: '14',
                  NUMBER_B: '25',
                  NAME_B: ' 郑载想',
                  SCORE_B: '0',
                  UNIT_B: '四川',
                  SCORE_W: '0',
                  NUMBER_W: '27',
                  NAME_W: ' 韩卓然',
                  UNIT_W: '厦门'
                }
              ],
              singleItemList: [{ name: '围棋', logo: l('5d38'), flag: !0 }]
            }
          },
          watch: {},
          computed: {},
          filters: {},
          components: {},
          methods: {}
        },
        u = r,
        c = (l('49d8'), l('76b2')),
        _ = Object(c['a'])(u, s, i, !1, null, 'eed04f82', null),
        N = _.exports,
        B = function() {
          var e = this,
            a = e._self._c
          return a('div', [
            a(
              'div',
              { staticClass: 'item-info' },
              [
                e._l(e.singleItemList, function(l, t) {
                  return a('div', { key: t, class: ['single-item', l.flag ? 'active-item' : ''] }, [
                    a('div', [e._v(e._s(l.name))]),
                    a('div', { staticClass: 'item-logo' }, [
                      a('img', { attrs: { src: l.logo, alt: '' } })
                    ])
                  ])
                }),
                a('div', { staticClass: 'single-item-info' }, [
                  a('div', { staticClass: 'info-line1' }, [
                    a('div', [e._v(e._s(e.gameTitle))]),
                    a('div', { staticClass: 'venue' }, [e._v(e._s(e.venueName))])
                  ]),
                  a('div', { staticClass: 'info-line2' }, [
                    a('div', { staticClass: 'report-name' }, [e._v('成绩公告')]),
                    a('div', [e._v(e._s(e.gameTime))])
                  ])
                ])
              ],
              2
            ),
            a(
              'div',
              { staticClass: 'game-info' },
              [
                a('el-col', { attrs: { span: 3 } }, [a('div', [e._v(e._s(e.tableData.column1))])]),
                a('el-col', { attrs: { span: 9 } }, [
                  a('div', { staticClass: 'game-b', staticStyle: { 'margin-left': '-10px' } }, [
                    e._v(e._s(e.tableData.column2))
                  ])
                ]),
                a('el-col', { attrs: { span: 3 } }, [
                  a('div', { staticClass: 'game-vs' }, [e._v(e._s(e.tableData.column3))])
                ]),
                a('el-col', { attrs: { span: 9 } }, [
                  a('div', { staticClass: 'game-a' }, [e._v(' ' + e._s(e.tableData.column4) + ' ')])
                ])
              ],
              1
            ),
            a(
              'div',
              { staticClass: 'game-table-data' },
              [
                a(
                  'el-row',
                  { attrs: { gutter: 24 } },
                  [
                    a(
                      'el-col',
                      { staticClass: 'col-style-1', attrs: { span: 3 } },
                      [
                        a(
                          'el-table',
                          { staticStyle: { width: '100%' }, attrs: { data: e.startList } },
                          [
                            a('el-table-column', {
                              attrs: {
                                prop: 'TableNo',
                                label: '桌号',
                                'header-align': 'center',
                                align: 'center'
                              }
                            })
                          ],
                          1
                        )
                      ],
                      1
                    ),
                    a(
                      'el-col',
                      { staticClass: 'col-style', attrs: { span: 9 } },
                      [
                        a(
                          'el-table',
                          { staticStyle: { width: '100%' }, attrs: { data: e.startList } },
                          [
                            a('el-table-column', { attrs: { prop: 'BibA', label: '号码' } }),
                            a('el-table-column', {
                              attrs: { label: '姓名', 'min-width': '130%' },
                              scopedSlots: e._u([
                                {
                                  key: 'default',
                                  fn: function(l) {
                                    return [
                                      a('div', { staticStyle: { color: 'rgba(230,24,71,1)' } }, [
                                        e._v(' ' + e._s(l.row.NameA) + ' ')
                                      ])
                                    ]
                                  }
                                }
                              ])
                            }),
                            a('el-table-column', { attrs: { prop: 'TeamA', label: '单位' } }),
                            a('el-table-column', {
                              attrs: {
                                prop: 'ScoreA',
                                label: '积分',
                                'header-align': 'center',
                                align: 'center'
                              }
                            })
                          ],
                          1
                        )
                      ],
                      1
                    ),
                    a(
                      'el-col',
                      { staticClass: 'col-style', attrs: { span: 3 } },
                      [
                        a(
                          'el-table',
                          {
                            staticStyle: { width: '100%' },
                            attrs: { data: e.startList, 'header-align': 'center' }
                          },
                          [
                            a('el-table-column', {
                              attrs: { prop: '', 'header-align': 'center' },
                              scopedSlots: e._u([
                                {
                                  key: 'header',
                                  fn: function(e) {
                                    return [a('div', { staticClass: 'game-vs-logo' })]
                                  }
                                }
                              ])
                            })
                          ],
                          1
                        )
                      ],
                      1
                    ),
                    a(
                      'el-col',
                      { staticClass: 'col-style-2', attrs: { span: 9 } },
                      [
                        a(
                          'el-table',
                          { staticStyle: { width: '100%' }, attrs: { data: e.startList } },
                          [
                            a('el-table-column', {
                              attrs: {
                                prop: 'ScoreB',
                                label: '积分',
                                'header-align': 'center',
                                align: 'center'
                              }
                            }),
                            a('el-table-column', {
                              attrs: { label: '姓名', 'min-width': '130%' },
                              scopedSlots: e._u([
                                {
                                  key: 'default',
                                  fn: function(l) {
                                    return [
                                      a('div', { staticStyle: { color: 'rgba(60,135,247,1)' } }, [
                                        e._v(' ' + e._s(l.row.NameB) + ' ')
                                      ])
                                    ]
                                  }
                                }
                              ])
                            }),
                            a('el-table-column', { attrs: { prop: 'TeamB', label: '单位' } }),
                            a('el-table-column', { attrs: { prop: 'BibB', label: '号码' } })
                          ],
                          1
                        )
                      ],
                      1
                    )
                  ],
                  1
                )
              ],
              1
            )
          ])
        },
        T = [],
        R = {
          created: function() {
            ;(this.venueName = this.$route.query.venueName),
              (this.gameTime = this.$route.query.gameTime),
              (this.gameTitle = this.$route.query.gameTitle)
          },
          props: {},
          data: function() {
            return {
              tableData: { column1: '对阵', column2: '白方', column3: 'VS', column4: '黑方' },
              venueName: '',
              gameTime: '',
              gameTitle: '',
              startList: [
                {
                  TableNo: '1',
                  BibA: '35',
                  NameA: '房岩',
                  TeamA: '湖北',
                  ScoreA: '0',
                  Result: null,
                  ScoreB: '0',
                  NameB: '丁立人',
                  TeamB: '浙江',
                  BibB: '1'
                },
                {
                  TableNo: '2',
                  BibA: '2',
                  NameA: '余泱漪',
                  TeamA: '北京',
                  ScoreA: '0',
                  Result: null,
                  ScoreB: '0',
                  NameB: '鲍麒麟',
                  TeamB: '黑龙江',
                  BibB: '36'
                },
                {
                  TableNo: '3',
                  BibA: '37',
                  NameA: '张子洋',
                  TeamA: '河南',
                  ScoreA: '0',
                  Result: null,
                  ScoreB: '0',
                  NameB: '王皓',
                  TeamB: '黑龙江',
                  BibB: '3'
                },
                {
                  TableNo: '4',
                  BibA: '4',
                  NameA: '卜祥志',
                  TeamA: '山东',
                  ScoreA: '0',
                  Result: null,
                  ScoreB: '0',
                  NameB: '牟科',
                  TeamB: '青岛',
                  BibB: '38'
                },
                {
                  TableNo: '5',
                  BibA: '39',
                  NameA: '徐铭辉',
                  TeamA: '浙江',
                  ScoreA: '0',
                  Result: null,
                  ScoreB: '0',
                  NameB: '韦奕',
                  TeamB: '江苏',
                  BibB: '5'
                },
                {
                  TableNo: '6',
                  BibA: '6',
                  NameA: '李超',
                  TeamA: '北京',
                  ScoreA: '0',
                  Result: null,
                  ScoreB: '0',
                  NameB: '赵晨曦',
                  TeamB: '广东',
                  BibB: '40'
                },
                {
                  TableNo: '7',
                  BibA: '41',
                  NameA: '林卫国',
                  TeamA: '湖北',
                  ScoreA: '0',
                  Result: null,
                  ScoreB: '0',
                  NameB: '王玥',
                  TeamB: '天津',
                  BibB: '7'
                },
                {
                  TableNo: '8',
                  BibA: '8',
                  NameA: '倪华',
                  TeamA: '上海',
                  ScoreA: '0',
                  Result: null,
                  ScoreB: '0',
                  NameB: '刘钊杞',
                  TeamB: '河北',
                  BibB: '42'
                },
                {
                  TableNo: '9',
                  BibA: '43',
                  NameA: '许亦柠',
                  TeamA: '上海',
                  ScoreA: '0',
                  Result: null,
                  ScoreB: '0',
                  NameB: '赵骏',
                  TeamB: '山东',
                  BibB: '9'
                },
                {
                  TableNo: '10',
                  BibA: '10',
                  NameA: '马群',
                  TeamA: '浙江',
                  ScoreA: '0',
                  Result: null,
                  ScoreB: '0',
                  NameB: '周江南',
                  TeamB: '青岛',
                  BibB: '44'
                },
                {
                  TableNo: '11',
                  BibA: '45',
                  NameA: '彭红墀',
                  TeamA: '河北',
                  ScoreA: '0',
                  Result: null,
                  ScoreB: '0',
                  NameB: '周健超',
                  TeamB: '上海',
                  BibB: '11'
                },
                {
                  TableNo: '12',
                  BibA: '12',
                  NameA: '卢尚磊',
                  TeamA: '浙江',
                  ScoreA: '0',
                  Result: null,
                  ScoreB: '0',
                  NameB: '范会丰',
                  TeamB: '大连',
                  BibB: '46'
                },
                {
                  TableNo: '13',
                  BibA: '47',
                  NameA: '赵洲桥',
                  TeamA: '天津',
                  ScoreA: '0',
                  Result: null,
                  ScoreB: '0',
                  NameB: '周唯奇',
                  TeamB: '重庆',
                  BibB: '13'
                },
                {
                  TableNo: '14',
                  BibA: '14',
                  NameA: '温阳',
                  TeamA: '山东',
                  ScoreA: '0',
                  Result: null,
                  ScoreB: '0',
                  NameB: '张家宝',
                  TeamB: '甘肃',
                  BibB: '48'
                },
                {
                  TableNo: '15',
                  BibA: '49',
                  NameA: '吴玺斌',
                  TeamA: '黑龙江',
                  ScoreA: '0',
                  Result: null,
                  ScoreB: '0',
                  NameB: '张鹏翔',
                  TeamB: '河北',
                  BibB: '15'
                },
                {
                  TableNo: '16',
                  BibA: '16',
                  NameA: '徐英伦',
                  TeamA: '广东',
                  ScoreA: '0',
                  Result: null,
                  ScoreB: '0',
                  NameB: '马麟',
                  TeamB: '甘肃',
                  BibB: '50'
                },
                {
                  TableNo: '17',
                  BibA: '51',
                  NameA: '李国豪',
                  TeamA: '甘肃',
                  ScoreA: '0',
                  Result: null,
                  ScoreB: '0',
                  NameB: '曾重生',
                  TeamB: '重庆',
                  BibB: '17'
                },
                {
                  TableNo: '18',
                  BibA: '18',
                  NameA: '李荻',
                  TeamA: '四川',
                  ScoreA: '0',
                  Result: null,
                  ScoreB: '0',
                  NameB: '张晟昀',
                  TeamB: '四川',
                  BibB: '52'
                },
                {
                  TableNo: '19',
                  BibA: '53',
                  NameA: '孙超',
                  TeamA: '甘肃',
                  ScoreA: '0',
                  Result: null,
                  ScoreB: '0',
                  NameB: '刘庆南',
                  TeamB: '山东',
                  BibB: '19'
                },
                {
                  TableNo: '20',
                  BibA: '20',
                  NameA: '修德顺',
                  TeamA: '北京',
                  ScoreA: '0',
                  Result: null,
                  ScoreB: '0',
                  NameB: '林培森',
                  TeamB: '广东',
                  BibB: '54'
                },
                {
                  TableNo: '21',
                  BibA: '55',
                  NameA: '张胤哲',
                  TeamA: '河南',
                  ScoreA: '0',
                  Result: null,
                  ScoreB: '0',
                  NameB: '刘言',
                  TeamB: '重庆',
                  BibB: '21'
                },
                {
                  TableNo: '22',
                  BibA: '22',
                  NameA: '戴常人',
                  TeamA: '天津',
                  ScoreA: '0',
                  Result: null,
                  ScoreB: '0',
                  NameB: '恩和',
                  TeamB: '内蒙古',
                  BibB: '56'
                },
                {
                  TableNo: '23',
                  BibA: '57',
                  NameA: '白巴特尔',
                  TeamA: '内蒙古',
                  ScoreA: '0',
                  Result: null,
                  ScoreB: '0',
                  NameB: '万云国',
                  TeamB: '河北',
                  BibB: '23'
                },
                {
                  TableNo: '24',
                  BibA: '24',
                  NameA: '徐志行',
                  TeamA: '青岛',
                  ScoreA: '0',
                  Result: null,
                  ScoreB: '0',
                  NameB: '陈瑜亮',
                  TeamB: '海南',
                  BibB: '58'
                },
                {
                  TableNo: '25',
                  BibA: '59',
                  NameA: '呼木吉勒图',
                  TeamA: '内蒙古',
                  ScoreA: '0',
                  Result: null,
                  ScoreB: '0',
                  NameB: '余瑞源',
                  TeamB: '江苏',
                  BibB: '25'
                },
                {
                  TableNo: '26',
                  BibA: '26',
                  NameA: '林晨',
                  TeamA: '江苏',
                  ScoreA: '0',
                  Result: null,
                  ScoreB: '0',
                  NameB: '刘将',
                  TeamB: '西藏',
                  BibB: '60'
                },
                {
                  TableNo: '27',
                  BibA: '61',
                  NameA: '鲁宽',
                  TeamA: '江西',
                  ScoreA: '0',
                  Result: null,
                  ScoreB: '0',
                  NameB: '汪自力',
                  TeamB: '深圳',
                  BibB: '27'
                },
                {
                  TableNo: '28',
                  BibA: '28',
                  NameA: '王晨',
                  TeamA: '重庆',
                  ScoreA: '0',
                  Result: null,
                  ScoreB: '0',
                  NameB: '史一凯',
                  TeamB: '宁波',
                  BibB: '62'
                },
                {
                  TableNo: '29',
                  BibA: '63',
                  NameA: '图木尔巴根',
                  TeamA: '内蒙古',
                  ScoreA: '0',
                  Result: null,
                  ScoreB: '0',
                  NameB: '马中涵',
                  TeamB: '青岛',
                  BibB: '29'
                },
                {
                  TableNo: '30',
                  BibA: '30',
                  NameA: '楼一平',
                  TeamA: '上海',
                  ScoreA: '0',
                  Result: null,
                  ScoreB: '0',
                  NameB: '王飞',
                  TeamB: '河南',
                  BibB: '64'
                },
                {
                  TableNo: '31',
                  BibA: '65',
                  NameA: '王鸿',
                  TeamA: '江西',
                  ScoreA: '0',
                  Result: null,
                  ScoreB: '0',
                  NameB: '王一业',
                  TeamB: '黑龙江',
                  BibB: '31'
                },
                {
                  TableNo: '32',
                  BibA: '32',
                  NameA: '李师龙',
                  TeamA: '广东',
                  ScoreA: '0',
                  Result: null,
                  ScoreB: '0',
                  NameB: '汪进',
                  TeamB: '江西',
                  BibB: '66'
                },
                {
                  TableNo: '33',
                  BibA: '67',
                  NameA: '张海威',
                  TeamA: '福建',
                  ScoreA: '0',
                  Result: null,
                  ScoreB: '0',
                  NameB: '陈琪',
                  TeamB: '江苏',
                  BibB: '33'
                },
                {
                  TableNo: '34',
                  BibA: '34',
                  NameA: '刘冠初',
                  TeamA: '四川',
                  ScoreA: '0',
                  Result: null,
                  ScoreB: '0',
                  NameB: '张少峰',
                  TeamB: '青海',
                  BibB: '68'
                },
                {
                  TableNo: '',
                  BibA: '69',
                  NameA: '邹智平',
                  TeamA: '江西',
                  ScoreA: '0',
                  Result: null,
                  ScoreB: null,
                  NameB: 'Bye',
                  TeamB: null,
                  BibB: null
                }
              ],
              singleItemList: [{ name: '国际象棋', logo: l('c6de0'), flag: !0 }]
            }
          },
          watch: {},
          computed: {},
          filters: {},
          components: {},
          methods: {}
        },
        b = R,
        d = (l('1bf8'), Object(c['a'])(b, B, T, !1, null, 'e75dc892', null)),
        p = d.exports,
        A = function() {
          var e = this,
            a = e._self._c
          return a('div', [
            a(
              'div',
              { staticClass: 'item-info' },
              [
                e._l(e.singleItemList, function(l, t) {
                  return a('div', { key: t, class: ['single-item', l.flag ? 'active-item' : ''] }, [
                    a('div', [e._v(e._s(l.name))]),
                    a('div', { staticClass: 'item-logo' }, [
                      a('img', { attrs: { src: l.logo, alt: '' } })
                    ])
                  ])
                }),
                a('div', { staticClass: 'single-item-info' }, [
                  a('div', { staticClass: 'info-line1' }, [
                    a('div', [e._v(e._s(e.gameTitle))]),
                    a('div', { staticClass: 'venue' }, [e._v(e._s(e.venueName))])
                  ]),
                  a('div', { staticClass: 'info-line2' }, [
                    a('div', { staticClass: 'report-name' }, [e._v('成绩公告')]),
                    a('div', [e._v(e._s(e.gameTime))])
                  ])
                ])
              ],
              2
            ),
            a(
              'div',
              { staticClass: 'game-info' },
              [
                a('el-col', { attrs: { span: 3 } }, [a('div', [e._v(e._s(e.tableData.column1))])]),
                a('el-col', { attrs: { span: 9 } }, [
                  a(
                    'div',
                    {
                      staticClass: 'game-b',
                      staticStyle: { 'margin-left': '-10px', color: 'red' }
                    },
                    [e._v(e._s(e.tableData.column2))]
                  )
                ]),
                a('el-col', { attrs: { span: 3 } }, [
                  a('div', { staticClass: 'game-vs' }, [e._v(e._s(e.tableData.column3))])
                ]),
                a('el-col', { attrs: { span: 9 } }, [
                  a('div', { staticClass: 'game-a' }, [e._v(' ' + e._s(e.tableData.column4) + ' ')])
                ])
              ],
              1
            ),
            a(
              'div',
              { staticClass: 'game-table-data' },
              [
                a(
                  'el-row',
                  { attrs: { gutter: 24 } },
                  [
                    a(
                      'el-col',
                      { staticClass: 'col-style-1', attrs: { span: 3 } },
                      [
                        a(
                          'el-table',
                          { staticStyle: { width: '100%' }, attrs: { data: e.startList } },
                          [
                            a('el-table-column', {
                              attrs: {
                                prop: 'TableNo',
                                label: '台次',
                                'header-align': 'center',
                                align: 'center'
                              }
                            })
                          ],
                          1
                        )
                      ],
                      1
                    ),
                    a(
                      'el-col',
                      { staticClass: 'col-style', attrs: { span: 9 } },
                      [
                        a(
                          'el-table',
                          { staticStyle: { width: '100%' }, attrs: { data: e.startList } },
                          [
                            a('el-table-column', { attrs: { prop: 'Code_R', label: '编号' } }),
                            a('el-table-column', {
                              attrs: { prop: 'Unit_R', label: '单位', 'min-width': '130%' }
                            }),
                            a('el-table-column', {
                              attrs: { label: '姓名' },
                              scopedSlots: e._u([
                                {
                                  key: 'default',
                                  fn: function(l) {
                                    return [
                                      a('div', { staticStyle: { color: 'rgba(230,24,71,1)' } }, [
                                        e._v(' ' + e._s(l.row.Name_R) + ' ')
                                      ])
                                    ]
                                  }
                                }
                              ])
                            }),
                            a('el-table-column', {
                              attrs: {
                                prop: 'Grade_R',
                                label: '前轮积分',
                                'header-align': 'center',
                                align: 'center'
                              }
                            })
                          ],
                          1
                        )
                      ],
                      1
                    ),
                    a(
                      'el-col',
                      { staticClass: 'col-style', attrs: { span: 3 } },
                      [
                        a(
                          'el-table',
                          {
                            staticStyle: { width: '100%' },
                            attrs: { data: e.startList, 'header-align': 'center' }
                          },
                          [
                            a('el-table-column', {
                              attrs: { prop: '', 'header-align': 'center' },
                              scopedSlots: e._u([
                                {
                                  key: 'header',
                                  fn: function(e) {
                                    return [a('div', { staticClass: 'game-vs-logo' })]
                                  }
                                }
                              ])
                            })
                          ],
                          1
                        )
                      ],
                      1
                    ),
                    a(
                      'el-col',
                      { staticClass: 'col-style-2', attrs: { span: 9 } },
                      [
                        a(
                          'el-table',
                          { staticStyle: { width: '100%' }, attrs: { data: e.startList } },
                          [
                            a('el-table-column', {
                              attrs: {
                                prop: 'Grade_B',
                                label: '前轮积分',
                                'header-align': 'center',
                                align: 'center'
                              }
                            }),
                            a('el-table-column', {
                              attrs: { label: '姓名' },
                              scopedSlots: e._u([
                                {
                                  key: 'default',
                                  fn: function(l) {
                                    return [
                                      a('div', { staticStyle: { color: 'rgba(60,135,247,1)' } }, [
                                        e._v(' ' + e._s(l.row.Name_B) + ' ')
                                      ])
                                    ]
                                  }
                                }
                              ])
                            }),
                            a('el-table-column', {
                              attrs: { prop: 'Unit_B', label: '单位', 'min-width': '130%' }
                            }),
                            a('el-table-column', { attrs: { prop: 'Code_B', label: '编号' } })
                          ],
                          1
                        )
                      ],
                      1
                    )
                  ],
                  1
                )
              ],
              1
            )
          ])
        },
        S = [],
        g = {
          created: function() {
            ;(this.venueName = this.$route.query.venueName),
              (this.gameTime = this.$route.query.gameTime),
              (this.gameTitle = this.$route.query.gameTitle)
          },
          props: {},
          data: function() {
            return {
              venueName: '',
              gameTime: '',
              gameTitle: '',
              tableData: { column1: '对阵', column2: '红方', column3: 'VS', column4: '黑方' },
              startList: [
                {
                  TableNo: '1',
                  Code_R: '1',
                  Unit_R: '内蒙古',
                  Name_R: '蔚  强',
                  Grade_R: null,
                  Grade_B: null,
                  Name_B: '宋国强',
                  Unit_B: '火车头体协',
                  Code_B: '2',
                  Score_R: '2.0',
                  Score_B: '0.0'
                },
                {
                  TableNo: '2',
                  Code_R: '4',
                  Unit_R: '新疆',
                  Name_R: '崔淞博',
                  Grade_R: null,
                  Grade_B: null,
                  Name_B: '连泽特',
                  Unit_B: '石油体协',
                  Code_B: '3',
                  Score_R: '0.0',
                  Score_B: '2.0'
                },
                {
                  TableNo: '3',
                  Code_R: '5',
                  Unit_R: '四川',
                  Name_R: '孟  辰',
                  Grade_R: null,
                  Grade_B: null,
                  Name_B: '周  军',
                  Unit_B: '山西',
                  Code_B: '6',
                  Score_R: '2.0',
                  Score_B: '0.0'
                },
                {
                  TableNo: '4',
                  Code_R: '8',
                  Unit_R: '甘肃',
                  Name_R: '何  刚',
                  Grade_R: null,
                  Grade_B: null,
                  Name_B: '徐  超',
                  Unit_B: '江苏',
                  Code_B: '7',
                  Score_R: '1.0',
                  Score_B: '1.0'
                },
                {
                  TableNo: '5',
                  Code_R: '9',
                  Unit_R: '北京',
                  Name_R: '蒋  川',
                  Grade_R: null,
                  Grade_B: null,
                  Name_B: '杨  浩',
                  Unit_B: '新疆',
                  Code_B: '10',
                  Score_R: '2.0',
                  Score_B: '0.0'
                },
                {
                  TableNo: '6',
                  Code_R: '12',
                  Unit_R: '煤矿体协',
                  Name_R: '窦  超',
                  Grade_R: null,
                  Grade_B: null,
                  Name_B: '谢业枧',
                  Unit_B: '湖南',
                  Code_B: '11',
                  Score_R: '0.0',
                  Score_B: '2.0'
                },
                {
                  TableNo: '7',
                  Code_R: '13',
                  Unit_R: '上海',
                  Name_R: '洪  智',
                  Grade_R: null,
                  Grade_B: null,
                  Name_B: '梁  军',
                  Unit_B: '甘肃',
                  Code_B: '14',
                  Score_R: '2.0',
                  Score_B: '0.0'
                },
                {
                  TableNo: '8',
                  Code_R: '16',
                  Unit_R: '云南',
                  Name_R: '黎德志',
                  Grade_R: null,
                  Grade_B: null,
                  Name_B: '汪  洋',
                  Unit_B: '湖北',
                  Code_B: '15',
                  Score_R: '1.0',
                  Score_B: '1.0'
                },
                {
                  TableNo: '9',
                  Code_R: '17',
                  Unit_R: '浙江',
                  Name_R: '赵鑫鑫',
                  Grade_R: null,
                  Grade_B: null,
                  Name_B: '陈富杰',
                  Unit_B: '山东',
                  Code_B: '18',
                  Score_R: '2.0',
                  Score_B: '0.0'
                },
                {
                  TableNo: '10',
                  Code_R: '20',
                  Unit_R: '吉林',
                  Name_R: '曹  霖',
                  Grade_R: null,
                  Grade_B: null,
                  Name_B: '郝继超',
                  Unit_B: '黑龙江',
                  Code_B: '19',
                  Score_R: '0.0',
                  Score_B: '2.0'
                },
                {
                  TableNo: '11',
                  Code_R: '21',
                  Unit_R: '黑龙江',
                  Name_R: '崔  革',
                  Grade_R: null,
                  Grade_B: null,
                  Name_B: '宋长海',
                  Unit_B: '青海',
                  Code_B: '22',
                  Score_R: '2.0',
                  Score_B: '0.0'
                },
                {
                  TableNo: '12',
                  Code_R: '24',
                  Unit_R: '大连',
                  Name_R: '李迈新',
                  Grade_R: null,
                  Grade_B: null,
                  Name_B: '党  斐',
                  Unit_B: '河南',
                  Code_B: '23',
                  Score_R: '1.0',
                  Score_B: '1.0'
                },
                {
                  TableNo: '13',
                  Code_R: '25',
                  Unit_R: '厦门',
                  Name_R: '郑一泓',
                  Grade_R: null,
                  Grade_B: null,
                  Name_B: '卓赞烽',
                  Unit_B: '福建',
                  Code_B: '26',
                  Score_R: '2.0',
                  Score_B: '0.0'
                },
                {
                  TableNo: '14',
                  Code_R: '28',
                  Unit_R: '煤矿体协',
                  Name_R: '蒋凤山',
                  Grade_R: null,
                  Grade_B: null,
                  Name_B: '宿少峰',
                  Unit_B: '内蒙古',
                  Code_B: '27',
                  Score_R: '0.0',
                  Score_B: '2.0'
                },
                {
                  TableNo: '15',
                  Code_R: '29',
                  Unit_R: '宁夏',
                  Name_R: '刘  明',
                  Grade_R: null,
                  Grade_B: null,
                  Name_B: '梁辉远',
                  Unit_B: '山西',
                  Code_B: '30',
                  Score_R: '1.0',
                  Score_B: '1.0'
                },
                {
                  TableNo: '16',
                  Code_R: '32',
                  Unit_R: '北京',
                  Name_R: '靳玉砚',
                  Grade_R: null,
                  Grade_B: null,
                  Name_B: '武俊强',
                  Unit_B: '河南',
                  Code_B: '31',
                  Score_R: '0.0',
                  Score_B: '2.0'
                },
                {
                  TableNo: '17',
                  Code_R: '33',
                  Unit_R: '山东',
                  Name_R: '谢  岿',
                  Grade_R: null,
                  Grade_B: null,
                  Name_B: '刘光辉',
                  Unit_B: '江西',
                  Code_B: '34',
                  Score_R: '2.0',
                  Score_B: '0.0'
                },
                {
                  TableNo: '18',
                  Code_R: '36',
                  Unit_R: '辽宁',
                  Name_R: '李冠男',
                  Grade_R: null,
                  Grade_B: null,
                  Name_B: '程  鸣',
                  Unit_B: '江苏',
                  Code_B: '35',
                  Score_R: '0.0',
                  Score_B: '2.0'
                },
                {
                  TableNo: '19',
                  Code_R: '37',
                  Unit_R: '河北',
                  Name_R: '陆伟韬',
                  Grade_R: null,
                  Grade_B: null,
                  Name_B: '黄光颖',
                  Unit_B: '广东',
                  Code_B: '38',
                  Score_R: '1.0',
                  Score_B: '1.0'
                },
                {
                  TableNo: '20',
                  Code_R: '40',
                  Unit_R: '火车头体协',
                  Name_R: '杨成福',
                  Grade_R: null,
                  Grade_B: null,
                  Name_B: '申  鹏',
                  Unit_B: '河北',
                  Code_B: '39',
                  Score_R: '0.0',
                  Score_B: '2.0'
                },
                {
                  TableNo: '21',
                  Code_R: '41',
                  Unit_R: '上海',
                  Name_R: '孙勇征',
                  Grade_R: null,
                  Grade_B: null,
                  Name_B: '邓志强',
                  Unit_B: '江西',
                  Code_B: '42',
                  Score_R: '2.0',
                  Score_B: '0.0'
                },
                {
                  TableNo: '22',
                  Code_R: '44',
                  Unit_R: '福建',
                  Name_R: '王晓华',
                  Grade_R: null,
                  Grade_B: null,
                  Name_B: '黄海林',
                  Unit_B: '广东',
                  Code_B: '43',
                  Score_R: '0.0',
                  Score_B: '2.0'
                },
                {
                  TableNo: '23',
                  Code_R: '45',
                  Unit_R: '湖北',
                  Name_R: '柳大华',
                  Grade_R: null,
                  Grade_B: null,
                  Name_B: '张  欣',
                  Unit_B: '石油体协',
                  Code_B: '46',
                  Score_R: '2.0',
                  Score_B: '0.0'
                },
                {
                  TableNo: '24',
                  Code_R: '48',
                  Unit_R: '陕西',
                  Name_R: '吕建陆',
                  Grade_R: null,
                  Grade_B: null,
                  Name_B: '黄学谦',
                  Unit_B: '香港象棋总会',
                  Code_B: '47',
                  Score_R: '0.0',
                  Score_B: '2.0'
                },
                {
                  TableNo: '25',
                  Code_R: '49',
                  Unit_R: '辽宁',
                  Name_R: '钟少鸿',
                  Grade_R: null,
                  Grade_B: null,
                  Name_B: '陈志刚',
                  Unit_B: '青海',
                  Code_B: '50',
                  Score_R: '2.0',
                  Score_B: '0.0'
                },
                {
                  TableNo: '26',
                  Code_R: '52',
                  Unit_R: '陕西',
                  Name_R: '贺岁学',
                  Grade_R: null,
                  Grade_B: null,
                  Name_B: '郑惟桐',
                  Unit_B: '四川',
                  Code_B: '51',
                  Score_R: '0.0',
                  Score_B: '2.0'
                },
                {
                  TableNo: '27',
                  Code_R: '53',
                  Unit_R: '浙江',
                  Name_R: '黄竹风',
                  Grade_R: null,
                  Grade_B: null,
                  Name_B: '林文汉',
                  Unit_B: '厦门',
                  Code_B: '54',
                  Score_R: '2.0',
                  Score_B: '0.0'
                }
              ],
              singleItemList: [{ name: '象棋', logo: l('53ed'), flag: !0 }]
            }
          },
          watch: {},
          computed: {},
          filters: {},
          components: {},
          methods: {}
        },
        G = g,
        v = (l('2e1c'), Object(c['a'])(G, A, S, !1, null, '7af8b2fa', null)),
        C = v.exports,
        E = function() {
          var e = this,
            a = e._self._c
          return a('div', [
            a(
              'div',
              { staticClass: 'item-info' },
              [
                e._l(e.singleItemList, function(l, t) {
                  return a('div', { key: t, class: ['single-item', l.flag ? 'active-item' : ''] }, [
                    a('div', [e._v(e._s(l.name))]),
                    a('div', { staticClass: 'item-logo' }, [
                      a('img', { attrs: { src: l.logo, alt: '' } })
                    ])
                  ])
                }),
                a('div', { staticClass: 'single-item-info' }, [
                  a('div', { staticClass: 'info-line1' }, [
                    a('div', [e._v(e._s(e.gameTitle))]),
                    a('div', { staticClass: 'venue' }, [e._v(e._s(e.venueName))])
                  ]),
                  a('div', { staticClass: 'info-line2' }, [
                    a('div', { staticClass: 'report-name' }, [e._v('成绩公告')]),
                    a('div', [e._v(e._s(e.gameTime))])
                  ])
                ])
              ],
              2
            ),
            a(
              'div',
              { staticClass: 'game-info' },
              [
                a('el-col', { attrs: { span: 3 } }, [a('div', [e._v(e._s(e.tableData.column1))])]),
                a('el-col', { attrs: { span: 9 } }, [
                  a('div', { staticClass: 'game-a', staticStyle: { color: 'rgba(0,0,0,0)' } }, [
                    e._v(e._s(e.tableData.column2))
                  ])
                ]),
                a('el-col', { attrs: { span: 3 } }, [
                  a('div', { staticClass: 'game-vs' }, [e._v(e._s(e.tableData.column3))])
                ]),
                a('el-col', { attrs: { span: 9 } }, [
                  a('div', { staticClass: 'game-b', staticStyle: { color: 'rgba(0,0,0,0)' } }, [
                    e._v(e._s(e.tableData.column4))
                  ])
                ])
              ],
              1
            ),
            a(
              'div',
              { staticClass: 'game-table-data' },
              [
                a(
                  'el-row',
                  { attrs: { gutter: 24 } },
                  [
                    a(
                      'el-col',
                      { staticClass: 'col-style-1', attrs: { span: 3 } },
                      [
                        a(
                          'el-table',
                          { staticStyle: { width: '100%' }, attrs: { data: e.startList } },
                          [
                            a('el-table-column', {
                              attrs: {
                                prop: 'table',
                                label: '台号',
                                'header-align': 'center',
                                align: 'center'
                              }
                            })
                          ],
                          1
                        )
                      ],
                      1
                    ),
                    a(
                      'el-col',
                      { staticClass: 'col-style', attrs: { span: 9 } },
                      [
                        a(
                          'el-table',
                          { staticStyle: { width: '100%' }, attrs: { data: e.startList } },
                          [
                            a('el-table-column', {
                              attrs: { label: '队编号', 'header-align': 'center', align: 'center' },
                              scopedSlots: e._u([
                                {
                                  key: 'default',
                                  fn: function(l) {
                                    return [
                                      a('div', { class: e.getTableOtherStyle(l.row.GroupName2) }, [
                                        e._v(' ' + e._s(l.row.Team1) + ' ')
                                      ])
                                    ]
                                  }
                                }
                              ])
                            }),
                            a('el-table-column', {
                              attrs: { label: '单位/姓名', 'min-width': '130%' },
                              scopedSlots: e._u([
                                {
                                  key: 'default',
                                  fn: function(l) {
                                    return [
                                      a(
                                        'div',
                                        { class: [e.getTableTeamStyle(l.row.GroupName1, '')] },
                                        [e._v(' ' + e._s(l.row.GroupName1) + ' ')]
                                      )
                                    ]
                                  }
                                }
                              ])
                            }),
                            a('el-table-column', {
                              attrs: { label: '积分', 'header-align': 'center', align: 'center' },
                              scopedSlots: e._u([
                                {
                                  key: 'default',
                                  fn: function(l) {
                                    return [
                                      a('div', { class: e.getTableOtherStyle(l.row.GroupName2) }, [
                                        e._v(' ' + e._s(l.row.JiFen1) + ' ')
                                      ])
                                    ]
                                  }
                                }
                              ])
                            })
                          ],
                          1
                        )
                      ],
                      1
                    ),
                    a(
                      'el-col',
                      { staticClass: 'col-style', attrs: { span: 3 } },
                      [
                        a(
                          'el-table',
                          {
                            staticStyle: { width: '100%' },
                            attrs: { data: e.startList, 'header-align': 'center' }
                          },
                          [
                            a('el-table-column', {
                              attrs: { prop: '', 'header-align': 'center' },
                              scopedSlots: e._u([
                                {
                                  key: 'header',
                                  fn: function(e) {
                                    return [a('div', { staticClass: 'game-vs-logo' })]
                                  }
                                }
                              ])
                            })
                          ],
                          1
                        )
                      ],
                      1
                    ),
                    a(
                      'el-col',
                      { staticClass: 'col-style-2', attrs: { span: 9 } },
                      [
                        a(
                          'el-table',
                          { staticStyle: { width: '100%' }, attrs: { data: e.startList } },
                          [
                            a('el-table-column', {
                              attrs: { label: '积分', 'header-align': 'center', align: 'center' },
                              scopedSlots: e._u([
                                {
                                  key: 'default',
                                  fn: function(l) {
                                    return [
                                      a('div', { class: e.getTableOtherStyle(l.row.GroupName2) }, [
                                        e._v(' ' + e._s(l.row.JiFen2) + ' ')
                                      ])
                                    ]
                                  }
                                }
                              ])
                            }),
                            a('el-table-column', {
                              attrs: { label: '队编号', 'header-align': 'center', align: 'center' },
                              scopedSlots: e._u([
                                {
                                  key: 'default',
                                  fn: function(l) {
                                    return [
                                      a('div', { class: e.getTableOtherStyle(l.row.GroupName2) }, [
                                        e._v(' ' + e._s(l.row.Team2) + ' ')
                                      ])
                                    ]
                                  }
                                }
                              ])
                            }),
                            a('el-table-column', {
                              attrs: { label: '单位/姓名', 'min-width': '130%' },
                              scopedSlots: e._u([
                                {
                                  key: 'default',
                                  fn: function(l) {
                                    return [
                                      a(
                                        'div',
                                        { class: [e.getTableTeamStyle('', l.row.GroupName2)] },
                                        [e._v(' ' + e._s(l.row.GroupName2) + ' ')]
                                      )
                                    ]
                                  }
                                }
                              ])
                            })
                          ],
                          1
                        )
                      ],
                      1
                    )
                  ],
                  1
                )
              ],
              1
            ),
            e._m(0)
          ])
        },
        U = [
          function() {
            var e = this,
              a = e._self._c
            return a('div', { staticClass: 'game-legend' }, [
              a('div', { staticClass: 'legend-title' }, [e._v('注释')]),
              a('div', { staticClass: 'legend-content' }, [
                a('span', { staticClass: 'legend-content-title' }, [e._v('*')]),
                a('span', [e._v('先手')])
              ])
            ])
          }
        ],
        f =
          (l('8dd1'),
          l('11dc'),
          {
            created: function() {
              ;(this.venueName = this.$route.query.venueName),
                (this.gameTime = this.$route.query.gameTime),
                (this.gameTitle = this.$route.query.gameTitle)
            },
            props: {},
            data: function() {
              return {
                tableData: { column1: '对阵', column2: '-', column3: '当前成绩', column4: '-' },
                venueName: '',
                gameTime: '',
                gameTitle: '',
                startList: [
                  {
                    table: null,
                    Team1: '13',
                    GroupName1: '深圳市代表队',
                    Team2: '1',
                    GroupName2: '湖北省代表队',
                    Result: '',
                    TeamResult: 'Y',
                    JiFen1: '0',
                    JiFen2: '0'
                  },
                  {
                    table: '1-1',
                    Team1: '',
                    GroupName1: '*任皓麟',
                    Team2: '',
                    GroupName2: '泮忆铭',
                    Result: '0:2',
                    TeamResult: null,
                    JiFen1: null,
                    JiFen2: ''
                  },
                  {
                    table: '1-2',
                    Team1: '',
                    GroupName1: '蔡杰程',
                    Team2: '',
                    GroupName2: '*李振宇',
                    Result: '0:2',
                    TeamResult: null,
                    JiFen1: null,
                    JiFen2: ''
                  },
                  {
                    table: '1-3',
                    Team1: null,
                    GroupName1: '*余曼琪',
                    Team2: '',
                    GroupName2: '赵汗青',
                    Result: '0:2',
                    TeamResult: null,
                    JiFen1: null,
                    JiFen2: ''
                  },
                  {
                    table: null,
                    Team1: '2',
                    GroupName1: '河北省代表队',
                    Team2: '14',
                    GroupName2: '山东省代表队',
                    Result: '',
                    TeamResult: 'Y',
                    JiFen1: '0',
                    JiFen2: '0'
                  },
                  {
                    table: '2-1',
                    Team1: '',
                    GroupName1: '*田晟宬',
                    Team2: '',
                    GroupName2: '王晨帆',
                    Result: '1:1',
                    TeamResult: null,
                    JiFen1: null,
                    JiFen2: null
                  },
                  {
                    table: '2-2',
                    Team1: '',
                    GroupName1: '刘昊炎',
                    Team2: '',
                    GroupName2: '*王亨鑫',
                    Result: '1:1',
                    TeamResult: null,
                    JiFen1: null,
                    JiFen2: null
                  },
                  {
                    table: '2-3',
                    Team1: null,
                    GroupName1: '*李晶',
                    Team2: '',
                    GroupName2: '李郑义',
                    Result: '0:2',
                    TeamResult: null,
                    JiFen1: null,
                    JiFen2: null
                  },
                  {
                    table: null,
                    Team1: '15',
                    GroupName1: '湖南省代表队',
                    Team2: '3',
                    GroupName2: '吉林省代表队',
                    Result: '',
                    TeamResult: 'Y',
                    JiFen1: '0',
                    JiFen2: '0'
                  },
                  {
                    table: '3-1',
                    Team1: '',
                    GroupName1: '*容嘉荣',
                    Team2: '',
                    GroupName2: '汪文松',
                    Result: '1:1',
                    TeamResult: '',
                    JiFen1: '',
                    JiFen2: ''
                  },
                  {
                    table: '3-2',
                    Team1: '',
                    GroupName1: '段宗昊',
                    Team2: '',
                    GroupName2: '*邱浩纯',
                    Result: '0:2',
                    TeamResult: '',
                    JiFen1: '',
                    JiFen2: ''
                  },
                  {
                    table: '3-3',
                    Team1: null,
                    GroupName1: '*周楚卉',
                    Team2: '',
                    GroupName2: '刘铭',
                    Result: '2:0',
                    TeamResult: '',
                    JiFen1: '',
                    JiFen2: ''
                  },
                  {
                    table: null,
                    Team1: '4',
                    GroupName1: '内蒙古代表队',
                    Team2: '16',
                    GroupName2: '宁波市代表队',
                    Result: '',
                    TeamResult: 'Y',
                    JiFen1: '0',
                    JiFen2: '0'
                  },
                  {
                    table: '4-1',
                    Team1: '',
                    GroupName1: '*钢苏和',
                    Team2: '',
                    GroupName2: '盛宏安',
                    Result: '1:1',
                    TeamResult: null,
                    JiFen1: null,
                    JiFen2: null
                  },
                  {
                    table: '4-2',
                    Team1: '',
                    GroupName1: '郑策',
                    Team2: '',
                    GroupName2: '*戚成龙',
                    Result: '2:0',
                    TeamResult: null,
                    JiFen1: null,
                    JiFen2: null
                  },
                  {
                    table: '4-3',
                    Team1: null,
                    GroupName1: '*赛娅',
                    Team2: '',
                    GroupName2: '空缺',
                    Result: '2:0',
                    TeamResult: null,
                    JiFen1: null,
                    JiFen2: null
                  },
                  {
                    table: null,
                    Team1: '17',
                    GroupName1: '山西省体育局',
                    Team2: '5',
                    GroupName2: '上海市代表队',
                    Result: '',
                    TeamResult: 'Y',
                    JiFen1: '0',
                    JiFen2: '0'
                  },
                  {
                    table: '5-1',
                    Team1: '',
                    GroupName1: '*胡卓群',
                    Team2: null,
                    GroupName2: '陶丁舟',
                    Result: '1:1',
                    TeamResult: '',
                    JiFen1: '',
                    JiFen2: ''
                  },
                  {
                    table: '5-2',
                    Team1: '',
                    GroupName1: '韩旭',
                    Team2: null,
                    GroupName2: '*杜卓远',
                    Result: '2:0',
                    TeamResult: '',
                    JiFen1: '',
                    JiFen2: ''
                  },
                  {
                    table: '5-3',
                    Team1: null,
                    GroupName1: '*廉博',
                    Team2: null,
                    GroupName2: '戴嘉璐',
                    Result: '0:2',
                    TeamResult: '',
                    JiFen1: '',
                    JiFen2: ''
                  },
                  {
                    table: null,
                    Team1: '6',
                    GroupName1: '四川省代表队',
                    Team2: '18',
                    GroupName2: '安徽省代表队',
                    Result: '',
                    TeamResult: 'Y',
                    JiFen1: '0',
                    JiFen2: '0'
                  },
                  {
                    table: '6-1',
                    Team1: '',
                    GroupName1: '*周伟',
                    Team2: null,
                    GroupName2: '汪润',
                    Result: '2:0',
                    TeamResult: null,
                    JiFen1: null,
                    JiFen2: null
                  },
                  {
                    table: '6-2',
                    Team1: '',
                    GroupName1: '季炜明',
                    Team2: null,
                    GroupName2: '*游德义',
                    Result: '1:1',
                    TeamResult: null,
                    JiFen1: null,
                    JiFen2: null
                  },
                  {
                    table: '6-3',
                    Team1: null,
                    GroupName1: '*阿拉腾花',
                    Team2: null,
                    GroupName2: '王田翠',
                    Result: '0:2',
                    TeamResult: null,
                    JiFen1: null,
                    JiFen2: null
                  },
                  {
                    table: null,
                    Team1: '19',
                    GroupName1: '青海省代表队',
                    Team2: '7',
                    GroupName2: '浙江省代表队',
                    Result: '',
                    TeamResult: 'Y',
                    JiFen1: '0',
                    JiFen2: '0'
                  },
                  {
                    table: '7-1',
                    Team1: '',
                    GroupName1: '*陈章',
                    Team2: null,
                    GroupName2: '金坚强',
                    Result: '0:2',
                    TeamResult: '',
                    JiFen1: '',
                    JiFen2: ''
                  },
                  {
                    table: '7-2',
                    Team1: '',
                    GroupName1: '张乐驰',
                    Team2: null,
                    GroupName2: '*李宇翔',
                    Result: '0:2',
                    TeamResult: '',
                    JiFen1: '',
                    JiFen2: ''
                  },
                  {
                    table: '7-3',
                    Team1: null,
                    GroupName1: '*郝思佳',
                    Team2: null,
                    GroupName2: '叶春花',
                    Result: '0:2',
                    TeamResult: '',
                    JiFen1: '',
                    JiFen2: ''
                  },
                  {
                    table: null,
                    Team1: '8',
                    GroupName1: '北京市代表队',
                    Team2: '20',
                    GroupName2: '重庆市代表队',
                    Result: '',
                    TeamResult: 'Y',
                    JiFen1: '0',
                    JiFen2: '0'
                  },
                  {
                    table: '8-1',
                    Team1: '',
                    GroupName1: '*干启航',
                    Team2: null,
                    GroupName2: '张磊',
                    Result: '0:2',
                    TeamResult: null,
                    JiFen1: null,
                    JiFen2: null
                  },
                  {
                    table: '8-2',
                    Team1: '',
                    GroupName1: '李垠圻',
                    Team2: null,
                    GroupName2: '*熊云博',
                    Result: '1:1',
                    TeamResult: null,
                    JiFen1: null,
                    JiFen2: null
                  },
                  {
                    table: '8-3',
                    Team1: null,
                    GroupName1: '*杨苓',
                    Team2: null,
                    GroupName2: '万佳丽',
                    Result: '0:2',
                    TeamResult: null,
                    JiFen1: null,
                    JiFen2: null
                  },
                  {
                    table: null,
                    Team1: '21',
                    GroupName1: '江苏省代表队',
                    Team2: '9',
                    GroupName2: '广东省代表队',
                    Result: '',
                    TeamResult: 'Y',
                    JiFen1: '0',
                    JiFen2: '0'
                  },
                  {
                    table: '9-1',
                    Team1: null,
                    GroupName1: '*孙铭',
                    Team2: null,
                    GroupName2: '罗百顺',
                    Result: '0:2',
                    TeamResult: '',
                    JiFen1: '',
                    JiFen2: ''
                  },
                  {
                    table: '9-2',
                    Team1: null,
                    GroupName1: '杨浩颖',
                    Team2: null,
                    GroupName2: '*王振宇',
                    Result: '0:2',
                    TeamResult: '',
                    JiFen1: '',
                    JiFen2: ''
                  },
                  {
                    table: '9-3',
                    Team1: null,
                    GroupName1: '*王清圆',
                    Team2: null,
                    GroupName2: '罗柏莹',
                    Result: '2:0',
                    TeamResult: '',
                    JiFen1: '',
                    JiFen2: ''
                  },
                  {
                    table: null,
                    Team1: '10',
                    GroupName1: '天津市代表队',
                    Team2: '22',
                    GroupName2: '黑龙江省代表队',
                    Result: '',
                    TeamResult: 'Y',
                    JiFen1: '0',
                    JiFen2: '0'
                  },
                  {
                    table: '10-1',
                    Team1: '',
                    GroupName1: '*吴弋泉',
                    Team2: null,
                    GroupName2: '王若维',
                    Result: '2:0',
                    TeamResult: null,
                    JiFen1: null,
                    JiFen2: null
                  },
                  {
                    table: '10-2',
                    Team1: '',
                    GroupName1: '石岳',
                    Team2: null,
                    GroupName2: '*王子诚',
                    Result: '2:0',
                    TeamResult: null,
                    JiFen1: null,
                    JiFen2: null
                  },
                  {
                    table: '10-3',
                    Team1: null,
                    GroupName1: '*邓莉',
                    Team2: null,
                    GroupName2: '么坤',
                    Result: '2:0',
                    TeamResult: null,
                    JiFen1: null,
                    JiFen2: null
                  },
                  {
                    table: null,
                    Team1: '23',
                    GroupName1: '陕西省代表队',
                    Team2: '11',
                    GroupName2: '江西省代表队',
                    Result: '',
                    TeamResult: 'Y',
                    JiFen1: '0',
                    JiFen2: '0'
                  },
                  {
                    table: '11-1',
                    Team1: '',
                    GroupName1: '*韩东霖',
                    Team2: null,
                    GroupName2: '汪易峰',
                    Result: '0:2',
                    TeamResult: '',
                    JiFen1: '',
                    JiFen2: ''
                  },
                  {
                    table: '11-2',
                    Team1: '',
                    GroupName1: '张一航',
                    Team2: null,
                    GroupName2: '*陈治政',
                    Result: '0:2',
                    TeamResult: '',
                    JiFen1: '',
                    JiFen2: ''
                  },
                  {
                    table: '11-3',
                    Team1: null,
                    GroupName1: '*刘衡宇',
                    Team2: null,
                    GroupName2: '俞盈',
                    Result: '0:2',
                    TeamResult: '',
                    JiFen1: '',
                    JiFen2: ''
                  },
                  {
                    table: null,
                    Team1: '12',
                    GroupName1: '青岛市代表队',
                    Team2: '24',
                    GroupName2: '河南省代表队',
                    Result: '',
                    TeamResult: 'Y',
                    JiFen1: '0',
                    JiFen2: '0'
                  },
                  {
                    table: '12-1',
                    Team1: '',
                    GroupName1: '*丁家宝',
                    Team2: null,
                    GroupName2: '秦睿彤',
                    Result: '2:0',
                    TeamResult: null,
                    JiFen1: null,
                    JiFen2: null
                  },
                  {
                    table: '12-2',
                    Team1: '',
                    GroupName1: '于庚艺',
                    Team2: null,
                    GroupName2: '*甄士豪',
                    Result: '2:0',
                    TeamResult: null,
                    JiFen1: null,
                    JiFen2: null
                  },
                  {
                    table: '12-3',
                    Team1: null,
                    GroupName1: '*王艺晓',
                    Team2: null,
                    GroupName2: '侯奕含',
                    Result: '2:0',
                    TeamResult: null,
                    JiFen1: null,
                    JiFen2: null
                  }
                ],
                singleItemList: [{ name: '国际跳棋', logo: l('79ec'), flag: !0 }]
              }
            },
            watch: {},
            computed: {},
            filters: {},
            components: {},
            methods: {
              getTableTeamStyle: function(e, a) {
                return (e && e.includes('代表队')) || e.includes('体育局')
                  ? 'table-team-style-1'
                  : (a && a.includes('代表队')) || a.includes('体育局')
                  ? 'table-team-style-2'
                  : void 0
              },
              getTableOtherStyle: function(e) {
                return (e && e.includes('代表队')) || e.includes('体育局')
                  ? 'table-other-style'
                  : void 0
              }
            }
          }),
        y = f,
        h = (l('9a84'), Object(c['a'])(y, E, U, !1, null, '738a3e23', null)),
        W = h.exports,
        M = function() {
          var e = this,
            a = e._self._c
          return a('div', [
            a(
              'div',
              { staticClass: 'item-info' },
              [
                e._l(e.singleItemList, function(l, t) {
                  return a('div', { key: t, class: ['single-item', l.flag ? 'active-item' : ''] }, [
                    a('div', [e._v(e._s(l.name))]),
                    a('div', { staticClass: 'item-logo' }, [
                      a('img', { attrs: { src: l.logo, alt: '' } })
                    ])
                  ])
                }),
                a('div', { staticClass: 'single-item-info' }, [
                  a('div', { staticClass: 'info-line1' }, [
                    a('div', [e._v(e._s(e.gameTitle))]),
                    a('div', { staticClass: 'venue' }, [e._v(e._s(e.venueName))])
                  ]),
                  a('div', { staticClass: 'info-line2' }, [
                    a('div', { staticClass: 'report-name' }, [e._v('成绩公告')]),
                    a('div', [e._v(e._s(e.gameTime))])
                  ])
                ])
              ],
              2
            ),
            a(
              'div',
              { staticClass: 'game-table-data' },
              [
                a(
                  'el-table',
                  { staticStyle: { width: '100%' }, attrs: { data: e.startList } },
                  [
                    a('el-table-column', {
                      attrs: {
                        prop: 'Group',
                        label: '桌号',
                        'header-align': 'center',
                        align: 'center'
                      }
                    }),
                    a('el-table-column', { attrs: { prop: 'Team', label: '号码' } }),
                    a('el-table-column', {
                      attrs: { label: '姓名', prop: 'Name', 'min-width': '130%' }
                    }),
                    a('el-table-column', { attrs: { prop: 'Note', label: '备注' } })
                  ],
                  1
                )
              ],
              1
            )
          ])
        },
        O = [],
        F = {
          created: function() {
            ;(this.venueName = this.$route.query.venueName),
              (this.gameTime = this.$route.query.gameTime),
              (this.gameTitle = this.$route.query.gameTitle)
          },
          props: {},
          data: function() {
            return {
              venueName: '',
              gameTime: '',
              gameTitle: '',
              startList: [
                {
                  Group: 'A1-NS',
                  Team: '1',
                  Name: '王芃淏-戴汉扬',
                  Note: '',
                  Ranking: '1',
                  Team_: '1',
                  Name_: '王芃淏-戴汉扬',
                  Gain: '1008.093',
                  Penalty: '63.640%'
                },
                {
                  Group: 'A1-EW',
                  Team: '11',
                  Name: '申伟-吴照丰',
                  Note: '',
                  Ranking: '2',
                  Team_: '11',
                  Name_: '申伟-吴照丰',
                  Gain: '995.152',
                  Penalty: '63.430%'
                },
                {
                  Group: 'A2-NS',
                  Team: '2',
                  Name: '裴悦涵-史和明',
                  Note: '',
                  Ranking: '3',
                  Team_: '安徽',
                  Name_: '黄烨/张伟',
                  Gain: '60.71',
                  Penalty: '0.00'
                },
                {
                  Group: 'A2-EW',
                  Team: '12',
                  Name: '张贤-王乐银',
                  Note: '',
                  Ranking: '4',
                  Team_: '山西',
                  Name_: '林亚夫/周松鹤',
                  Gain: '60.71',
                  Penalty: '0.00'
                },
                {
                  Group: 'A3-NS',
                  Team: '3',
                  Name: '刘邓超-余舟',
                  Note: '',
                  Ranking: '5',
                  Team_: '煤矿体协',
                  Name_: '龙浩/朱羽',
                  Gain: '60.71',
                  Penalty: '0.00'
                },
                {
                  Group: 'A3-EW',
                  Team: '13',
                  Name: '黄耀-胡一鸿',
                  Note: '',
                  Ranking: '6',
                  Team_: '江西',
                  Name_: '王炜/余明泉',
                  Gain: '60.34',
                  Penalty: '0.00'
                },
                {
                  Group: 'A4-NS',
                  Team: '4',
                  Name: '褚子杰-高宏剑',
                  Note: '',
                  Ranking: '7',
                  Team_: '深圳',
                  Name_: '张邦祥/李建伟',
                  Gain: '58.83',
                  Penalty: '0.00'
                },
                {
                  Group: 'A4-EW',
                  Team: '14',
                  Name: '张宁-张翔宇',
                  Note: '',
                  Ranking: '8',
                  Team_: '天津',
                  Name_: '王卓宽/张孟涛',
                  Gain: '58.27',
                  Penalty: '0.00'
                },
                {
                  Group: 'A5-NS',
                  Team: '5',
                  Name: '徐立鑫-王粟',
                  Note: '',
                  Ranking: '9',
                  Team_: '上海',
                  Name_: '邵子建/单柏松',
                  Gain: '57.52',
                  Penalty: '0.00'
                },
                {
                  Group: 'A5-EW',
                  Team: '15',
                  Name: '赵芳宸-张博信',
                  Note: '',
                  Ranking: '10',
                  Team_: '陕西',
                  Name_: '王剑冰/崔桦',
                  Gain: '56.95',
                  Penalty: '0.00'
                },
                {
                  Group: 'A6-NS',
                  Team: '6',
                  Name: '赵泉-陈晓龙',
                  Note: '',
                  Ranking: '11',
                  Team_: '广东',
                  Name_: '刘松/陈纪恩',
                  Gain: '56.58',
                  Penalty: '0.00'
                },
                {
                  Group: 'A6-EW',
                  Team: '16',
                  Name: '魏文杰-吕欣洋',
                  Note: '',
                  Ranking: '12',
                  Team_: '北京',
                  Name_: '李小毅/胡林林',
                  Gain: '55.83',
                  Penalty: '0.00'
                },
                {
                  Group: 'A7-NS',
                  Team: '7',
                  Name: '顾嘉文-曾挚',
                  Note: '',
                  Ranking: '13',
                  Team_: '湖北',
                  Name_: '刘世铜/梁一雄',
                  Gain: '54.70',
                  Penalty: '0.00'
                },
                {
                  Group: 'A7-EW',
                  Team: '17',
                  Name: '连博-李冰远',
                  Note: '',
                  Ranking: '14',
                  Team_: '黑龙江',
                  Name_: '姚卫/隋杰',
                  Gain: '54.51',
                  Penalty: '0.00'
                },
                {
                  Group: 'B1-NS',
                  Team: '21',
                  Name: '于昊卿-仇实',
                  Note: null,
                  Ranking: null,
                  Team_: null,
                  Name_: null,
                  Gain: null,
                  Penalty: null
                },
                {
                  Group: 'B1-EW',
                  Team: '31',
                  Name: '温家琦-游铭豪',
                  Note: null,
                  Ranking: null,
                  Team_: null,
                  Name_: null,
                  Gain: null,
                  Penalty: null
                },
                {
                  Group: 'B2-NS',
                  Team: '22',
                  Name: '陈昆-朱志诚',
                  Note: null,
                  Ranking: null,
                  Team_: null,
                  Name_: null,
                  Gain: null,
                  Penalty: null
                },
                {
                  Group: 'B2-EW',
                  Team: '32',
                  Name: '王玉飞-余紫阳',
                  Note: null,
                  Ranking: null,
                  Team_: null,
                  Name_: null,
                  Gain: null,
                  Penalty: null
                },
                {
                  Group: 'B3-NS',
                  Team: '23',
                  Name: '舒健-王涛',
                  Note: null,
                  Ranking: null,
                  Team_: null,
                  Name_: null,
                  Gain: null,
                  Penalty: null
                },
                {
                  Group: 'B3-EW',
                  Team: '33',
                  Name: '殷家莘-房正阳',
                  Note: null,
                  Ranking: null,
                  Team_: null,
                  Name_: null,
                  Gain: null,
                  Penalty: null
                },
                {
                  Group: 'B4-NS',
                  Team: '24',
                  Name: '方军-钱宇',
                  Note: null,
                  Ranking: null,
                  Team_: null,
                  Name_: null,
                  Gain: null,
                  Penalty: null
                },
                {
                  Group: 'B4-EW',
                  Team: '34',
                  Name: '刘昊辰-邓程',
                  Note: null,
                  Ranking: null,
                  Team_: null,
                  Name_: null,
                  Gain: null,
                  Penalty: null
                },
                {
                  Group: 'B5-NS',
                  Team: '25',
                  Name: '刘怿泓-袁智杰',
                  Note: null,
                  Ranking: null,
                  Team_: null,
                  Name_: null,
                  Gain: null,
                  Penalty: null
                },
                {
                  Group: 'B5-EW',
                  Team: '35',
                  Name: '金凯-朱辰宇',
                  Note: null,
                  Ranking: null,
                  Team_: null,
                  Name_: null,
                  Gain: null,
                  Penalty: null
                },
                {
                  Group: 'B6-NS',
                  Team: '26',
                  Name: '孙世昱-姜保卓',
                  Note: null,
                  Ranking: null,
                  Team_: null,
                  Name_: null,
                  Gain: null,
                  Penalty: null
                },
                {
                  Group: 'B6-EW',
                  Team: '36',
                  Name: '金堃-韦继彪',
                  Note: null,
                  Ranking: null,
                  Team_: null,
                  Name_: null,
                  Gain: null,
                  Penalty: null
                },
                {
                  Group: 'B7-NS',
                  Team: '27',
                  Name: '刘浩荡-夏瑛珏',
                  Note: null,
                  Ranking: null,
                  Team_: null,
                  Name_: null,
                  Gain: null,
                  Penalty: null
                },
                {
                  Group: 'B7-EW',
                  Team: '37',
                  Name: '郭晓磊-周川尧',
                  Note: null,
                  Ranking: null,
                  Team_: null,
                  Name_: null,
                  Gain: null,
                  Penalty: null
                },
                {
                  Group: 'C1-NS',
                  Team: '41',
                  Name: '李浩宇-高翔',
                  Note: null,
                  Ranking: null,
                  Team_: null,
                  Name_: null,
                  Gain: null,
                  Penalty: null
                },
                {
                  Group: 'C1-EW',
                  Team: '51',
                  Name: '缪本杰-顾斯嘉',
                  Note: null,
                  Ranking: null,
                  Team_: null,
                  Name_: null,
                  Gain: null,
                  Penalty: null
                },
                {
                  Group: 'C2-NS',
                  Team: '42',
                  Name: '王运豪-李子钰',
                  Note: null,
                  Ranking: null,
                  Team_: null,
                  Name_: null,
                  Gain: null,
                  Penalty: null
                },
                {
                  Group: 'C2-EW',
                  Team: '52',
                  Name: '向望-韦文昊',
                  Note: null,
                  Ranking: null,
                  Team_: null,
                  Name_: null,
                  Gain: null,
                  Penalty: null
                },
                {
                  Group: 'C3-NS',
                  Team: '43',
                  Name: '周晓曦-刘鑫浩',
                  Note: null,
                  Ranking: null,
                  Team_: null,
                  Name_: null,
                  Gain: null,
                  Penalty: null
                },
                {
                  Group: 'C3-EW',
                  Team: '53',
                  Name: '邵千芊-黄文锋',
                  Note: null,
                  Ranking: null,
                  Team_: null,
                  Name_: null,
                  Gain: null,
                  Penalty: null
                },
                {
                  Group: 'C4-NS',
                  Team: '44',
                  Name: '杨上力-刘汶鑫',
                  Note: null,
                  Ranking: null,
                  Team_: null,
                  Name_: null,
                  Gain: null,
                  Penalty: null
                },
                {
                  Group: 'C4-EW',
                  Team: '54',
                  Name: '邵雨桐-范轩诚',
                  Note: null,
                  Ranking: null,
                  Team_: null,
                  Name_: null,
                  Gain: null,
                  Penalty: null
                },
                {
                  Group: 'C5-NS',
                  Team: '45',
                  Name: '黄宇杰-李浩',
                  Note: null,
                  Ranking: null,
                  Team_: null,
                  Name_: null,
                  Gain: null,
                  Penalty: null
                },
                {
                  Group: 'C5-EW',
                  Team: '55',
                  Name: '卢桂栋-邓家钊',
                  Note: null,
                  Ranking: null,
                  Team_: null,
                  Name_: null,
                  Gain: null,
                  Penalty: null
                },
                {
                  Group: 'C6-NS',
                  Team: '46',
                  Name: '赵清扬-贺焱三',
                  Note: null,
                  Ranking: null,
                  Team_: null,
                  Name_: null,
                  Gain: null,
                  Penalty: null
                },
                {
                  Group: 'C6-EW',
                  Team: '56',
                  Name: '肖建强-韩冬',
                  Note: null,
                  Ranking: null,
                  Team_: null,
                  Name_: null,
                  Gain: null,
                  Penalty: null
                },
                {
                  Group: 'C7-NS',
                  Team: '47',
                  Name: '陈思远-王晴枫',
                  Note: null,
                  Ranking: null,
                  Team_: null,
                  Name_: null,
                  Gain: null,
                  Penalty: null
                },
                {
                  Group: 'C7-EW',
                  Team: '57',
                  Name: '唐博时-张靖',
                  Note: null,
                  Ranking: null,
                  Team_: null,
                  Name_: null,
                  Gain: null,
                  Penalty: null
                },
                {
                  Group: 'C8-NS',
                  Team: '48',
                  Name: '李渃宇-陈璧藤',
                  Note: null,
                  Ranking: null,
                  Team_: null,
                  Name_: null,
                  Gain: null,
                  Penalty: null
                },
                {
                  Group: 'C8-EW',
                  Team: '58',
                  Name: '刘国松-袁孟新',
                  Note: null,
                  Ranking: null,
                  Team_: null,
                  Name_: null,
                  Gain: null,
                  Penalty: null
                },
                {
                  Group: 'C9-NS',
                  Team: '49',
                  Name: '胡宇杰-鲁茼',
                  Note: null,
                  Ranking: null,
                  Team_: null,
                  Name_: null,
                  Gain: null,
                  Penalty: null
                },
                {
                  Group: 'C9-EW',
                  Team: '59',
                  Name: '李泓波-宋子同',
                  Note: null,
                  Ranking: null,
                  Team_: null,
                  Name_: null,
                  Gain: null,
                  Penalty: null
                },
                {
                  Group: 'C10-EW',
                  Team: '60',
                  Name: '朱汉卿-李靖逸',
                  Note: null,
                  Ranking: null,
                  Team_: null,
                  Name_: null,
                  Gain: null,
                  Penalty: null
                }
              ],
              singleItemList: [{ name: '桥牌', logo: l('a8d9'), flag: !0 }]
            }
          },
          watch: {},
          computed: {},
          filters: {},
          components: {},
          methods: {}
        },
        J = F,
        I = (l('0f81'), Object(c['a'])(J, M, O, !1, null, '7d0c7f36', null)),
        P = I.exports,
        w = function() {
          var e = this,
            a = e._self._c
          return a('div', [
            a(
              'div',
              { staticClass: 'item-info' },
              [
                e._l(e.singleItemList, function(l, t) {
                  return a('div', { key: t, class: ['single-item', l.flag ? 'active-item' : ''] }, [
                    a('div', [e._v(e._s(l.name))]),
                    a('div', { staticClass: 'item-logo' }, [
                      a('img', { attrs: { src: l.logo, alt: '' } })
                    ])
                  ])
                }),
                a('div', { staticClass: 'single-item-info' }, [
                  a('div', { staticClass: 'info-line1' }, [
                    a('div', [e._v(e._s(e.gameTitle))]),
                    a('div', { staticClass: 'venue' }, [e._v(e._s(e.venueName))])
                  ]),
                  a('div', { staticClass: 'info-line2' }, [
                    a('div', { staticClass: 'report-name' }, [e._v('成绩公告')]),
                    a('div', [e._v(e._s(e.gameTime))])
                  ])
                ])
              ],
              2
            ),
            a(
              'div',
              { staticClass: 'game-info' },
              [
                a('el-col', { attrs: { span: 3 } }, [a('div', [e._v(e._s(e.tableData.column1))])]),
                a('el-col', { attrs: { span: 9 } }, [
                  a('div', { staticClass: 'game-a', staticStyle: { color: 'rgba(0,0,0,0)' } }, [
                    e._v(e._s(e.tableData.column2))
                  ])
                ]),
                a('el-col', { attrs: { span: 3 } }, [
                  a('div', { staticClass: 'game-vs' }, [e._v(e._s(e.tableData.column3))])
                ]),
                a('el-col', { attrs: { span: 9 } }, [
                  a('div', { staticClass: 'game-b', staticStyle: { color: 'rgba(0,0,0,0)' } }, [
                    e._v(' ' + e._s(e.tableData.column4) + ' ')
                  ])
                ])
              ],
              1
            ),
            a(
              'div',
              { staticClass: 'game-table-data' },
              [
                a(
                  'el-row',
                  { attrs: { gutter: 24 } },
                  [
                    a(
                      'el-col',
                      { staticClass: 'col-style-1', attrs: { span: 3 } },
                      [
                        a(
                          'el-table',
                          { staticStyle: { width: '100%' }, attrs: { data: e.startList } },
                          [
                            a('el-table-column', {
                              attrs: {
                                prop: 'TableNo',
                                label: '台号',
                                'header-align': 'center',
                                align: 'center'
                              }
                            })
                          ],
                          1
                        )
                      ],
                      1
                    ),
                    a(
                      'el-col',
                      { staticClass: 'col-style', attrs: { span: 9 } },
                      [
                        a(
                          'el-table',
                          { staticStyle: { width: '100%' }, attrs: { data: e.startList } },
                          [
                            a('el-table-column', {
                              attrs: { label: '姓名' },
                              scopedSlots: e._u([
                                {
                                  key: 'default',
                                  fn: function(l) {
                                    return [
                                      a('div', { staticStyle: { color: 'rgba(230,24,71,1)' } }, [
                                        e._v(' ' + e._s(l.row['Name-A']) + ' ')
                                      ])
                                    ]
                                  }
                                }
                              ])
                            }),
                            a('el-table-column', {
                              attrs: { prop: 'Team-A', label: '单位', 'min-width': '130%' }
                            }),
                            a('el-table-column', {
                              attrs: {
                                prop: 'Score-A',
                                label: '积分',
                                'header-align': 'center',
                                align: 'center'
                              }
                            })
                          ],
                          1
                        )
                      ],
                      1
                    ),
                    a(
                      'el-col',
                      { staticClass: 'col-style', attrs: { span: 3 } },
                      [
                        a(
                          'el-table',
                          {
                            staticStyle: { width: '100%' },
                            attrs: { data: e.startList, 'header-align': 'center' }
                          },
                          [
                            a('el-table-column', {
                              attrs: { prop: '', 'header-align': 'center' },
                              scopedSlots: e._u([
                                {
                                  key: 'header',
                                  fn: function(e) {
                                    return [a('div', { staticClass: 'game-vs-logo' })]
                                  }
                                }
                              ])
                            })
                          ],
                          1
                        )
                      ],
                      1
                    ),
                    a(
                      'el-col',
                      { staticClass: 'col-style-2', attrs: { span: 9 } },
                      [
                        a(
                          'el-table',
                          { staticStyle: { width: '100%' }, attrs: { data: e.startList } },
                          [
                            a('el-table-column', {
                              attrs: { label: '姓名' },
                              scopedSlots: e._u([
                                {
                                  key: 'default',
                                  fn: function(l) {
                                    return [
                                      a('div', { staticStyle: { color: 'rgba(60,135,247,1)' } }, [
                                        e._v(' ' + e._s(l.row['Name-B']) + ' ')
                                      ])
                                    ]
                                  }
                                }
                              ])
                            }),
                            a('el-table-column', {
                              attrs: { prop: 'Team-B', label: '单位', 'min-width': '130%' }
                            }),
                            a('el-table-column', {
                              attrs: {
                                prop: 'Score-B',
                                label: '积分',
                                'header-align': 'center',
                                align: 'center'
                              }
                            })
                          ],
                          1
                        )
                      ],
                      1
                    )
                  ],
                  1
                )
              ],
              1
            )
          ])
        },
        k = [],
        L = {
          created: function() {
            ;(this.venueName = this.$route.query.venueName),
              (this.gameTime = this.$route.query.gameTime),
              (this.gameTitle = this.$route.query.gameTitle)
          },
          props: {},
          data: function() {
            return {
              tableData: { column1: '对阵', column2: '-', column3: '当前成绩', column4: '-' },
              venueName: '',
              gameTime: '',
              gameTitle: '',
              startList: [
                {
                  TableNo: '1',
                  'Team-A': '四川    ',
                  'Score-A': '[0] ',
                  Result: ':',
                  'Team-B': '重庆    ',
                  'Score-B': '[0] ',
                  'Name-A': ' 祁观 (1)   ',
                  'Name-B': ' 王骞 (34)  '
                },
                {
                  TableNo: '2',
                  'Team-A': '黑龙江  ',
                  'Score-A': '[0] ',
                  Result: ':',
                  'Team-B': '上海    ',
                  'Score-B': '[0] ',
                  'Name-A': ' 邹佳来 (35)',
                  'Name-B': ' 朱建锋 (2) '
                },
                {
                  TableNo: '3',
                  'Team-A': '山东    ',
                  'Score-A': '[0] ',
                  Result: ':',
                  'Team-B': '青岛    ',
                  'Score-B': '[0] ',
                  'Name-A': ' 杨彦希 (3) ',
                  'Name-B': ' 王晗宇 (36)'
                },
                {
                  TableNo: '4',
                  'Team-A': '江苏    ',
                  'Score-A': '[0] ',
                  Result: ':',
                  'Team-B': '广东    ',
                  'Score-B': '[0] ',
                  'Name-A': ' 唐世祺 (37)',
                  'Name-B': ' 黄立勤 (4) '
                },
                {
                  TableNo: '5',
                  'Team-A': '湖北    ',
                  'Score-A': '[0] ',
                  Result: ':',
                  'Team-B': '浙江    ',
                  'Score-B': '[0] ',
                  'Name-A': ' 曹冬 (5)   ',
                  'Name-B': ' 陶俊吉 (38)'
                },
                {
                  TableNo: '6',
                  'Team-A': '甘肃    ',
                  'Score-A': '[0] ',
                  Result: ':',
                  'Team-B': '湖北    ',
                  'Score-B': '[0] ',
                  'Name-A': ' 李刚 (39)  ',
                  'Name-B': ' 梅凡 (6)   '
                },
                {
                  TableNo: '7',
                  'Team-A': '江苏    ',
                  'Score-A': '[0] ',
                  Result: ':',
                  'Team-B': '江西    ',
                  'Score-B': '[0] ',
                  'Name-A': ' 张轶峰 (7) ',
                  'Name-B': ' 周瑞磊 (40)'
                },
                {
                  TableNo: '8',
                  'Team-A': '金融体协',
                  'Score-A': '[0] ',
                  Result: ':',
                  'Team-B': '江苏    ',
                  'Score-B': '[0] ',
                  'Name-A': ' 施贺昕 (41)',
                  'Name-B': ' 陈靖 (8)   '
                },
                {
                  TableNo: '9',
                  'Team-A': '山东    ',
                  'Score-A': '[0] ',
                  Result: ':',
                  'Team-B': '金融体协',
                  'Score-B': '[0] ',
                  'Name-A': ' 兰志仁 (9) ',
                  'Name-B': ' 黄一峰 (42)'
                },
                {
                  TableNo: '10',
                  'Team-A': '重庆    ',
                  'Score-A': '[0] ',
                  Result: ':',
                  'Team-B': '海南    ',
                  'Score-B': '[0] ',
                  'Name-A': ' 黄小平 (43)',
                  'Name-B': ' 黄圣明 (10)'
                },
                {
                  TableNo: '11',
                  'Team-A': '上海    ',
                  'Score-A': '[0] ',
                  Result: ':',
                  'Team-B': '安徽    ',
                  'Score-B': '[0] ',
                  'Name-A': ' 江齐文 (11)',
                  'Name-B': ' 倪仲星 (44)'
                },
                {
                  TableNo: '12',
                  'Team-A': '福建    ',
                  'Score-A': '[0] ',
                  Result: ':',
                  'Team-B': '山东    ',
                  'Score-B': '[0] ',
                  'Name-A': ' 吴侃 (45)  ',
                  'Name-B': ' 刘洋 (12)  '
                },
                {
                  TableNo: '13',
                  'Team-A': '江苏    ',
                  'Score-A': '[0] ',
                  Result: ':',
                  'Team-B': '青岛    ',
                  'Score-B': '[0] ',
                  'Name-A': ' 奚振扬 (13)',
                  'Name-B': ' 王天羽 (46)'
                },
                {
                  TableNo: '14',
                  'Team-A': '金融体协',
                  'Score-A': '[0] ',
                  Result: ':',
                  'Team-B': '上海    ',
                  'Score-B': '[0] ',
                  'Name-A': ' 周煜昕 (47)',
                  'Name-B': ' 葛凌峰 (14)'
                },
                {
                  TableNo: '15',
                  'Team-A': '四川    ',
                  'Score-A': '[0] ',
                  Result: ':',
                  'Team-B': '云南    ',
                  'Score-B': '[0] ',
                  'Name-A': ' 陈新 (15)  ',
                  'Name-B': ' 胡龙 (48)  '
                },
                {
                  TableNo: '16',
                  'Team-A': '青岛    ',
                  'Score-A': '[0] ',
                  Result: ':',
                  'Team-B': '湖北    ',
                  'Score-B': '[0] ',
                  'Name-A': ' 李泽凯 (50)',
                  'Name-B': ' 殷立成 (16)'
                },
                {
                  TableNo: '17',
                  'Team-A': '浙江    ',
                  'Score-A': '[0] ',
                  Result: ':',
                  'Team-B': '北京    ',
                  'Score-B': '[0] ',
                  'Name-A': ' 张纪国 (17)',
                  'Name-B': ' 胡瑜 (51)  '
                },
                {
                  TableNo: '18',
                  'Team-A': '黑龙江  ',
                  'Score-A': '[0] ',
                  Result: ':',
                  'Team-B': '广东    ',
                  'Score-B': '[0] ',
                  'Name-A': ' 李硕 (52)  ',
                  'Name-B': ' 贺启发 (18)'
                },
                {
                  TableNo: '19',
                  'Team-A': '海南    ',
                  'Score-A': '[0] ',
                  Result: ':',
                  'Team-B': '重庆    ',
                  'Score-B': '[0] ',
                  'Name-A': ' 芦海 (19)  ',
                  'Name-B': ' 张延頔 (53)'
                },
                {
                  TableNo: '20',
                  'Team-A': '河南    ',
                  'Score-A': '[0] ',
                  Result: ':',
                  'Team-B': '辽宁    ',
                  'Score-B': '[0] ',
                  'Name-A': ' 张文博 (54)',
                  'Name-B': ' 艾显平 (20)'
                },
                {
                  TableNo: '21',
                  'Team-A': '河南    ',
                  'Score-A': '[0] ',
                  Result: ':',
                  'Team-B': '湖北    ',
                  'Score-B': '[0] ',
                  'Name-A': ' 刘超 (21)  ',
                  'Name-B': ' 詹力洋 (55)'
                },
                {
                  TableNo: '22',
                  'Team-A': '陕西    ',
                  'Score-A': '[0] ',
                  Result: ':',
                  'Team-B': '辽宁    ',
                  'Score-B': '[0] ',
                  'Name-A': ' 岳永剑 (56)',
                  'Name-B': ' 谭鑫麟 (22)'
                },
                {
                  TableNo: '23',
                  'Team-A': '内蒙古  ',
                  'Score-A': '[0] ',
                  Result: ':',
                  'Team-B': '福建    ',
                  'Score-B': '[0] ',
                  'Name-A': ' 蒙杰焕 (23)',
                  'Name-B': ' 姚兆贤 (57)'
                },
                {
                  TableNo: '24',
                  'Team-A': '北京    ',
                  'Score-A': '[0] ',
                  Result: ':',
                  'Team-B': '上海    ',
                  'Score-B': '[0] ',
                  'Name-A': ' 杨听雨 (58)',
                  'Name-B': ' 黄宇峰 (24)'
                },
                {
                  TableNo: '25',
                  'Team-A': '福建    ',
                  'Score-A': '[0] ',
                  Result: ':',
                  'Team-B': '甘肃    ',
                  'Score-B': '[0] ',
                  'Name-A': ' 谢增忠 (25)',
                  'Name-B': ' 杨树森 (59)'
                },
                {
                  TableNo: '26',
                  'Team-A': '江西    ',
                  'Score-A': '[0] ',
                  Result: ':',
                  'Team-B': '浙江    ',
                  'Score-B': '[0] ',
                  'Name-A': ' 严佳俊 (60)',
                  'Name-B': ' 谢维祥 (26)'
                },
                {
                  TableNo: '27',
                  'Team-A': '浙江    ',
                  'Score-A': '[0] ',
                  Result: ':',
                  'Team-B': '河北    ',
                  'Score-B': '[0] ',
                  'Name-A': ' 樊星岑 (27)',
                  'Name-B': ' 徐嘉琦 (61)'
                },
                {
                  TableNo: '28',
                  'Team-A': '河南    ',
                  'Score-A': '[0] ',
                  Result: ':',
                  'Team-B': '江西    ',
                  'Score-B': '[0] ',
                  'Name-A': ' 夏盛 (63)  ',
                  'Name-B': ' 洪士光 (28)'
                },
                {
                  TableNo: '29',
                  'Team-A': '广东    ',
                  'Score-A': '[0] ',
                  Result: ':',
                  'Team-B': '河南    ',
                  'Score-B': '[0] ',
                  'Name-A': ' 朱凯 (29)  ',
                  'Name-B': ' 黄健光 (64)'
                },
                {
                  TableNo: '30',
                  'Team-A': '金融体协',
                  'Score-A': '[0] ',
                  Result: ':',
                  'Team-B': '陕西    ',
                  'Score-B': '[0] ',
                  'Name-A': ' 陈卓贤 (65)',
                  'Name-B': ' 马宏洲 (30)'
                },
                {
                  TableNo: '31',
                  'Team-A': '广东    ',
                  'Score-A': '[0] ',
                  Result: ':',
                  'Team-B': '宁波    ',
                  'Score-B': '[0] ',
                  'Name-A': ' 林奕忠 (31)',
                  'Name-B': ' 陈骏 (66)  '
                },
                {
                  TableNo: '32',
                  'Team-A': '河北    ',
                  'Score-A': '[0] ',
                  Result: ':',
                  'Team-B': '青海    ',
                  'Score-B': '[0] ',
                  'Name-A': ' 陈杰 (67)  ',
                  'Name-B': ' 金东雷 (32)'
                },
                {
                  TableNo: '33',
                  'Team-A': '宁夏    ',
                  'Score-A': '[0] ',
                  Result: ':',
                  'Team-B': '黑龙江  ',
                  'Score-B': '[0] ',
                  'Name-A': ' 焦中其 (33)',
                  'Name-B': ' 王启超 (68)'
                },
                {
                  TableNo: '34',
                  'Team-A': '甘肃    ',
                  'Score-A': '[0] ',
                  Result: '1:0   ',
                  'Team-B': null,
                  'Score-B': null,
                  'Name-A': ' 王兰成 (69)',
                  'Name-B': 'BYE'
                }
              ],
              singleItemList: [{ name: '五子棋', logo: l('9a5b'), flag: !0 }]
            }
          },
          watch: {},
          computed: {},
          filters: {},
          components: {},
          methods: {}
        },
        D = L,
        q = (l('a6e4'), Object(c['a'])(D, w, k, !1, null, '43541460', null)),
        $ = q.exports,
        j = function() {
          var e = this,
            a = e._self._c
          return a('div', [
            a(
              'div',
              { staticClass: 'item-info' },
              [
                e._l(e.singleItemList, function(l, t) {
                  return a('div', { key: t, class: ['single-item', l.flag ? 'active-item' : ''] }, [
                    a('div', [e._v(e._s(l.name))]),
                    a('div', { staticClass: 'item-logo' }, [
                      a('img', { attrs: { src: l.logo, alt: '' } })
                    ])
                  ])
                }),
                a('div', { staticClass: 'single-item-info' }, [
                  a('div', { staticClass: 'info-line1' }, [
                    a('div', [e._v(e._s(e.gameTitle))]),
                    a('div', { staticClass: 'venue' }, [e._v(e._s(e.venueName))])
                  ]),
                  a('div', { staticClass: 'info-line2' }, [
                    a('div', { staticClass: 'report-name' }, [e._v('成绩公告')]),
                    a('div', [e._v(e._s(e.gameTime))])
                  ])
                ])
              ],
              2
            ),
            a(
              'div',
              { staticClass: 'game-info' },
              [
                a('el-col', { attrs: { span: 3 } }, [a('div', [e._v(e._s(e.tableData.column1))])]),
                a('el-col', { attrs: { span: 9 } }, [
                  a('div', { staticClass: 'game-a' }, [e._v(e._s(e.tableData.column2))])
                ]),
                a('el-col', { attrs: { span: 3 } }, [
                  a('div', { staticClass: 'game-vs' }, [e._v(e._s(e.tableData.column3))])
                ]),
                a('el-col', { attrs: { span: 9 } }, [
                  a('div', { staticClass: 'game-b' }, [e._v(e._s(e.tableData.column4))])
                ])
              ],
              1
            ),
            a(
              'div',
              { staticClass: 'game-table-data' },
              [
                a(
                  'el-row',
                  { attrs: { gutter: 24 } },
                  [
                    a(
                      'el-col',
                      { staticClass: 'col-style-1', attrs: { span: 3 } },
                      [
                        a(
                          'el-table',
                          { staticStyle: { width: '100%' }, attrs: { data: e.resultList } },
                          [
                            a('el-table-column', { attrs: { prop: 'GROUP', label: '组别' } }),
                            a('el-table-column', { attrs: { prop: 'TABLE', label: '桌号' } })
                          ],
                          1
                        )
                      ],
                      1
                    ),
                    a(
                      'el-col',
                      { staticClass: 'col-style', attrs: { span: 9 } },
                      [
                        a(
                          'el-table',
                          { staticStyle: { width: '100%' }, attrs: { data: e.resultList } },
                          [
                            a('el-table-column', { attrs: { prop: 'NUMBER_B', label: '编号' } }),
                            a('el-table-column', {
                              attrs: { label: '姓名', 'min-width': '130%' },
                              scopedSlots: e._u([
                                {
                                  key: 'default',
                                  fn: function(l) {
                                    return [
                                      a('div', { staticStyle: { color: 'rgba(230,24,71,1)' } }, [
                                        e._v(' ' + e._s(l.row.NAME_B) + ' ')
                                      ])
                                    ]
                                  }
                                }
                              ])
                            }),
                            a('el-table-column', { attrs: { prop: 'UNIT_B', label: '单位' } }),
                            a('el-table-column', {
                              attrs: {
                                prop: 'SCORE_B',
                                label: '积分',
                                'header-align': 'center',
                                align: 'center'
                              }
                            })
                          ],
                          1
                        )
                      ],
                      1
                    ),
                    a(
                      'el-col',
                      { staticClass: 'col-style', attrs: { span: 3 } },
                      [
                        a(
                          'el-table',
                          {
                            staticStyle: { width: '100%' },
                            attrs: { data: e.resultList, 'header-align': 'center' }
                          },
                          [
                            a('el-table-column', {
                              attrs: { prop: '', 'header-align': 'center' },
                              scopedSlots: e._u([
                                {
                                  key: 'header',
                                  fn: function(e) {
                                    return [a('div', { staticClass: 'game-vs-logo' })]
                                  }
                                }
                              ])
                            })
                          ],
                          1
                        )
                      ],
                      1
                    ),
                    a(
                      'el-col',
                      { staticClass: 'col-style-2', attrs: { span: 9 } },
                      [
                        a(
                          'el-table',
                          { staticStyle: { width: '100%' }, attrs: { data: e.resultList } },
                          [
                            a('el-table-column', {
                              attrs: {
                                prop: 'SCORE_W',
                                label: '积分',
                                'header-align': 'center',
                                align: 'center'
                              }
                            }),
                            a('el-table-column', { attrs: { prop: 'NUMBER_W', label: '编号' } }),
                            a('el-table-column', {
                              attrs: { prop: 'NAME_W', label: '姓名', 'min-width': '130%' },
                              scopedSlots: e._u([
                                {
                                  key: 'default',
                                  fn: function(l) {
                                    return [
                                      a('div', { staticStyle: { color: 'rgba(60,135,247,1)' } }, [
                                        e._v(' ' + e._s(l.row.NAME_W) + ' ')
                                      ])
                                    ]
                                  }
                                }
                              ])
                            }),
                            a('el-table-column', { attrs: { prop: 'UNIT_W', label: '单位' } })
                          ],
                          1
                        )
                      ],
                      1
                    )
                  ],
                  1
                )
              ],
              1
            )
          ])
        },
        x = [],
        Y = {
          created: function() {
            ;(this.venueName = this.$route.query.venueName),
              (this.gameTime = this.$route.query.gameTime),
              (this.gameTitle = this.$route.query.gameTitle)
          },
          props: {},
          data: function() {
            return {
              tableData: { column1: '对阵', column2: '黑棋', column3: 'VS', column4: '白棋' },
              venueName: '',
              gameTime: '',
              gameTitle: '',
              resultList: [
                {
                  GROUP: 'A',
                  TABLE: '1',
                  NUMBER_B: '1',
                  NAME_B: '李轩豪 ',
                  SCORE_B: '2',
                  UNIT_B: '重庆',
                  SCORE_W: '0',
                  NUMBER_W: '2',
                  NAME_W: '李建宇 ',
                  UNIT_W: '河北'
                },
                {
                  GROUP: '',
                  TABLE: '2',
                  NUMBER_B: '3',
                  NAME_B: ' 韦一博 ',
                  SCORE_B: '2',
                  UNIT_B: '厦门',
                  SCORE_W: '0',
                  NUMBER_W: '4',
                  NAME_W: ' 张家尧 ',
                  UNIT_W: '内蒙古'
                },
                {
                  GROUP: '',
                  TABLE: '3',
                  NUMBER_B: '5',
                  NAME_B: ' 张  强 ',
                  SCORE_B: '2',
                  UNIT_B: '西藏',
                  SCORE_W: '0',
                  NUMBER_W: '6',
                  NAME_W: ' 王星昊 ',
                  UNIT_W: '上海'
                },
                {
                  GROUP: '',
                  TABLE: '4',
                  NUMBER_B: '7',
                  NAME_B: ' 陈一纯 ',
                  SCORE_B: '2',
                  UNIT_B: '西藏',
                  SCORE_W: '0',
                  NUMBER_W: '8',
                  NAME_W: ' 张浩伟 ',
                  UNIT_W: '安徽'
                },
                {
                  GROUP: '',
                  TABLE: '5',
                  NUMBER_B: '9',
                  NAME_B: ' 柯  洁 ',
                  SCORE_B: '2',
                  UNIT_B: '云南',
                  SCORE_W: '0',
                  NUMBER_W: '10',
                  NAME_W: ' 廖元赫 ',
                  UNIT_W: '四川'
                },
                {
                  GROUP: '',
                  TABLE: '6',
                  NUMBER_B: '11',
                  NAME_B: ' 柁嘉熹 ',
                  SCORE_B: '2',
                  UNIT_B: '黑龙江',
                  SCORE_W: '0',
                  NUMBER_W: '12',
                  NAME_W: ' 陈昱森 ',
                  UNIT_W: '广东'
                },
                {
                  GROUP: '',
                  TABLE: '7',
                  NUMBER_B: '13',
                  NAME_B: ' 黄春棋 ',
                  SCORE_B: '0',
                  UNIT_B: '河南',
                  SCORE_W: '2',
                  NUMBER_W: '14',
                  NAME_W: ' 范廷钰 ',
                  UNIT_W: '山东'
                },
                {
                  GROUP: '',
                  TABLE: '8',
                  NUMBER_B: '15',
                  NAME_B: ' 李  喆 ',
                  SCORE_B: '2',
                  UNIT_B: '湖北',
                  SCORE_W: '0',
                  NUMBER_W: '16',
                  NAME_W: ' 焦士维 ',
                  UNIT_W: '云南'
                },
                {
                  GROUP: '',
                  TABLE: '9',
                  NUMBER_B: '17',
                  NAME_B: ' 甘思阳 ',
                  SCORE_B: '0',
                  UNIT_B: '深圳',
                  SCORE_W: '2',
                  NUMBER_W: '18',
                  NAME_W: ' 李雨昂 ',
                  UNIT_W: '辽宁'
                },
                {
                  GROUP: '',
                  TABLE: '10',
                  NUMBER_B: '19',
                  NAME_B: ' 薛冠华 ',
                  SCORE_B: '0',
                  UNIT_B: '河北',
                  SCORE_W: '2',
                  NUMBER_W: '20',
                  NAME_W: ' 陈  贤 ',
                  UNIT_W: '江苏'
                },
                {
                  GROUP: '',
                  TABLE: '11',
                  NUMBER_B: '21',
                  NAME_B: ' 杨宗煜 ',
                  SCORE_B: '0',
                  UNIT_B: '北京',
                  SCORE_W: '2',
                  NUMBER_W: '22',
                  NAME_W: ' 马逸超 ',
                  UNIT_W: '四川'
                },
                {
                  GROUP: '',
                  TABLE: '12',
                  NUMBER_B: '23',
                  NAME_B: ' 韩一洲 ',
                  SCORE_B: '2',
                  UNIT_B: '北京',
                  SCORE_W: '0',
                  NUMBER_W: '24',
                  NAME_W: '王音灵水',
                  UNIT_W: '新疆'
                },
                {
                  GROUP: '',
                  TABLE: '13',
                  NUMBER_B: '25',
                  NAME_B: ' 徐泽鑫 ',
                  SCORE_B: '0',
                  UNIT_B: '煤矿体协',
                  SCORE_W: '2',
                  NUMBER_W: '26',
                  NAME_W: ' 李钦诚 ',
                  UNIT_W: '浙江'
                },
                {
                  GROUP: '',
                  TABLE: '14',
                  NUMBER_B: '27',
                  NAME_B: ' 程宏昊',
                  SCORE_B: '2',
                  UNIT_B: '广东',
                  SCORE_W: '0',
                  NUMBER_W: '28',
                  NAME_W: ' 张亚博',
                  UNIT_W: '陕西'
                },
                {
                  GROUP: 'B',
                  TABLE: '1',
                  NUMBER_B: '1',
                  NAME_B: '陈梓健 ',
                  SCORE_B: '2',
                  UNIT_B: '山东',
                  SCORE_W: '0',
                  NUMBER_W: '2',
                  NAME_W: '冯  昊 ',
                  UNIT_W: '河北'
                },
                {
                  GROUP: '',
                  TABLE: '2',
                  NUMBER_B: '3',
                  NAME_B: ' 陈  浩 ',
                  SCORE_B: '0',
                  UNIT_B: '西藏',
                  SCORE_W: '2',
                  NUMBER_W: '4',
                  NAME_W: ' 时  越 ',
                  UNIT_W: '上海'
                },
                {
                  GROUP: '',
                  TABLE: '3',
                  NUMBER_B: '5',
                  NAME_B: ' 石豫来 ',
                  SCORE_B: '2',
                  UNIT_B: '陕西',
                  SCORE_W: '0',
                  NUMBER_W: '6',
                  NAME_W: ' 谢  科 ',
                  UNIT_W: '浙江'
                },
                {
                  GROUP: '',
                  TABLE: '4',
                  NUMBER_B: '7',
                  NAME_B: ' 李成森 ',
                  SCORE_B: '2',
                  UNIT_B: '煤矿体协',
                  SCORE_W: '0',
                  NUMBER_W: '8',
                  NAME_W: ' 许瀚文 ',
                  UNIT_W: '广东'
                },
                {
                  GROUP: '',
                  TABLE: '5',
                  NUMBER_B: '9',
                  NAME_B: ' 刘兆哲 ',
                  SCORE_B: '2',
                  UNIT_B: '河南',
                  SCORE_W: '0',
                  NUMBER_W: '10',
                  NAME_W: ' 黄明宇 ',
                  UNIT_W: '上海'
                },
                {
                  GROUP: '',
                  TABLE: '6',
                  NUMBER_B: '11',
                  NAME_B: ' 黄一鸣 ',
                  SCORE_B: '0',
                  UNIT_B: '深圳',
                  SCORE_W: '2',
                  NUMBER_W: '12',
                  NAME_W: ' 陈豪鑫 ',
                  UNIT_W: '厦门'
                },
                {
                  GROUP: '',
                  TABLE: '7',
                  NUMBER_B: '13',
                  NAME_B: ' 舒一笑 ',
                  SCORE_B: '2',
                  UNIT_B: '煤矿体协',
                  SCORE_W: '0',
                  NUMBER_W: '14',
                  NAME_W: ' 周玉川 ',
                  UNIT_W: '大连'
                },
                {
                  GROUP: '',
                  TABLE: '8',
                  NUMBER_B: '15',
                  NAME_B: ' 唐韦星 ',
                  SCORE_B: '2',
                  UNIT_B: '江苏',
                  SCORE_W: '0',
                  NUMBER_W: '16',
                  NAME_W: ' 郭信驿 ',
                  UNIT_W: '新疆'
                },
                {
                  GROUP: '',
                  TABLE: '9',
                  NUMBER_B: '17',
                  NAME_B: ' 陈必森 ',
                  SCORE_B: '0',
                  UNIT_B: '海南',
                  SCORE_W: '2',
                  NUMBER_W: '18',
                  NAME_W: ' 何语涵 ',
                  UNIT_W: '重庆'
                },
                {
                  GROUP: '',
                  TABLE: '10',
                  NUMBER_B: '19',
                  NAME_B: ' 杨文铠 ',
                  SCORE_B: '0',
                  UNIT_B: '北京',
                  SCORE_W: '2',
                  NUMBER_W: '20',
                  NAME_W: ' 柳琪峰 ',
                  UNIT_W: '云南'
                },
                {
                  GROUP: '',
                  TABLE: '11',
                  NUMBER_B: '21',
                  NAME_B: ' 严在明 ',
                  SCORE_B: '2',
                  UNIT_B: '黑龙江',
                  SCORE_W: '0',
                  NUMBER_W: '22',
                  NAME_W: ' 陈  磊 ',
                  UNIT_W: '陕西'
                },
                {
                  GROUP: '',
                  TABLE: '12',
                  NUMBER_B: '23',
                  NAME_B: ' 杨鼎新 ',
                  SCORE_B: '0',
                  UNIT_B: '重庆',
                  SCORE_W: '2',
                  NUMBER_W: '24',
                  NAME_W: ' 童梦成 ',
                  UNIT_W: '浙江'
                },
                {
                  GROUP: '',
                  TABLE: '13',
                  NUMBER_B: '25',
                  NAME_B: ' 郑载想 ',
                  SCORE_B: '0',
                  UNIT_B: '四川',
                  SCORE_W: '2',
                  NUMBER_W: '26',
                  NAME_W: ' 江维杰 ',
                  UNIT_W: '山东'
                },
                {
                  GROUP: '',
                  TABLE: '14',
                  NUMBER_B: '27',
                  NAME_B: ' 韩卓然',
                  SCORE_B: '0',
                  UNIT_B: '厦门',
                  SCORE_W: '2',
                  NUMBER_W: '28',
                  NAME_W: ' 赵晨宇',
                  UNIT_W: '江苏'
                }
              ],
              singleItemList: [{ name: '围棋', logo: l('5d38'), flag: !0 }]
            }
          },
          watch: {},
          computed: {},
          filters: {},
          components: {},
          methods: {}
        },
        V = Y,
        H = (l('a864'), Object(c['a'])(V, j, x, !1, null, '72de8fe7', null)),
        X = H.exports,
        z = function() {
          var e = this,
            a = e._self._c
          return a('div', [
            a(
              'div',
              { staticClass: 'item-info' },
              [
                e._l(e.singleItemList, function(l, t) {
                  return a('div', { key: t, class: ['single-item', l.flag ? 'active-item' : ''] }, [
                    a('div', [e._v(e._s(l.name))]),
                    a('div', { staticClass: 'item-logo' }, [
                      a('img', { attrs: { src: l.logo, alt: '' } })
                    ])
                  ])
                }),
                a('div', { staticClass: 'single-item-info' }, [
                  a('div', { staticClass: 'info-line1' }, [
                    a('div', [e._v(e._s(e.gameTitle))]),
                    a('div', { staticClass: 'venue' }, [e._v(e._s(e.venueName))])
                  ]),
                  a('div', { staticClass: 'info-line2' }, [
                    a('div', { staticClass: 'report-name' }, [e._v('成绩公告')]),
                    a('div', [e._v(e._s(e.gameTime))])
                  ])
                ])
              ],
              2
            ),
            a(
              'div',
              { staticClass: 'game-info' },
              [
                a('el-col', { attrs: { span: 3 } }, [a('div', [e._v(e._s(e.tableData.column1))])]),
                a('el-col', { attrs: { span: 9 } }, [
                  a('div', { staticClass: 'game-b', staticStyle: { 'margin-left': '-10px' } }, [
                    e._v(e._s(e.tableData.column2))
                  ])
                ]),
                a('el-col', { attrs: { span: 3 } }, [
                  a('div', { staticClass: 'game-vs' }, [e._v(e._s(e.tableData.column3))])
                ]),
                a('el-col', { attrs: { span: 9 } }, [
                  a('div', { staticClass: 'game-a' }, [e._v(e._s(e.tableData.column4))])
                ])
              ],
              1
            ),
            a(
              'div',
              { staticClass: 'game-table-data' },
              [
                a(
                  'el-row',
                  { attrs: { gutter: 24 } },
                  [
                    a(
                      'el-col',
                      { staticClass: 'col-style-1', attrs: { span: 3 } },
                      [
                        a(
                          'el-table',
                          { staticStyle: { width: '100%' }, attrs: { data: e.resultList } },
                          [
                            a('el-table-column', {
                              attrs: {
                                prop: 'TableNo',
                                label: '桌号',
                                'header-align': 'center',
                                align: 'center'
                              }
                            })
                          ],
                          1
                        )
                      ],
                      1
                    ),
                    a(
                      'el-col',
                      { staticClass: 'col-style', attrs: { span: 9 } },
                      [
                        a(
                          'el-table',
                          { staticStyle: { width: '100%' }, attrs: { data: e.resultList } },
                          [
                            a('el-table-column', { attrs: { prop: 'BibA', label: '号码' } }),
                            a('el-table-column', {
                              attrs: { label: '姓名', 'min-width': '130%' },
                              scopedSlots: e._u([
                                {
                                  key: 'default',
                                  fn: function(l) {
                                    return [
                                      a('div', { staticStyle: { color: 'rgba(230,24,71,1)' } }, [
                                        e._v(' ' + e._s(l.row.NameA) + ' ')
                                      ])
                                    ]
                                  }
                                }
                              ])
                            }),
                            a('el-table-column', { attrs: { prop: 'TeamA', label: '单位' } }),
                            a('el-table-column', {
                              attrs: {
                                prop: 'ScoreA',
                                label: '积分',
                                'header-align': 'center',
                                align: 'center'
                              }
                            })
                          ],
                          1
                        )
                      ],
                      1
                    ),
                    a(
                      'el-col',
                      { staticClass: 'col-style', attrs: { span: 3 } },
                      [
                        a(
                          'el-table',
                          { staticStyle: { width: '100%' }, attrs: { data: e.resultList } },
                          [
                            a('el-table-column', {
                              attrs: { prop: 'Result', 'header-align': 'center', align: 'center' },
                              scopedSlots: e._u([
                                {
                                  key: 'header',
                                  fn: function(e) {
                                    return [a('div', { staticClass: 'game-vs-logo' })]
                                  }
                                }
                              ])
                            })
                          ],
                          1
                        )
                      ],
                      1
                    ),
                    a(
                      'el-col',
                      { staticClass: 'col-style-2', attrs: { span: 9 } },
                      [
                        a(
                          'el-table',
                          { staticStyle: { width: '100%' }, attrs: { data: e.resultList } },
                          [
                            a('el-table-column', {
                              attrs: {
                                prop: 'ScoreB',
                                label: '积分',
                                'header-align': 'center',
                                align: 'center'
                              }
                            }),
                            a('el-table-column', {
                              attrs: { label: '姓名', 'min-width': '130%' },
                              scopedSlots: e._u([
                                {
                                  key: 'default',
                                  fn: function(l) {
                                    return [
                                      a('div', { staticStyle: { color: 'rgba(60,135,247,1)' } }, [
                                        e._v(' ' + e._s(l.row.NameB) + ' ')
                                      ])
                                    ]
                                  }
                                }
                              ])
                            }),
                            a('el-table-column', { attrs: { prop: 'TeamB', label: '单位' } }),
                            a('el-table-column', { attrs: { prop: 'BibB', label: '号码' } })
                          ],
                          1
                        )
                      ],
                      1
                    )
                  ],
                  1
                )
              ],
              1
            )
          ])
        },
        K = [],
        Q = {
          created: function() {
            ;(this.venueName = this.$route.query.venueName),
              (this.gameTime = this.$route.query.gameTime),
              (this.gameTitle = this.$route.query.gameTitle)
          },
          props: {},
          data: function() {
            return {
              tableData: { column1: '对阵', column2: '白方', column3: 'VS', column4: '黑方' },
              venueName: '',
              gameTime: '',
              gameTitle: '',
              resultList: [
                {
                  TableNo: '1',
                  BibA: '35',
                  NameA: '房岩',
                  TeamA: '湖北',
                  ScoreA: '0',
                  Result: null,
                  ScoreB: '0',
                  NameB: '丁立人',
                  TeamB: '浙江',
                  BibB: '1'
                },
                {
                  TableNo: '2',
                  BibA: '2',
                  NameA: '余泱漪',
                  TeamA: '北京',
                  ScoreA: '0',
                  Result: null,
                  ScoreB: '0',
                  NameB: '鲍麒麟',
                  TeamB: '黑龙江',
                  BibB: '36'
                },
                {
                  TableNo: '3',
                  BibA: '37',
                  NameA: '张子洋',
                  TeamA: '河南',
                  ScoreA: '0',
                  Result: null,
                  ScoreB: '0',
                  NameB: '王皓',
                  TeamB: '黑龙江',
                  BibB: '3'
                },
                {
                  TableNo: '4',
                  BibA: '4',
                  NameA: '卜祥志',
                  TeamA: '山东',
                  ScoreA: '0',
                  Result: null,
                  ScoreB: '0',
                  NameB: '牟科',
                  TeamB: '青岛',
                  BibB: '38'
                },
                {
                  TableNo: '5',
                  BibA: '39',
                  NameA: '徐铭辉',
                  TeamA: '浙江',
                  ScoreA: '0',
                  Result: null,
                  ScoreB: '0',
                  NameB: '韦奕',
                  TeamB: '江苏',
                  BibB: '5'
                },
                {
                  TableNo: '6',
                  BibA: '6',
                  NameA: '李超',
                  TeamA: '北京',
                  ScoreA: '0',
                  Result: null,
                  ScoreB: '0',
                  NameB: '赵晨曦',
                  TeamB: '广东',
                  BibB: '40'
                },
                {
                  TableNo: '7',
                  BibA: '41',
                  NameA: '林卫国',
                  TeamA: '湖北',
                  ScoreA: '0',
                  Result: null,
                  ScoreB: '0',
                  NameB: '王玥',
                  TeamB: '天津',
                  BibB: '7'
                },
                {
                  TableNo: '8',
                  BibA: '8',
                  NameA: '倪华',
                  TeamA: '上海',
                  ScoreA: '0',
                  Result: null,
                  ScoreB: '0',
                  NameB: '刘钊杞',
                  TeamB: '河北',
                  BibB: '42'
                },
                {
                  TableNo: '9',
                  BibA: '43',
                  NameA: '许亦柠',
                  TeamA: '上海',
                  ScoreA: '0',
                  Result: null,
                  ScoreB: '0',
                  NameB: '赵骏',
                  TeamB: '山东',
                  BibB: '9'
                },
                {
                  TableNo: '10',
                  BibA: '10',
                  NameA: '马群',
                  TeamA: '浙江',
                  ScoreA: '0',
                  Result: null,
                  ScoreB: '0',
                  NameB: '周江南',
                  TeamB: '青岛',
                  BibB: '44'
                },
                {
                  TableNo: '11',
                  BibA: '45',
                  NameA: '彭红墀',
                  TeamA: '河北',
                  ScoreA: '0',
                  Result: null,
                  ScoreB: '0',
                  NameB: '周健超',
                  TeamB: '上海',
                  BibB: '11'
                },
                {
                  TableNo: '12',
                  BibA: '12',
                  NameA: '卢尚磊',
                  TeamA: '浙江',
                  ScoreA: '0',
                  Result: null,
                  ScoreB: '0',
                  NameB: '范会丰',
                  TeamB: '大连',
                  BibB: '46'
                },
                {
                  TableNo: '13',
                  BibA: '47',
                  NameA: '赵洲桥',
                  TeamA: '天津',
                  ScoreA: '0',
                  Result: null,
                  ScoreB: '0',
                  NameB: '周唯奇',
                  TeamB: '重庆',
                  BibB: '13'
                },
                {
                  TableNo: '14',
                  BibA: '14',
                  NameA: '温阳',
                  TeamA: '山东',
                  ScoreA: '0',
                  Result: null,
                  ScoreB: '0',
                  NameB: '张家宝',
                  TeamB: '甘肃',
                  BibB: '48'
                },
                {
                  TableNo: '15',
                  BibA: '49',
                  NameA: '吴玺斌',
                  TeamA: '黑龙江',
                  ScoreA: '0',
                  Result: null,
                  ScoreB: '0',
                  NameB: '张鹏翔',
                  TeamB: '河北',
                  BibB: '15'
                },
                {
                  TableNo: '16',
                  BibA: '16',
                  NameA: '徐英伦',
                  TeamA: '广东',
                  ScoreA: '0',
                  Result: null,
                  ScoreB: '0',
                  NameB: '马麟',
                  TeamB: '甘肃',
                  BibB: '50'
                },
                {
                  TableNo: '17',
                  BibA: '51',
                  NameA: '李国豪',
                  TeamA: '甘肃',
                  ScoreA: '0',
                  Result: null,
                  ScoreB: '0',
                  NameB: '曾重生',
                  TeamB: '重庆',
                  BibB: '17'
                },
                {
                  TableNo: '18',
                  BibA: '18',
                  NameA: '李荻',
                  TeamA: '四川',
                  ScoreA: '0',
                  Result: null,
                  ScoreB: '0',
                  NameB: '张晟昀',
                  TeamB: '四川',
                  BibB: '52'
                },
                {
                  TableNo: '19',
                  BibA: '53',
                  NameA: '孙超',
                  TeamA: '甘肃',
                  ScoreA: '0',
                  Result: null,
                  ScoreB: '0',
                  NameB: '刘庆南',
                  TeamB: '山东',
                  BibB: '19'
                },
                {
                  TableNo: '20',
                  BibA: '20',
                  NameA: '修德顺',
                  TeamA: '北京',
                  ScoreA: '0',
                  Result: null,
                  ScoreB: '0',
                  NameB: '林培森',
                  TeamB: '广东',
                  BibB: '54'
                },
                {
                  TableNo: '21',
                  BibA: '55',
                  NameA: '张胤哲',
                  TeamA: '河南',
                  ScoreA: '0',
                  Result: null,
                  ScoreB: '0',
                  NameB: '刘言',
                  TeamB: '重庆',
                  BibB: '21'
                },
                {
                  TableNo: '22',
                  BibA: '22',
                  NameA: '戴常人',
                  TeamA: '天津',
                  ScoreA: '0',
                  Result: null,
                  ScoreB: '0',
                  NameB: '恩和',
                  TeamB: '内蒙古',
                  BibB: '56'
                },
                {
                  TableNo: '23',
                  BibA: '57',
                  NameA: '白巴特尔',
                  TeamA: '内蒙古',
                  ScoreA: '0',
                  Result: null,
                  ScoreB: '0',
                  NameB: '万云国',
                  TeamB: '河北',
                  BibB: '23'
                },
                {
                  TableNo: '24',
                  BibA: '24',
                  NameA: '徐志行',
                  TeamA: '青岛',
                  ScoreA: '0',
                  Result: null,
                  ScoreB: '0',
                  NameB: '陈瑜亮',
                  TeamB: '海南',
                  BibB: '58'
                },
                {
                  TableNo: '25',
                  BibA: '59',
                  NameA: '呼木吉勒图',
                  TeamA: '内蒙古',
                  ScoreA: '0',
                  Result: null,
                  ScoreB: '0',
                  NameB: '余瑞源',
                  TeamB: '江苏',
                  BibB: '25'
                },
                {
                  TableNo: '26',
                  BibA: '26',
                  NameA: '林晨',
                  TeamA: '江苏',
                  ScoreA: '0',
                  Result: null,
                  ScoreB: '0',
                  NameB: '刘将',
                  TeamB: '西藏',
                  BibB: '60'
                },
                {
                  TableNo: '27',
                  BibA: '61',
                  NameA: '鲁宽',
                  TeamA: '江西',
                  ScoreA: '0',
                  Result: null,
                  ScoreB: '0',
                  NameB: '汪自力',
                  TeamB: '深圳',
                  BibB: '27'
                },
                {
                  TableNo: '28',
                  BibA: '28',
                  NameA: '王晨',
                  TeamA: '重庆',
                  ScoreA: '0',
                  Result: null,
                  ScoreB: '0',
                  NameB: '史一凯',
                  TeamB: '宁波',
                  BibB: '62'
                },
                {
                  TableNo: '29',
                  BibA: '63',
                  NameA: '图木尔巴根',
                  TeamA: '内蒙古',
                  ScoreA: '0',
                  Result: null,
                  ScoreB: '0',
                  NameB: '马中涵',
                  TeamB: '青岛',
                  BibB: '29'
                },
                {
                  TableNo: '30',
                  BibA: '30',
                  NameA: '楼一平',
                  TeamA: '上海',
                  ScoreA: '0',
                  Result: null,
                  ScoreB: '0',
                  NameB: '王飞',
                  TeamB: '河南',
                  BibB: '64'
                },
                {
                  TableNo: '31',
                  BibA: '65',
                  NameA: '王鸿',
                  TeamA: '江西',
                  ScoreA: '0',
                  Result: null,
                  ScoreB: '0',
                  NameB: '王一业',
                  TeamB: '黑龙江',
                  BibB: '31'
                },
                {
                  TableNo: '32',
                  BibA: '32',
                  NameA: '李师龙',
                  TeamA: '广东',
                  ScoreA: '0',
                  Result: null,
                  ScoreB: '0',
                  NameB: '汪进',
                  TeamB: '江西',
                  BibB: '66'
                },
                {
                  TableNo: '33',
                  BibA: '67',
                  NameA: '张海威',
                  TeamA: '福建',
                  ScoreA: '0',
                  Result: null,
                  ScoreB: '0',
                  NameB: '陈琪',
                  TeamB: '江苏',
                  BibB: '33'
                },
                {
                  TableNo: '34',
                  BibA: '34',
                  NameA: '刘冠初',
                  TeamA: '四川',
                  ScoreA: '0',
                  Result: null,
                  ScoreB: '0',
                  NameB: '张少峰',
                  TeamB: '青海',
                  BibB: '68'
                },
                {
                  TableNo: '',
                  BibA: '69',
                  NameA: '邹智平',
                  TeamA: '江西',
                  ScoreA: '0',
                  Result: null,
                  ScoreB: null,
                  NameB: 'Bye',
                  TeamB: null,
                  BibB: null
                }
              ],
              singleItemList: [{ name: '国际象棋', logo: l('c6de0'), flag: !0 }]
            }
          },
          watch: {},
          computed: {},
          filters: {},
          components: {},
          methods: {}
        },
        Z = Q,
        ee = (l('d054'), Object(c['a'])(Z, z, K, !1, null, 'ad03cd58', null)),
        ae = ee.exports,
        le = function() {
          var e = this,
            a = e._self._c
          return a('div', [
            a(
              'div',
              { staticClass: 'item-info' },
              [
                e._l(e.singleItemList, function(l, t) {
                  return a('div', { key: t, class: ['single-item', l.flag ? 'active-item' : ''] }, [
                    a('div', [e._v(e._s(l.name))]),
                    a('div', { staticClass: 'item-logo' }, [
                      a('img', { attrs: { src: l.logo, alt: '' } })
                    ])
                  ])
                }),
                a('div', { staticClass: 'single-item-info' }, [
                  a('div', { staticClass: 'info-line1' }, [
                    a('div', [e._v(e._s(e.gameTitle))]),
                    a('div', { staticClass: 'venue' }, [e._v(e._s(e.venueName))])
                  ]),
                  a('div', { staticClass: 'info-line2' }, [
                    a('div', { staticClass: 'report-name' }, [e._v('成绩公告')]),
                    a('div', [e._v(e._s(e.gameTime))])
                  ])
                ])
              ],
              2
            ),
            a(
              'div',
              { staticClass: 'game-info' },
              [
                a('el-col', { attrs: { span: 3 } }, [a('div', [e._v(e._s(e.tableData.column1))])]),
                a('el-col', { attrs: { span: 9 } }, [
                  a(
                    'div',
                    {
                      staticClass: 'game-b',
                      staticStyle: { 'margin-left': '-10px', color: 'red' }
                    },
                    [e._v(e._s(e.tableData.column2))]
                  )
                ]),
                a('el-col', { attrs: { span: 3 } }, [
                  a('div', { staticClass: 'game-vs' }, [e._v(e._s(e.tableData.column3))])
                ]),
                a('el-col', { attrs: { span: 9 } }, [
                  a('div', { staticClass: 'game-a' }, [e._v(' ' + e._s(e.tableData.column4) + ' ')])
                ])
              ],
              1
            ),
            a(
              'div',
              { staticClass: 'game-table-data' },
              [
                a(
                  'el-row',
                  { attrs: { gutter: 24 } },
                  [
                    a(
                      'el-col',
                      { staticClass: 'col-style-1', attrs: { span: 3 } },
                      [
                        a(
                          'el-table',
                          { staticStyle: { width: '100%' }, attrs: { data: e.resultList } },
                          [
                            a('el-table-column', {
                              attrs: {
                                prop: 'TableNo',
                                label: '台次',
                                'header-align': 'center',
                                align: 'center'
                              }
                            })
                          ],
                          1
                        )
                      ],
                      1
                    ),
                    a(
                      'el-col',
                      { staticClass: 'col-style', attrs: { span: 9 } },
                      [
                        a(
                          'el-table',
                          { staticStyle: { width: '100%' }, attrs: { data: e.resultList } },
                          [
                            a('el-table-column', { attrs: { prop: 'Code_R', label: '编号' } }),
                            a('el-table-column', {
                              attrs: { prop: 'Unit_R', label: '单位', 'min-width': '130%' }
                            }),
                            a('el-table-column', {
                              attrs: { label: '姓名' },
                              scopedSlots: e._u([
                                {
                                  key: 'default',
                                  fn: function(l) {
                                    return [
                                      a('div', { staticStyle: { color: 'rgba(230,24,71,1)' } }, [
                                        e._v(' ' + e._s(l.row.Name_R) + ' ')
                                      ])
                                    ]
                                  }
                                }
                              ])
                            }),
                            a('el-table-column', {
                              attrs: {
                                prop: 'Grade_R',
                                label: '前轮积分',
                                'header-align': 'center',
                                align: 'center'
                              }
                            }),
                            a('el-table-column', {
                              attrs: {
                                prop: 'Score_R',
                                label: '成绩',
                                'header-align': 'center',
                                align: 'center'
                              }
                            })
                          ],
                          1
                        )
                      ],
                      1
                    ),
                    a(
                      'el-col',
                      { staticClass: 'col-style', attrs: { span: 3 } },
                      [
                        a(
                          'el-table',
                          {
                            staticStyle: { width: '100%' },
                            attrs: { data: e.resultList, 'header-align': 'center' }
                          },
                          [
                            a('el-table-column', {
                              attrs: { prop: '', 'header-align': 'center' },
                              scopedSlots: e._u([
                                {
                                  key: 'header',
                                  fn: function(e) {
                                    return [a('div', { staticClass: 'game-vs-logo' })]
                                  }
                                }
                              ])
                            })
                          ],
                          1
                        )
                      ],
                      1
                    ),
                    a(
                      'el-col',
                      { staticClass: 'col-style-2', attrs: { span: 9 } },
                      [
                        a(
                          'el-table',
                          { staticStyle: { width: '100%' }, attrs: { data: e.resultList } },
                          [
                            a('el-table-column', {
                              attrs: {
                                prop: 'Score_B',
                                label: '成绩',
                                'header-align': 'center',
                                align: 'center'
                              }
                            }),
                            a('el-table-column', {
                              attrs: {
                                prop: 'Grade_B',
                                label: '前轮积分',
                                'header-align': 'center',
                                align: 'center'
                              }
                            }),
                            a('el-table-column', {
                              attrs: { label: '姓名' },
                              scopedSlots: e._u([
                                {
                                  key: 'default',
                                  fn: function(l) {
                                    return [
                                      a('div', { staticStyle: { color: 'rgba(60,135,247,1)' } }, [
                                        e._v(' ' + e._s(l.row.Name_B) + ' ')
                                      ])
                                    ]
                                  }
                                }
                              ])
                            }),
                            a('el-table-column', {
                              attrs: { prop: 'Unit_B', label: '单位', 'min-width': '130%' }
                            }),
                            a('el-table-column', { attrs: { prop: 'Code_B', label: '编号' } })
                          ],
                          1
                        )
                      ],
                      1
                    )
                  ],
                  1
                )
              ],
              1
            )
          ])
        },
        te = [],
        ne = {
          created: function() {
            ;(this.venueName = this.$route.query.venueName),
              (this.gameTime = this.$route.query.gameTime),
              (this.gameTitle = this.$route.query.gameTitle)
          },
          props: {},
          data: function() {
            return {
              tableData: { column1: '对阵', column2: '红方', column3: 'VS', column4: '黑方' },
              venueName: '',
              gameTime: '',
              gameTitle: '',
              resultList: [
                {
                  TableNo: '1',
                  Code_R: '1',
                  Unit_R: '内蒙古',
                  Name_R: '蔚  强',
                  Grade_R: null,
                  Grade_B: null,
                  Name_B: '宋国强',
                  Unit_B: '火车头体协',
                  Code_B: '2',
                  Score_R: '2.0',
                  Score_B: '0.0'
                },
                {
                  TableNo: '2',
                  Code_R: '4',
                  Unit_R: '新疆',
                  Name_R: '崔淞博',
                  Grade_R: null,
                  Grade_B: null,
                  Name_B: '连泽特',
                  Unit_B: '石油体协',
                  Code_B: '3',
                  Score_R: '0.0',
                  Score_B: '2.0'
                },
                {
                  TableNo: '3',
                  Code_R: '5',
                  Unit_R: '四川',
                  Name_R: '孟  辰',
                  Grade_R: null,
                  Grade_B: null,
                  Name_B: '周  军',
                  Unit_B: '山西',
                  Code_B: '6',
                  Score_R: '2.0',
                  Score_B: '0.0'
                },
                {
                  TableNo: '4',
                  Code_R: '8',
                  Unit_R: '甘肃',
                  Name_R: '何  刚',
                  Grade_R: null,
                  Grade_B: null,
                  Name_B: '徐  超',
                  Unit_B: '江苏',
                  Code_B: '7',
                  Score_R: '1.0',
                  Score_B: '1.0'
                },
                {
                  TableNo: '5',
                  Code_R: '9',
                  Unit_R: '北京',
                  Name_R: '蒋  川',
                  Grade_R: null,
                  Grade_B: null,
                  Name_B: '杨  浩',
                  Unit_B: '新疆',
                  Code_B: '10',
                  Score_R: '2.0',
                  Score_B: '0.0'
                },
                {
                  TableNo: '6',
                  Code_R: '12',
                  Unit_R: '煤矿体协',
                  Name_R: '窦  超',
                  Grade_R: null,
                  Grade_B: null,
                  Name_B: '谢业枧',
                  Unit_B: '湖南',
                  Code_B: '11',
                  Score_R: '0.0',
                  Score_B: '2.0'
                },
                {
                  TableNo: '7',
                  Code_R: '13',
                  Unit_R: '上海',
                  Name_R: '洪  智',
                  Grade_R: null,
                  Grade_B: null,
                  Name_B: '梁  军',
                  Unit_B: '甘肃',
                  Code_B: '14',
                  Score_R: '2.0',
                  Score_B: '0.0'
                },
                {
                  TableNo: '8',
                  Code_R: '16',
                  Unit_R: '云南',
                  Name_R: '黎德志',
                  Grade_R: null,
                  Grade_B: null,
                  Name_B: '汪  洋',
                  Unit_B: '湖北',
                  Code_B: '15',
                  Score_R: '1.0',
                  Score_B: '1.0'
                },
                {
                  TableNo: '9',
                  Code_R: '17',
                  Unit_R: '浙江',
                  Name_R: '赵鑫鑫',
                  Grade_R: null,
                  Grade_B: null,
                  Name_B: '陈富杰',
                  Unit_B: '山东',
                  Code_B: '18',
                  Score_R: '2.0',
                  Score_B: '0.0'
                },
                {
                  TableNo: '10',
                  Code_R: '20',
                  Unit_R: '吉林',
                  Name_R: '曹  霖',
                  Grade_R: null,
                  Grade_B: null,
                  Name_B: '郝继超',
                  Unit_B: '黑龙江',
                  Code_B: '19',
                  Score_R: '0.0',
                  Score_B: '2.0'
                },
                {
                  TableNo: '11',
                  Code_R: '21',
                  Unit_R: '黑龙江',
                  Name_R: '崔  革',
                  Grade_R: null,
                  Grade_B: null,
                  Name_B: '宋长海',
                  Unit_B: '青海',
                  Code_B: '22',
                  Score_R: '2.0',
                  Score_B: '0.0'
                },
                {
                  TableNo: '12',
                  Code_R: '24',
                  Unit_R: '大连',
                  Name_R: '李迈新',
                  Grade_R: null,
                  Grade_B: null,
                  Name_B: '党  斐',
                  Unit_B: '河南',
                  Code_B: '23',
                  Score_R: '1.0',
                  Score_B: '1.0'
                },
                {
                  TableNo: '13',
                  Code_R: '25',
                  Unit_R: '厦门',
                  Name_R: '郑一泓',
                  Grade_R: null,
                  Grade_B: null,
                  Name_B: '卓赞烽',
                  Unit_B: '福建',
                  Code_B: '26',
                  Score_R: '2.0',
                  Score_B: '0.0'
                },
                {
                  TableNo: '14',
                  Code_R: '28',
                  Unit_R: '煤矿体协',
                  Name_R: '蒋凤山',
                  Grade_R: null,
                  Grade_B: null,
                  Name_B: '宿少峰',
                  Unit_B: '内蒙古',
                  Code_B: '27',
                  Score_R: '0.0',
                  Score_B: '2.0'
                },
                {
                  TableNo: '15',
                  Code_R: '29',
                  Unit_R: '宁夏',
                  Name_R: '刘  明',
                  Grade_R: null,
                  Grade_B: null,
                  Name_B: '梁辉远',
                  Unit_B: '山西',
                  Code_B: '30',
                  Score_R: '1.0',
                  Score_B: '1.0'
                },
                {
                  TableNo: '16',
                  Code_R: '32',
                  Unit_R: '北京',
                  Name_R: '靳玉砚',
                  Grade_R: null,
                  Grade_B: null,
                  Name_B: '武俊强',
                  Unit_B: '河南',
                  Code_B: '31',
                  Score_R: '0.0',
                  Score_B: '2.0'
                },
                {
                  TableNo: '17',
                  Code_R: '33',
                  Unit_R: '山东',
                  Name_R: '谢  岿',
                  Grade_R: null,
                  Grade_B: null,
                  Name_B: '刘光辉',
                  Unit_B: '江西',
                  Code_B: '34',
                  Score_R: '2.0',
                  Score_B: '0.0'
                },
                {
                  TableNo: '18',
                  Code_R: '36',
                  Unit_R: '辽宁',
                  Name_R: '李冠男',
                  Grade_R: null,
                  Grade_B: null,
                  Name_B: '程  鸣',
                  Unit_B: '江苏',
                  Code_B: '35',
                  Score_R: '0.0',
                  Score_B: '2.0'
                },
                {
                  TableNo: '19',
                  Code_R: '37',
                  Unit_R: '河北',
                  Name_R: '陆伟韬',
                  Grade_R: null,
                  Grade_B: null,
                  Name_B: '黄光颖',
                  Unit_B: '广东',
                  Code_B: '38',
                  Score_R: '1.0',
                  Score_B: '1.0'
                },
                {
                  TableNo: '20',
                  Code_R: '40',
                  Unit_R: '火车头体协',
                  Name_R: '杨成福',
                  Grade_R: null,
                  Grade_B: null,
                  Name_B: '申  鹏',
                  Unit_B: '河北',
                  Code_B: '39',
                  Score_R: '0.0',
                  Score_B: '2.0'
                },
                {
                  TableNo: '21',
                  Code_R: '41',
                  Unit_R: '上海',
                  Name_R: '孙勇征',
                  Grade_R: null,
                  Grade_B: null,
                  Name_B: '邓志强',
                  Unit_B: '江西',
                  Code_B: '42',
                  Score_R: '2.0',
                  Score_B: '0.0'
                },
                {
                  TableNo: '22',
                  Code_R: '44',
                  Unit_R: '福建',
                  Name_R: '王晓华',
                  Grade_R: null,
                  Grade_B: null,
                  Name_B: '黄海林',
                  Unit_B: '广东',
                  Code_B: '43',
                  Score_R: '0.0',
                  Score_B: '2.0'
                },
                {
                  TableNo: '23',
                  Code_R: '45',
                  Unit_R: '湖北',
                  Name_R: '柳大华',
                  Grade_R: null,
                  Grade_B: null,
                  Name_B: '张  欣',
                  Unit_B: '石油体协',
                  Code_B: '46',
                  Score_R: '2.0',
                  Score_B: '0.0'
                },
                {
                  TableNo: '24',
                  Code_R: '48',
                  Unit_R: '陕西',
                  Name_R: '吕建陆',
                  Grade_R: null,
                  Grade_B: null,
                  Name_B: '黄学谦',
                  Unit_B: '香港象棋总会',
                  Code_B: '47',
                  Score_R: '0.0',
                  Score_B: '2.0'
                },
                {
                  TableNo: '25',
                  Code_R: '49',
                  Unit_R: '辽宁',
                  Name_R: '钟少鸿',
                  Grade_R: null,
                  Grade_B: null,
                  Name_B: '陈志刚',
                  Unit_B: '青海',
                  Code_B: '50',
                  Score_R: '2.0',
                  Score_B: '0.0'
                },
                {
                  TableNo: '26',
                  Code_R: '52',
                  Unit_R: '陕西',
                  Name_R: '贺岁学',
                  Grade_R: null,
                  Grade_B: null,
                  Name_B: '郑惟桐',
                  Unit_B: '四川',
                  Code_B: '51',
                  Score_R: '0.0',
                  Score_B: '2.0'
                },
                {
                  TableNo: '27',
                  Code_R: '53',
                  Unit_R: '浙江',
                  Name_R: '黄竹风',
                  Grade_R: null,
                  Grade_B: null,
                  Name_B: '林文汉',
                  Unit_B: '厦门',
                  Code_B: '54',
                  Score_R: '2.0',
                  Score_B: '0.0'
                }
              ],
              singleItemList: [{ name: '象棋', logo: l('53ed'), flag: !0 }]
            }
          },
          watch: {},
          computed: {},
          filters: {},
          components: {},
          methods: {}
        },
        me = ne,
        oe = (l('429b'), Object(c['a'])(me, le, te, !1, null, '5cfdbc1c', null)),
        se = oe.exports,
        ie = function() {
          var e = this,
            a = e._self._c
          return a('div', [
            a(
              'div',
              { staticClass: 'item-info' },
              [
                e._l(e.singleItemList, function(l, t) {
                  return a('div', { key: t, class: ['single-item', l.flag ? 'active-item' : ''] }, [
                    a('div', [e._v(e._s(l.name))]),
                    a('div', { staticClass: 'item-logo' }, [
                      a('img', { attrs: { src: l.logo, alt: '' } })
                    ])
                  ])
                }),
                a('div', { staticClass: 'single-item-info' }, [
                  a('div', { staticClass: 'info-line1' }, [
                    a('div', [e._v(e._s(e.gameTitle))]),
                    a('div', { staticClass: 'venue' }, [e._v(e._s(e.venueName))])
                  ]),
                  a('div', { staticClass: 'info-line2' }, [
                    a('div', { staticClass: 'report-name' }, [e._v('成绩公告')]),
                    a('div', [e._v(e._s(e.gameTime))])
                  ])
                ])
              ],
              2
            ),
            a(
              'div',
              { staticClass: 'game-info' },
              [
                a('el-col', { attrs: { span: 3 } }, [a('div', [e._v(e._s(e.tableData.column1))])]),
                a('el-col', { attrs: { span: 9 } }, [
                  a('div', { staticClass: 'game-a', staticStyle: { color: 'rgba(0,0,0,0)' } }, [
                    e._v(e._s(e.tableData.column2))
                  ])
                ]),
                a('el-col', { attrs: { span: 3 } }, [
                  a('div', { staticClass: 'game-vs' }, [e._v(e._s(e.tableData.column3))])
                ]),
                a('el-col', { attrs: { span: 9 } }, [
                  a('div', { staticClass: 'game-b', staticStyle: { color: 'rgba(0,0,0,0)' } }, [
                    e._v(' ' + e._s(e.tableData.column4) + ' ')
                  ])
                ])
              ],
              1
            ),
            a(
              'div',
              { staticClass: 'game-table-data' },
              [
                a(
                  'el-row',
                  { attrs: { gutter: 24 } },
                  [
                    a(
                      'el-col',
                      { staticClass: 'col-style-1', attrs: { span: 3 } },
                      [
                        a(
                          'el-table',
                          { staticStyle: { width: '100%' }, attrs: { data: e.resultList } },
                          [
                            a('el-table-column', {
                              attrs: {
                                prop: 'table',
                                label: '台号',
                                'header-align': 'center',
                                align: 'center'
                              }
                            })
                          ],
                          1
                        )
                      ],
                      1
                    ),
                    a(
                      'el-col',
                      { staticClass: 'col-style', attrs: { span: 9 } },
                      [
                        a(
                          'el-table',
                          { staticStyle: { width: '100%' }, attrs: { data: e.resultList } },
                          [
                            a('el-table-column', {
                              attrs: { label: '队编号', 'header-align': 'center', align: 'center' },
                              scopedSlots: e._u([
                                {
                                  key: 'default',
                                  fn: function(l) {
                                    return [
                                      a('div', { class: e.getTableOtherStyle(l.row.GroupName2) }, [
                                        e._v(' ' + e._s(l.row.Team1) + ' ')
                                      ])
                                    ]
                                  }
                                }
                              ])
                            }),
                            a('el-table-column', {
                              attrs: { label: '单位/姓名', 'min-width': '130%' },
                              scopedSlots: e._u([
                                {
                                  key: 'default',
                                  fn: function(l) {
                                    return [
                                      a(
                                        'div',
                                        { class: [e.getTableTeamStyle(l.row.GroupName1, '')] },
                                        [e._v(' ' + e._s(l.row.GroupName1) + ' ')]
                                      )
                                    ]
                                  }
                                }
                              ])
                            }),
                            a('el-table-column', {
                              attrs: { label: '积分', 'header-align': 'center', align: 'center' },
                              scopedSlots: e._u([
                                {
                                  key: 'default',
                                  fn: function(l) {
                                    return [
                                      a('div', { class: e.getTableOtherStyle(l.row.GroupName2) }, [
                                        e._v(' ' + e._s(l.row.JiFen1) + ' ')
                                      ])
                                    ]
                                  }
                                }
                              ])
                            })
                          ],
                          1
                        )
                      ],
                      1
                    ),
                    a(
                      'el-col',
                      { staticClass: 'col-style', attrs: { span: 3 } },
                      [
                        a(
                          'el-table',
                          {
                            staticStyle: { width: '100%' },
                            attrs: { data: e.resultList, 'header-align': 'center' }
                          },
                          [
                            a('el-table-column', {
                              attrs: { prop: 'Result', 'header-align': 'center', align: 'center' },
                              scopedSlots: e._u([
                                {
                                  key: 'header',
                                  fn: function(e) {
                                    return [a('div', { staticClass: 'game-vs-logo' })]
                                  }
                                }
                              ])
                            })
                          ],
                          1
                        )
                      ],
                      1
                    ),
                    a(
                      'el-col',
                      { staticClass: 'col-style-2', attrs: { span: 9 } },
                      [
                        a(
                          'el-table',
                          { staticStyle: { width: '100%' }, attrs: { data: e.resultList } },
                          [
                            a('el-table-column', {
                              attrs: { label: '积分', 'header-align': 'center', align: 'center' },
                              scopedSlots: e._u([
                                {
                                  key: 'default',
                                  fn: function(l) {
                                    return [
                                      a('div', { class: e.getTableOtherStyle(l.row.GroupName2) }, [
                                        e._v(' ' + e._s(l.row.JiFen2) + ' ')
                                      ])
                                    ]
                                  }
                                }
                              ])
                            }),
                            a('el-table-column', {
                              attrs: { label: '队编号', 'header-align': 'center', align: 'center' },
                              scopedSlots: e._u([
                                {
                                  key: 'default',
                                  fn: function(l) {
                                    return [
                                      a('div', { class: e.getTableOtherStyle(l.row.GroupName2) }, [
                                        e._v(' ' + e._s(l.row.Team2) + ' ')
                                      ])
                                    ]
                                  }
                                }
                              ])
                            }),
                            a('el-table-column', {
                              attrs: { label: '单位/姓名', 'min-width': '130%' },
                              scopedSlots: e._u([
                                {
                                  key: 'default',
                                  fn: function(l) {
                                    return [
                                      a(
                                        'div',
                                        { class: [e.getTableTeamStyle('', l.row.GroupName2)] },
                                        [e._v(' ' + e._s(l.row.GroupName2) + ' ')]
                                      )
                                    ]
                                  }
                                }
                              ])
                            })
                          ],
                          1
                        )
                      ],
                      1
                    )
                  ],
                  1
                )
              ],
              1
            ),
            e._m(0)
          ])
        },
        re = [
          function() {
            var e = this,
              a = e._self._c
            return a('div', { staticClass: 'game-legend' }, [
              a('div', { staticClass: 'legend-title' }, [e._v('注释')]),
              a('div', { staticClass: 'legend-content' }, [
                a('span', { staticClass: 'legend-content-title' }, [e._v('*')]),
                a('span', [e._v('先手')])
              ])
            ])
          }
        ],
        ue = {
          created: function() {
            ;(this.venueName = this.$route.query.venueName),
              (this.gameTime = this.$route.query.gameTime),
              (this.gameTitle = this.$route.query.gameTitle)
          },
          props: {},
          data: function() {
            return {
              tableData: { column1: '对阵', column2: '-', column3: '当前成绩', column4: '-' },
              venueName: '',
              gameTime: '',
              gameTitle: '',
              resultList: [
                {
                  table: null,
                  Team1: '14',
                  GroupName1: '山东省代表队',
                  Result: '',
                  Team2: '1',
                  GroupName2: '湖北省代表队',
                  TeamResult: 'Y',
                  JiFen1: '4',
                  JiFen2: '4'
                },
                {
                  table: '1-1',
                  Team1: '',
                  GroupName1: '*王晨帆',
                  Result: '1:1',
                  Team2: '',
                  GroupName2: '泮忆铭',
                  TeamResult: '',
                  JiFen1: '',
                  JiFen2: ''
                },
                {
                  table: '1-2',
                  Team1: '',
                  GroupName1: '王亨鑫',
                  Result: '0:2',
                  Team2: '',
                  GroupName2: '*李振宇',
                  TeamResult: '',
                  JiFen1: '',
                  JiFen2: ''
                },
                {
                  table: '1-3',
                  Team1: '',
                  GroupName1: '*李郑义',
                  Result: '1:1',
                  Team2: '',
                  GroupName2: '赵汗青',
                  TeamResult: '',
                  JiFen1: '',
                  JiFen2: ''
                },
                {
                  table: '',
                  Team1: '4',
                  GroupName1: '内蒙古代表队',
                  Result: '',
                  Team2: '7',
                  GroupName2: '浙江省代表队',
                  TeamResult: 'Y',
                  JiFen1: '4',
                  JiFen2: '4'
                },
                {
                  table: '2-1',
                  Team1: '',
                  GroupName1: '*钢苏和',
                  Result: '0:2',
                  Team2: '',
                  GroupName2: '金坚强',
                  TeamResult: '',
                  JiFen1: '',
                  JiFen2: ''
                },
                {
                  table: '2-2',
                  Team1: '',
                  GroupName1: '郑策',
                  Result: '1:1',
                  Team2: '',
                  GroupName2: '*李宇翔',
                  TeamResult: '',
                  JiFen1: '',
                  JiFen2: ''
                },
                {
                  table: '2-3',
                  Team1: '',
                  GroupName1: '*赛娅',
                  Result: '1:1',
                  Team2: '',
                  GroupName2: '叶春花',
                  TeamResult: '',
                  JiFen1: '',
                  JiFen2: ''
                },
                {
                  table: '',
                  Team1: '6',
                  GroupName1: '四川省代表队',
                  Result: '',
                  Team2: '15',
                  GroupName2: '湖南省代表队',
                  TeamResult: 'Y',
                  JiFen1: '3',
                  JiFen2: '3'
                },
                {
                  table: '3-1',
                  Team1: '',
                  GroupName1: '*周伟',
                  Result: '2:0',
                  Team2: '',
                  GroupName2: '容嘉荣',
                  TeamResult: '',
                  JiFen1: '',
                  JiFen2: ''
                },
                {
                  table: '3-2',
                  Team1: '',
                  GroupName1: '季炜明',
                  Result: '1:1',
                  Team2: '',
                  GroupName2: '*段宗昊',
                  TeamResult: '',
                  JiFen1: '',
                  JiFen2: ''
                },
                {
                  table: '3-3',
                  Team1: '',
                  GroupName1: '*阿拉腾花',
                  Result: '1:1',
                  Team2: '',
                  GroupName2: '周楚卉',
                  TeamResult: '',
                  JiFen1: '',
                  JiFen2: ''
                },
                {
                  table: '',
                  Team1: '17',
                  GroupName1: '山西省体育局',
                  Result: '',
                  Team2: '9',
                  GroupName2: '广东省代表队',
                  TeamResult: 'Y',
                  JiFen1: '3',
                  JiFen2: '2'
                },
                {
                  table: '4-1',
                  Team1: '',
                  GroupName1: '*胡卓群',
                  Result: '2:0',
                  Team2: '',
                  GroupName2: '罗百顺',
                  TeamResult: '',
                  JiFen1: '',
                  JiFen2: ''
                },
                {
                  table: '4-2',
                  Team1: '',
                  GroupName1: '韩旭',
                  Result: '2:0',
                  Team2: '',
                  GroupName2: '*王振宇',
                  TeamResult: '',
                  JiFen1: '',
                  JiFen2: ''
                },
                {
                  table: '4-3',
                  Team1: '',
                  GroupName1: '*廉博',
                  Result: '2:0',
                  Team2: '',
                  GroupName2: '罗柏莹',
                  TeamResult: '',
                  JiFen1: '',
                  JiFen2: ''
                },
                {
                  table: '',
                  Team1: '2',
                  GroupName1: '河北省代表队',
                  Result: '',
                  Team2: '16',
                  GroupName2: '宁波市代表队',
                  TeamResult: 'Y',
                  JiFen1: '2',
                  JiFen2: '2'
                },
                {
                  table: '5-1',
                  Team1: '',
                  GroupName1: '*田晟宬',
                  Result: '2:0',
                  Team2: '',
                  GroupName2: '盛宏安',
                  TeamResult: '',
                  JiFen1: '',
                  JiFen2: ''
                },
                {
                  table: '5-2',
                  Team1: '',
                  GroupName1: '刘昊炎',
                  Result: '2:0',
                  Team2: '',
                  GroupName2: '*戚成龙',
                  TeamResult: '',
                  JiFen1: '',
                  JiFen2: ''
                },
                {
                  table: '5-3',
                  Team1: '',
                  GroupName1: '*李晶',
                  Result: '2:0',
                  Team2: '',
                  GroupName2: '空缺',
                  TeamResult: '',
                  JiFen1: '',
                  JiFen2: ''
                },
                {
                  table: '',
                  Team1: '10',
                  GroupName1: '天津市代表队',
                  Result: '',
                  Team2: '20',
                  GroupName2: '重庆市代表队',
                  TeamResult: 'Y',
                  JiFen1: '2',
                  JiFen2: '2'
                },
                {
                  table: '6-1',
                  Team1: '',
                  GroupName1: '*吴弋泉',
                  Result: '0:2',
                  Team2: '',
                  GroupName2: '张磊',
                  TeamResult: '',
                  JiFen1: '',
                  JiFen2: ''
                },
                {
                  table: '6-2',
                  Team1: '',
                  GroupName1: '石岳',
                  Result: '0:2',
                  Team2: '',
                  GroupName2: '*熊云博',
                  TeamResult: '',
                  JiFen1: '',
                  JiFen2: ''
                },
                {
                  table: '6-3',
                  Team1: '',
                  GroupName1: '*邓莉',
                  Result: '2:0',
                  Team2: '',
                  GroupName2: '万佳丽',
                  TeamResult: '',
                  JiFen1: '',
                  JiFen2: ''
                },
                {
                  table: '',
                  Team1: '13',
                  GroupName1: '深圳市代表队',
                  Result: '',
                  Team2: '11',
                  GroupName2: '江西省代表队',
                  TeamResult: 'Y',
                  JiFen1: '2',
                  JiFen2: '2'
                },
                {
                  table: '7-1',
                  Team1: '',
                  GroupName1: '*任皓麟',
                  Result: '2:0',
                  Team2: '',
                  GroupName2: '汪易峰',
                  TeamResult: '',
                  JiFen1: '',
                  JiFen2: ''
                },
                {
                  table: '7-2',
                  Team1: '',
                  GroupName1: '蔡杰程',
                  Result: '1:1',
                  Team2: '',
                  GroupName2: '*陈治政',
                  TeamResult: '',
                  JiFen1: '',
                  JiFen2: ''
                },
                {
                  table: '7-3',
                  Team1: '',
                  GroupName1: '*余曼琪',
                  Result: '1:1',
                  Team2: '',
                  GroupName2: '俞盈',
                  TeamResult: '',
                  JiFen1: '',
                  JiFen2: ''
                },
                {
                  table: '',
                  Team1: '12',
                  GroupName1: '青岛市代表队',
                  Result: '',
                  Team2: '21',
                  GroupName2: '江苏省代表队',
                  TeamResult: 'Y',
                  JiFen1: '2',
                  JiFen2: '2'
                },
                {
                  table: '8-1',
                  Team1: '',
                  GroupName1: '*丁家宝',
                  Result: '2:0',
                  Team2: '',
                  GroupName2: '孙铭',
                  TeamResult: '',
                  JiFen1: '',
                  JiFen2: ''
                },
                {
                  table: '8-2',
                  Team1: '',
                  GroupName1: '于庚艺',
                  Result: '0:2',
                  Team2: '',
                  GroupName2: '*杨浩颖',
                  TeamResult: '',
                  JiFen1: '',
                  JiFen2: ''
                },
                {
                  table: '8-3',
                  Team1: '',
                  GroupName1: '*王艺晓',
                  Result: '0:2',
                  Team2: '',
                  GroupName2: '王清圆',
                  TeamResult: '',
                  JiFen1: '',
                  JiFen2: ''
                },
                {
                  table: '',
                  Team1: '3',
                  GroupName1: '吉林省代表队',
                  Result: '',
                  Team2: '24',
                  GroupName2: '河南省代表队',
                  TeamResult: 'Y',
                  JiFen1: '1',
                  JiFen2: '2'
                },
                {
                  table: '9-1',
                  Team1: '',
                  GroupName1: '*汪文松',
                  Result: '2:0',
                  Team2: '',
                  GroupName2: '秦睿彤',
                  TeamResult: '',
                  JiFen1: '',
                  JiFen2: ''
                },
                {
                  table: '9-2',
                  Team1: '',
                  GroupName1: '邱浩纯',
                  Result: '2:0',
                  Team2: '',
                  GroupName2: '*甄士豪',
                  TeamResult: '',
                  JiFen1: '',
                  JiFen2: ''
                },
                {
                  table: '9-3',
                  Team1: '',
                  GroupName1: '*刘铭',
                  Result: '2:0',
                  Team2: '',
                  GroupName2: '侯奕含',
                  TeamResult: '',
                  JiFen1: '',
                  JiFen2: ''
                },
                {
                  table: '',
                  Team1: '18',
                  GroupName1: '安徽省代表队',
                  Result: '',
                  Team2: '5',
                  GroupName2: '上海市代表队',
                  TeamResult: 'Y',
                  JiFen1: '1',
                  JiFen2: '1'
                },
                {
                  table: '10-1',
                  Team1: '',
                  GroupName1: '*汪润',
                  Result: '2:0',
                  Team2: '',
                  GroupName2: '陶丁舟',
                  TeamResult: '',
                  JiFen1: '',
                  JiFen2: ''
                },
                {
                  table: '10-2',
                  Team1: '',
                  GroupName1: '游德义',
                  Result: '0:2',
                  Team2: '',
                  GroupName2: '*杜卓远',
                  TeamResult: '',
                  JiFen1: '',
                  JiFen2: ''
                },
                {
                  table: '10-3',
                  Team1: '',
                  GroupName1: '*王田翠',
                  Result: '0:2',
                  Team2: '',
                  GroupName2: '戴嘉璐',
                  TeamResult: '',
                  JiFen1: '',
                  JiFen2: ''
                },
                {
                  table: '',
                  Team1: '8',
                  GroupName1: '北京市代表队',
                  Result: '',
                  Team2: '22',
                  GroupName2: '黑龙江省代表队',
                  TeamResult: 'Y',
                  JiFen1: '0',
                  JiFen2: '0'
                },
                {
                  table: '11-1',
                  Team1: '',
                  GroupName1: '*干启航',
                  Result: '0:2',
                  Team2: '',
                  GroupName2: '王若维',
                  TeamResult: '',
                  JiFen1: '',
                  JiFen2: ''
                },
                {
                  table: '11-2',
                  Team1: '',
                  GroupName1: '李垠圻',
                  Result: '2:0',
                  Team2: '',
                  GroupName2: '*王子诚',
                  TeamResult: '',
                  JiFen1: '',
                  JiFen2: ''
                },
                {
                  table: '11-3',
                  Team1: '',
                  GroupName1: '*杨苓',
                  Result: '1:1',
                  Team2: '',
                  GroupName2: '么坤',
                  TeamResult: '',
                  JiFen1: '',
                  JiFen2: ''
                },
                {
                  table: '',
                  Team1: '19',
                  GroupName1: '青海省代表队',
                  Result: '',
                  Team2: '23',
                  GroupName2: '陕西省代表队',
                  TeamResult: 'Y',
                  JiFen1: '0',
                  JiFen2: '0'
                },
                {
                  table: '12-1',
                  Team1: '',
                  GroupName1: '*陈章',
                  Result: '2:0',
                  Team2: '',
                  GroupName2: '韩东霖',
                  TeamResult: null,
                  JiFen1: '',
                  JiFen2: ''
                },
                {
                  table: '12-2',
                  Team1: '',
                  GroupName1: '张乐驰',
                  Result: '2:0',
                  Team2: '',
                  GroupName2: '*张一航',
                  TeamResult: null,
                  JiFen1: '',
                  JiFen2: ''
                },
                {
                  table: '12-3',
                  Team1: '',
                  GroupName1: '*郝思佳',
                  Result: '2:0',
                  Team2: '',
                  GroupName2: '刘衡宇',
                  TeamResult: null,
                  JiFen1: '',
                  JiFen2: ''
                }
              ],
              singleItemList: [{ name: '国际跳棋', logo: l('79ec'), flag: !0 }]
            }
          },
          watch: {},
          computed: {},
          filters: {},
          components: {},
          methods: {
            getTableTeamStyle: function(e, a) {
              return (e && e.includes('代表队')) || e.includes('体育局')
                ? 'table-team-style-1'
                : (a && a.includes('代表队')) || a.includes('体育局')
                ? 'table-team-style-2'
                : void 0
            },
            getTableOtherStyle: function(e) {
              return (e && e.includes('代表队')) || e.includes('体育局')
                ? 'table-other-style'
                : void 0
            }
          }
        },
        ce = ue,
        _e = (l('9ca9'), Object(c['a'])(ce, ie, re, !1, null, 'a84c7242', null)),
        Ne = _e.exports,
        Be = function() {
          var e = this,
            a = e._self._c
          return a('div', [
            a(
              'div',
              { staticClass: 'item-info' },
              [
                e._l(e.singleItemList, function(l, t) {
                  return a('div', { key: t, class: ['single-item', l.flag ? 'active-item' : ''] }, [
                    a('div', [e._v(e._s(l.name))]),
                    a('div', { staticClass: 'item-logo' }, [
                      a('img', { attrs: { src: l.logo, alt: '' } })
                    ])
                  ])
                }),
                a('div', { staticClass: 'single-item-info' }, [
                  a('div', { staticClass: 'info-line1' }, [
                    a('div', [e._v(e._s(e.gameTitle))]),
                    a('div', { staticClass: 'venue' }, [e._v(e._s(e.venueName))])
                  ]),
                  a('div', { staticClass: 'info-line2' }, [
                    a('div', { staticClass: 'report-name' }, [e._v('成绩公告')]),
                    a('div', [e._v(e._s(e.gameTime))])
                  ])
                ])
              ],
              2
            ),
            a(
              'div',
              { staticClass: 'game-table-data' },
              [
                a(
                  'el-table',
                  { staticStyle: { width: '100%' }, attrs: { data: e.resultList } },
                  [
                    a('el-table-column', {
                      attrs: {
                        prop: 'Ranking',
                        label: '排名',
                        'header-align': 'center',
                        align: 'center'
                      }
                    }),
                    a('el-table-column', { attrs: { prop: 'Team_', label: '号码' } }),
                    a('el-table-column', {
                      attrs: { label: '姓名', prop: 'Name_', 'min-width': '130%' }
                    }),
                    a('el-table-column', { attrs: { prop: 'Gain', label: 'MP' } }),
                    a('el-table-column', { attrs: { prop: 'Penalty', label: '百分比' } })
                  ],
                  1
                )
              ],
              1
            )
          ])
        },
        Te = [],
        Re = {
          created: function() {
            ;(this.venueName = this.$route.query.venueName),
              (this.gameTime = this.$route.query.gameTime),
              (this.gameTitle = this.$route.query.gameTitle)
          },
          props: {},
          data: function() {
            return {
              venueName: '',
              gameTime: '',
              gameTitle: '',
              resultList: [
                {
                  Group: 'A1-NS',
                  Team: '8',
                  Name: '梁於河-杨玉娟',
                  Note: null,
                  Ranking: '1',
                  Team_: '7',
                  Name_: '李丽璇 - 沈冠宇',
                  Gain: '291',
                  Penalty: '64.96%'
                },
                {
                  Group: 'A1-EW',
                  Team: '1',
                  Name: '朱萍-朱敏荣',
                  Note: null,
                  Ranking: '2',
                  Team_: '18',
                  Name_: '张亚兰 - 马智威',
                  Gain: '284',
                  Penalty: '63.39%'
                },
                {
                  Group: 'A2-NS',
                  Team: '7',
                  Name: '李丽璇-沈冠宇',
                  Note: null,
                  Ranking: '3',
                  Team_: '34',
                  Name_: '郑勇 - 肖黛菁',
                  Gain: '278.8',
                  Penalty: '62.24%'
                },
                {
                  Group: 'A2-EW',
                  Team: '2',
                  Name: '张永革-吴志凌',
                  Note: null,
                  Ranking: '4',
                  Team_: '31',
                  Name_: '邓萍 - 张浩',
                  Gain: '267.2',
                  Penalty: '59.64%'
                },
                {
                  Group: 'A3-NS',
                  Team: '6',
                  Name: '赵景莉-蔡中至',
                  Note: null,
                  Ranking: '5',
                  Team_: '28',
                  Name_: '李欣 - 吴中华',
                  Gain: '263',
                  Penalty: '58.71%'
                },
                {
                  Group: 'A3-EW',
                  Team: '3',
                  Name: '赵屹涛-郝睿',
                  Note: null,
                  Ranking: '6',
                  Team_: '37',
                  Name_: '鲍卓强 - 汪慧君',
                  Gain: '261.3',
                  Penalty: '58.33%'
                },
                {
                  Group: 'A4-NS',
                  Team: '4',
                  Name: '张志芬-邹德峰',
                  Note: null,
                  Ranking: '7',
                  Team_: '32',
                  Name_: '朱德明 - 德吉措姆',
                  Gain: '260.2',
                  Penalty: '58.07%'
                },
                {
                  Group: 'A4-EW',
                  Team: '5',
                  Name: '刘洪文-柴俊阳',
                  Note: null,
                  Ranking: '8',
                  Team_: '6',
                  Name_: '赵景莉 - 蔡中至',
                  Gain: '250',
                  Penalty: '55.80%'
                },
                {
                  Group: 'B1-NS',
                  Team: '18',
                  Name: '张亚兰-马智威',
                  Note: null,
                  Ranking: '9',
                  Team_: '33',
                  Name_: '杨文瑜 - 符正平',
                  Gain: '243.8',
                  Penalty: '54.43%'
                },
                {
                  Group: 'B1-EW',
                  Team: '11',
                  Name: '徐莉-田伟强',
                  Note: null,
                  Ranking: '10',
                  Team_: '40',
                  Name_: '陈朗 - 叶宝辉',
                  Gain: '242',
                  Penalty: '54.02%'
                },
                {
                  Group: 'B2-NS',
                  Team: '17',
                  Name: '余修婷-余明泉',
                  Note: null,
                  Ranking: '11',
                  Team_: '14',
                  Name_: '文静 - 郑一心',
                  Gain: '241',
                  Penalty: '53.79%'
                },
                {
                  Group: 'B2-EW',
                  Team: '12',
                  Name: '李立-冉红',
                  Note: null,
                  Ranking: '11',
                  Team_: '8',
                  Name_: '梁於河 - 杨玉娟',
                  Gain: '241',
                  Penalty: '53.79%'
                },
                {
                  Group: 'B3-NS',
                  Team: '16',
                  Name: '陈萍-李杰',
                  Note: null,
                  Ranking: '13',
                  Team_: '26',
                  Name_: '李军 - 杜平',
                  Gain: '240',
                  Penalty: '53.57%'
                },
                {
                  Group: 'B3-EW',
                  Team: '13',
                  Name: '李维虎-梁雅莉',
                  Note: null,
                  Ranking: '14',
                  Team_: '25',
                  Name_: '王大德 - 杜冰',
                  Gain: '238',
                  Penalty: '53.13%'
                },
                {
                  Group: 'B4-NS',
                  Team: '14',
                  Name: '文静-郑一心',
                  Note: null,
                  Ranking: '14',
                  Team_: '35',
                  Name_: '刘钧 - 张敏',
                  Gain: '238',
                  Penalty: '53.13%'
                },
                {
                  Group: 'B4-EW',
                  Team: '15',
                  Name: '孙时玉-周飞卫',
                  Note: null,
                  Ranking: '16',
                  Team_: '23',
                  Name_: '赵东坡 - 陈小娟',
                  Gain: '236',
                  Penalty: '52.68%'
                },
                {
                  Group: 'C1-NS',
                  Team: '28',
                  Name: '李欣-吴中华',
                  Note: null,
                  Ranking: '17',
                  Team_: '3',
                  Name_: '赵屹涛 - 郝睿',
                  Gain: '228',
                  Penalty: '50.89%'
                },
                {
                  Group: 'C1-EW',
                  Team: '21',
                  Name: '杜健-厉民',
                  Note: null,
                  Ranking: '17',
                  Team_: '15',
                  Name_: '孙时玉 - 周飞卫',
                  Gain: '228',
                  Penalty: '50.89%'
                },
                {
                  Group: 'C2-NS',
                  Team: '27',
                  Name: '陈兆滨-籍之彦',
                  Note: null,
                  Ranking: '19',
                  Team_: '21',
                  Name_: '杜健 - 厉民',
                  Gain: '221',
                  Penalty: '49.33%'
                },
                {
                  Group: 'C2-EW',
                  Team: '22',
                  Name: '刘占学-李晓昕',
                  Note: null,
                  Ranking: '19',
                  Team_: '1',
                  Name_: '朱萍 - 朱敏荣',
                  Gain: '221',
                  Penalty: '49.33%'
                },
                {
                  Group: 'C3-NS',
                  Team: '26',
                  Name: '李军-杜平',
                  Note: null,
                  Ranking: '21',
                  Team_: '13',
                  Name_: '李维虎 - 梁雅莉',
                  Gain: '218',
                  Penalty: '48.66%'
                },
                {
                  Group: 'C3-EW',
                  Team: '23',
                  Name: '赵东坡-陈小娟',
                  Note: null,
                  Ranking: '22',
                  Team_: '16',
                  Name_: '陈萍 - 李杰',
                  Gain: '218',
                  Penalty: '48.66%'
                },
                {
                  Group: 'C4-NS',
                  Team: '24',
                  Name: '章瑜-赵杰',
                  Note: null,
                  Ranking: '23',
                  Team_: '11',
                  Name_: '徐莉 - 田伟强',
                  Gain: '214',
                  Penalty: '47.77%'
                },
                {
                  Group: 'C4-EW',
                  Team: '25',
                  Name: '王大德-杜冰',
                  Note: null,
                  Ranking: '24',
                  Team_: '17',
                  Name_: '余修婷 - 余明泉',
                  Gain: '212',
                  Penalty: '47.32%'
                },
                {
                  Group: 'D1-EW',
                  Team: '31',
                  Name: '邓萍-张浩',
                  Note: null,
                  Ranking: '25',
                  Team_: '27',
                  Name_: '陈兆滨 - 籍之彦',
                  Gain: '210',
                  Penalty: '46.88%'
                },
                {
                  Group: 'D2-NS',
                  Team: '41',
                  Name: '姚江涛-杨铭枢',
                  Note: null,
                  Ranking: '26',
                  Team_: '22',
                  Name_: '刘占学 - 李晓昕',
                  Gain: '205',
                  Penalty: '45.76%'
                },
                {
                  Group: 'D2-EW',
                  Team: '32',
                  Name: '朱德明-德吉措姆',
                  Note: null,
                  Ranking: '27',
                  Team_: '4',
                  Name_: '张志芬 - 邹德峰',
                  Gain: '198',
                  Penalty: '44.20%'
                },
                {
                  Group: 'D3-NS',
                  Team: '33',
                  Name: '杨文瑜-符正平',
                  Note: null,
                  Ranking: '28',
                  Team_: '38',
                  Name_: '陈学斌 - 严丽',
                  Gain: '195',
                  Penalty: '43.53%'
                },
                {
                  Group: 'D3-EW',
                  Team: '40',
                  Name: '陈朗-叶宝辉',
                  Note: null,
                  Ranking: '29',
                  Team_: '36',
                  Name_: '原新春 - 尚华',
                  Gain: '190.2',
                  Penalty: '42.45%'
                },
                {
                  Group: 'D4-NS',
                  Team: '39',
                  Name: '李戎-胡东华',
                  Note: null,
                  Ranking: '30',
                  Team_: '2',
                  Name_: '张永革 - 吴志凌',
                  Gain: '183',
                  Penalty: '40.85%'
                },
                {
                  Group: 'D4-EW',
                  Team: '34',
                  Name: '郑勇-肖黛菁',
                  Note: null,
                  Ranking: '31',
                  Team_: '5',
                  Name_: '刘洪文 - 柴俊阳',
                  Gain: '180',
                  Penalty: '40.18%'
                },
                {
                  Group: 'D5-NS',
                  Team: '38',
                  Name: '陈学斌-严丽',
                  Note: null,
                  Ranking: '32',
                  Team_: '24',
                  Name_: '章瑜 - 赵杰',
                  Gain: '179',
                  Penalty: '39.96%'
                },
                {
                  Group: 'D5-EW',
                  Team: '35',
                  Name: '刘钧-张敏',
                  Note: null,
                  Ranking: '33',
                  Team_: '12',
                  Name_: '李立 - 冉红',
                  Gain: '177',
                  Penalty: '39.51%'
                },
                {
                  Group: 'D6-NS',
                  Team: '37',
                  Name: '鲍卓强-汪慧君',
                  Note: null,
                  Ranking: '34',
                  Team_: '41',
                  Name_: '姚江涛 - 杨铭枢',
                  Gain: '173',
                  Penalty: '38.62%'
                },
                {
                  Group: 'D6-EW',
                  Team: '36',
                  Name: '原新春-尚华',
                  Note: null,
                  Ranking: '35',
                  Team_: '39',
                  Name_: '李戎 - 胡东华',
                  Gain: '139',
                  Penalty: '31.03%'
                }
              ],
              singleItemList: [{ name: '桥牌', logo: l('a8d9'), flag: !0 }]
            }
          },
          watch: {},
          computed: {},
          filters: {},
          components: {},
          methods: {}
        },
        be = Re,
        de = (l('b604'), Object(c['a'])(be, Be, Te, !1, null, 'ca810ade', null)),
        pe = de.exports,
        Ae = function() {
          var e = this,
            a = e._self._c
          return a('div', [
            a(
              'div',
              { staticClass: 'item-info' },
              [
                e._l(e.singleItemList, function(l, t) {
                  return a('div', { key: t, class: ['single-item', l.flag ? 'active-item' : ''] }, [
                    a('div', [e._v(e._s(l.name))]),
                    a('div', { staticClass: 'item-logo' }, [
                      a('img', { attrs: { src: l.logo, alt: '' } })
                    ])
                  ])
                }),
                a('div', { staticClass: 'single-item-info' }, [
                  a('div', { staticClass: 'info-line1' }, [
                    a('div', [e._v(e._s(e.gameTitle))]),
                    a('div', { staticClass: 'venue' }, [e._v(e._s(e.venueName))])
                  ]),
                  a('div', { staticClass: 'info-line2' }, [
                    a('div', { staticClass: 'report-name' }, [e._v('成绩公告')]),
                    a('div', [e._v(e._s(e.gameTime))])
                  ])
                ])
              ],
              2
            ),
            a(
              'div',
              { staticClass: 'game-info' },
              [
                a('el-col', { attrs: { span: 3 } }, [a('div', [e._v(e._s(e.tableData.column1))])]),
                a('el-col', { attrs: { span: 9 } }, [
                  a('div', { staticClass: 'game-a', staticStyle: { color: 'rgba(0,0,0,0)' } }, [
                    e._v(e._s(e.tableData.column2))
                  ])
                ]),
                a('el-col', { attrs: { span: 3 } }, [
                  a('div', { staticClass: 'game-vs' }, [e._v(e._s(e.tableData.column3))])
                ]),
                a('el-col', { attrs: { span: 9 } }, [
                  a('div', { staticClass: 'game-b', staticStyle: { color: 'rgba(0,0,0,0)' } }, [
                    e._v(e._s(e.tableData.column4))
                  ])
                ])
              ],
              1
            ),
            a(
              'div',
              { staticClass: 'game-table-data' },
              [
                a(
                  'el-row',
                  { attrs: { gutter: 24 } },
                  [
                    a(
                      'el-col',
                      { staticClass: 'col-style-1', attrs: { span: 3 } },
                      [
                        a(
                          'el-table',
                          { staticStyle: { width: '100%' }, attrs: { data: e.resultList } },
                          [
                            a('el-table-column', {
                              attrs: {
                                prop: 'TableNo',
                                label: '台号',
                                'header-align': 'center',
                                align: 'center'
                              }
                            })
                          ],
                          1
                        )
                      ],
                      1
                    ),
                    a(
                      'el-col',
                      { staticClass: 'col-style', attrs: { span: 9 } },
                      [
                        a(
                          'el-table',
                          { staticStyle: { width: '100%' }, attrs: { data: e.resultList } },
                          [
                            a('el-table-column', {
                              attrs: { label: '姓名' },
                              scopedSlots: e._u([
                                {
                                  key: 'default',
                                  fn: function(l) {
                                    return [
                                      a('div', { staticStyle: { color: 'rgba(230,24,71,1)' } }, [
                                        e._v(' ' + e._s(l.row['Name-A']) + ' ')
                                      ])
                                    ]
                                  }
                                }
                              ])
                            }),
                            a('el-table-column', {
                              attrs: { prop: 'Team-A', label: '单位', 'min-width': '130%' }
                            }),
                            a('el-table-column', {
                              attrs: {
                                prop: 'Score-A',
                                label: '积分',
                                'header-align': 'center',
                                align: 'center'
                              }
                            })
                          ],
                          1
                        )
                      ],
                      1
                    ),
                    a(
                      'el-col',
                      { staticClass: 'col-style', attrs: { span: 3 } },
                      [
                        a(
                          'el-table',
                          { staticStyle: { width: '100%' }, attrs: { data: e.resultList } },
                          [
                            a('el-table-column', {
                              attrs: { prop: 'Result', 'header-align': 'center', align: 'center' },
                              scopedSlots: e._u([
                                {
                                  key: 'header',
                                  fn: function(e) {
                                    return [a('div', { staticClass: 'game-vs-logo' })]
                                  }
                                }
                              ])
                            })
                          ],
                          1
                        )
                      ],
                      1
                    ),
                    a(
                      'el-col',
                      { staticClass: 'col-style-2', attrs: { span: 9 } },
                      [
                        a(
                          'el-table',
                          { staticStyle: { width: '100%' }, attrs: { data: e.resultList } },
                          [
                            a('el-table-column', {
                              attrs: { label: '姓名' },
                              scopedSlots: e._u([
                                {
                                  key: 'default',
                                  fn: function(l) {
                                    return [
                                      a('div', { staticStyle: { color: 'rgba(60,135,247,1)' } }, [
                                        e._v(' ' + e._s(l.row['Name-B']) + ' ')
                                      ])
                                    ]
                                  }
                                }
                              ])
                            }),
                            a('el-table-column', {
                              attrs: { prop: 'Team-B', label: '单位', 'min-width': '130%' }
                            }),
                            a('el-table-column', {
                              attrs: {
                                prop: 'Score-B',
                                label: '积分',
                                'header-align': 'center',
                                align: 'center'
                              }
                            })
                          ],
                          1
                        )
                      ],
                      1
                    )
                  ],
                  1
                )
              ],
              1
            )
          ])
        },
        Se = [],
        ge = {
          created: function() {
            ;(this.venueName = this.$route.query.venueName),
              (this.gameTime = this.$route.query.gameTime),
              (this.gameTitle = this.$route.query.gameTitle)
          },
          props: {},
          data: function() {
            return {
              tableData: { column1: '对阵', column2: '-', column3: '当前成绩', column4: '-' },
              venueName: '',
              gameTime: '',
              gameTitle: '',
              resultList: [
                {
                  TableNo: '1',
                  'Team-A': '江苏    ',
                  'Score-A': '[2]  ',
                  Result: '0:1   ',
                  'Team-B': '上海    ',
                  'Score-B': '[2]  ',
                  'Name-A': ' 奚振扬 (13)',
                  'Name-B': ' 朱建锋 (2) '
                },
                {
                  TableNo: '2',
                  'Team-A': '上海    ',
                  'Score-A': '[2]  ',
                  Result: '0.5:0.5  ',
                  'Team-B': '湖北    ',
                  'Score-B': '[2]  ',
                  'Name-A': ' 葛凌峰 (14)',
                  'Name-B': ' 梅凡 (6)   '
                },
                {
                  TableNo: '3',
                  'Team-A': '四川    ',
                  'Score-A': '[2]  ',
                  Result: '0:1   ',
                  'Team-B': '江苏    ',
                  'Score-B': '[2]  ',
                  'Name-A': ' 陈新 (15)  ',
                  'Name-B': ' 陈靖 (8)   '
                },
                {
                  TableNo: '4',
                  'Team-A': '广东    ',
                  'Score-A': '[2]  ',
                  Result: '1:0   ',
                  'Team-B': '海南    ',
                  'Score-B': '[2]  ',
                  'Name-A': ' 贺启发 (18)',
                  'Name-B': ' 黄圣明 (10)'
                },
                {
                  TableNo: '5',
                  'Team-A': '辽宁    ',
                  'Score-A': '[2]  ',
                  Result: '1:0   ',
                  'Team-B': '山东    ',
                  'Score-B': '[2]  ',
                  'Name-A': ' 艾显平 (20)',
                  'Name-B': ' 刘洋 (12)  '
                },
                {
                  TableNo: '6',
                  'Team-A': '山东    ',
                  'Score-A': '[1.5]',
                  Result: '1:0   ',
                  'Team-B': '江西    ',
                  'Score-B': '[2]  ',
                  'Name-A': ' 杨彦希 (3) ',
                  'Name-B': ' 洪士光 (28)'
                },
                {
                  TableNo: '7',
                  'Team-A': '湖北    ',
                  'Score-A': '[1.5]',
                  Result: '0.5:0.5  ',
                  'Team-B': '上海    ',
                  'Score-B': '[1.5]',
                  'Name-A': ' 曹冬 (5)   ',
                  'Name-B': ' 黄宇峰 (24)'
                },
                {
                  TableNo: '8',
                  'Team-A': '山东    ',
                  'Score-A': '[1.5]',
                  Result: '1:0   ',
                  'Team-B': '浙江    ',
                  'Score-B': '[1.5]',
                  'Name-A': ' 兰志仁 (9) ',
                  'Name-B': ' 谢维祥 (26)'
                },
                {
                  TableNo: '9',
                  'Team-A': '上海    ',
                  'Score-A': '[1.5]',
                  Result: '0.5:0.5  ',
                  'Team-B': '广东    ',
                  'Score-B': '[1.5]',
                  'Name-A': ' 江齐文 (11)',
                  'Name-B': ' 林奕忠 (31)'
                },
                {
                  TableNo: '10',
                  'Team-A': '海南    ',
                  'Score-A': '[1.5]',
                  Result: '0:1   ',
                  'Team-B': '湖北    ',
                  'Score-B': '[1.5]',
                  'Name-A': ' 芦海 (19)  ',
                  'Name-B': ' 殷立成 (16)'
                },
                {
                  TableNo: '11',
                  'Team-A': '河北    ',
                  'Score-A': '[1.5]',
                  Result: '1:0   ',
                  'Team-B': '北京    ',
                  'Score-B': '[1.5]',
                  'Name-A': ' 陈杰 (67)  ',
                  'Name-B': ' 胡瑜 (51)  '
                },
                {
                  TableNo: '12',
                  'Team-A': '四川    ',
                  'Score-A': '[1]  ',
                  Result: '1:0   ',
                  'Team-B': '黑龙江  ',
                  'Score-B': '[1.5]',
                  'Name-A': ' 祁观 (1)   ',
                  'Name-B': ' 王启超 (68)'
                },
                {
                  TableNo: '13',
                  'Team-A': '安徽    ',
                  'Score-A': '[1]  ',
                  Result: '1:0   ',
                  'Team-B': '广东    ',
                  'Score-B': '[1]  ',
                  'Name-A': ' 倪仲星 (44)',
                  'Name-B': ' 黄立勤 (4) '
                },
                {
                  TableNo: '14',
                  'Team-A': '江苏    ',
                  'Score-A': '[1]  ',
                  Result: '1:0   ',
                  'Team-B': '金融体协',
                  'Score-B': '[1]  ',
                  'Name-A': ' 张轶峰 (7) ',
                  'Name-B': ' 黄一峰 (42)'
                },
                {
                  TableNo: '15',
                  'Team-A': '浙江    ',
                  'Score-A': '[1]  ',
                  Result: '1:0   ',
                  'Team-B': '云南    ',
                  'Score-B': '[1]  ',
                  'Name-A': ' 张纪国 (17)',
                  'Name-B': ' 胡龙 (48)  '
                },
                {
                  TableNo: '16',
                  'Team-A': '河南    ',
                  'Score-A': '[1]  ',
                  Result: '0:1   ',
                  'Team-B': '河南    ',
                  'Score-B': '[1]  ',
                  'Name-A': ' 刘超 (21)  ',
                  'Name-B': ' 张文博 (54)'
                },
                {
                  TableNo: '17',
                  'Team-A': '北京    ',
                  'Score-A': '[1]  ',
                  Result: '0:1   ',
                  'Team-B': '辽宁    ',
                  'Score-B': '[1]  ',
                  'Name-A': ' 杨听雨 (58)',
                  'Name-B': ' 谭鑫麟 (22)'
                },
                {
                  TableNo: '18',
                  'Team-A': '内蒙古  ',
                  'Score-A': '[1]  ',
                  Result: '1:0   ',
                  'Team-B': '湖北    ',
                  'Score-B': '[1]  ',
                  'Name-A': ' 蒙杰焕 (23)',
                  'Name-B': ' 詹力洋 (55)'
                },
                {
                  TableNo: '19',
                  'Team-A': '福建    ',
                  'Score-A': '[1]  ',
                  Result: '1:0   ',
                  'Team-B': '陕西    ',
                  'Score-B': '[1]  ',
                  'Name-A': ' 谢增忠 (25)',
                  'Name-B': ' 岳永剑 (56)'
                },
                {
                  TableNo: '20',
                  'Team-A': '浙江    ',
                  'Score-A': '[1]  ',
                  Result: '1:0   ',
                  'Team-B': '福建    ',
                  'Score-B': '[1]  ',
                  'Name-A': ' 樊星岑 (27)',
                  'Name-B': ' 姚兆贤 (57)'
                },
                {
                  TableNo: '21',
                  'Team-A': '江西    ',
                  'Score-A': '[1]  ',
                  Result: '0:1   ',
                  'Team-B': '青海    ',
                  'Score-B': '[1]  ',
                  'Name-A': ' 严佳俊 (60)',
                  'Name-B': ' 金东雷 (32)'
                },
                {
                  TableNo: '22',
                  'Team-A': '宁夏    ',
                  'Score-A': '[1]  ',
                  Result: '0:1   ',
                  'Team-B': '河北    ',
                  'Score-B': '[1]  ',
                  'Name-A': ' 焦中其 (33)',
                  'Name-B': ' 徐嘉琦 (61)'
                },
                {
                  TableNo: '23',
                  'Team-A': '甘肃    ',
                  'Score-A': '[1]  ',
                  Result: '0:1   ',
                  'Team-B': '重庆    ',
                  'Score-B': '[1]  ',
                  'Name-A': ' 王兰成 (69)',
                  'Name-B': ' 王骞 (34)  '
                },
                {
                  TableNo: '24',
                  'Team-A': '江苏    ',
                  'Score-A': '[1]  ',
                  Result: '1:0   ',
                  'Team-B': '河南    ',
                  'Score-B': '[1]  ',
                  'Name-A': ' 唐世祺 (37)',
                  'Name-B': ' 黄健光 (64)'
                },
                {
                  TableNo: '25',
                  'Team-A': '金融体协',
                  'Score-A': '[1]  ',
                  Result: '1:0   ',
                  'Team-B': '金融体协',
                  'Score-B': '[1]  ',
                  'Name-A': ' 施贺昕 (41)',
                  'Name-B': ' 陈卓贤 (65)'
                },
                {
                  TableNo: '26',
                  'Team-A': '广东    ',
                  'Score-A': '[0]  ',
                  Result: '1:0   ',
                  'Team-B': '福建    ',
                  'Score-B': '[0]  ',
                  'Name-A': ' 朱凯 (29)  ',
                  'Name-B': ' 吴侃 (45)  '
                },
                {
                  TableNo: '27',
                  'Team-A': '黑龙江  ',
                  'Score-A': '[0]  ',
                  Result: '1:0   ',
                  'Team-B': '青岛    ',
                  'Score-B': '[0]  ',
                  'Name-A': ' 邹佳来 (35)',
                  'Name-B': ' 王天羽 (46)'
                },
                {
                  TableNo: '28',
                  'Team-A': '金融体协',
                  'Score-A': '[0]  ',
                  Result: '1:0   ',
                  'Team-B': '青岛    ',
                  'Score-B': '[0]  ',
                  'Name-A': ' 周煜昕 (47)',
                  'Name-B': ' 王晗宇 (36)'
                },
                {
                  TableNo: '29',
                  'Team-A': '青岛    ',
                  'Score-A': '[0]  ',
                  Result: '0:1   ',
                  'Team-B': '浙江    ',
                  'Score-B': '[0]  ',
                  'Name-A': ' 李泽凯 (50)',
                  'Name-B': ' 陶俊吉 (38)'
                },
                {
                  TableNo: '30',
                  'Team-A': '甘肃    ',
                  'Score-A': '[0]  ',
                  Result: '0:1   ',
                  'Team-B': '重庆    ',
                  'Score-B': '[0]  ',
                  'Name-A': ' 李刚 (39)  ',
                  'Name-B': ' 张延頔 (53)'
                },
                {
                  TableNo: '31',
                  'Team-A': '黑龙江  ',
                  'Score-A': '[0]  ',
                  Result: '1:0   ',
                  'Team-B': '江西    ',
                  'Score-B': '[0]  ',
                  'Name-A': ' 李硕 (52)  ',
                  'Name-B': ' 周瑞磊 (40)'
                },
                {
                  TableNo: '32',
                  'Team-A': '重庆    ',
                  'Score-A': '[0]  ',
                  Result: '1:0   ',
                  'Team-B': '甘肃    ',
                  'Score-B': '[0]  ',
                  'Name-A': ' 黄小平 (43)',
                  'Name-B': ' 杨树森 (59)'
                },
                {
                  TableNo: '33',
                  'Team-A': '河南    ',
                  'Score-A': '[0]  ',
                  Result: '1:0',
                  'Team-B': null,
                  'Score-B': null,
                  'Name-A': ' 夏盛 (63)  ',
                  'Name-B': 'BYE'
                }
              ],
              singleItemList: [{ name: '五子棋', logo: l('9a5b'), flag: !0 }]
            }
          },
          watch: {},
          computed: {},
          filters: {},
          components: {},
          methods: {}
        },
        Ge = ge,
        ve = (l('036b'), Object(c['a'])(Ge, Ae, Se, !1, null, '671163cd', null)),
        Ce = ve.exports,
        Ee = {
          created: function() {
            var e, a
            ;(this.activeItemName =
              null !== (e = this.$route.query.activeItemName) && void 0 !== e ? e : '围棋'),
              (this.sportFileName =
                null !== (a = this.$route.query.sportFileName) && void 0 !== a ? a : '')
          },
          mounted: function() {},
          data: function() {
            return { sportFileName: '对阵表', activeItemName: '围棋' }
          },
          watch: {},
          computed: Object(m['a'])({}, Object(o['b'])({})),
          filters: {},
          components: {
            GO: N,
            CH: p,
            CC: C,
            DR: W,
            BR: P,
            GB: $,
            resultGO: X,
            resultCH: ae,
            resultCC: se,
            resultDR: Ne,
            resultBR: pe,
            resultGB: Ce
          },
          methods: {}
        },
        Ue = Ee,
        fe = (l('1351'), Object(c['a'])(Ue, t, n, !1, null, '5ece8f4e', null))
      a['default'] = fe.exports
    },
    '65a0': function(e, a, l) {},
    '670b': function(e, a, l) {
      'use strict'
      var t = l('44dd'),
        n = l('39b7'),
        m = l('4340'),
        o = l('7f2e'),
        s = l('e5e5'),
        i = function(e) {
          if (e && e.forEach !== o)
            try {
              s(e, 'forEach', o)
            } catch (a) {
              e.forEach = o
            }
        }
      for (var r in n) n[r] && i(t[r] && t[r].prototype)
      i(m)
    },
    '6dac': function(e, a, l) {},
    '79ec': function(e, a, l) {
      e.exports = l.p + 'img/DR.af50492c.png'
    },
    '7f2e': function(e, a, l) {
      'use strict'
      var t = l('d871').forEach,
        n = l('f8e4'),
        m = n('forEach')
      e.exports = m
        ? [].forEach
        : function(e) {
            return t(this, e, arguments.length > 1 ? arguments[1] : void 0)
          }
    },
    '8a72': function(e, a, l) {},
    '97a5': function(e, a, l) {
      'use strict'
      var t = l('50c2'),
        n = l('d871').filter,
        m = l('0c94'),
        o = m('filter')
      t(
        { target: 'Array', proto: !0, forced: !o },
        {
          filter: function(e) {
            return n(this, e, arguments.length > 1 ? arguments[1] : void 0)
          }
        }
      )
    },
    '9a5b': function(e, a, l) {
      e.exports = l.p + 'img/GB.89136bb9.png'
    },
    '9a84': function(e, a, l) {
      'use strict'
      l('1baf')
    },
    '9ca9': function(e, a, l) {
      'use strict'
      l('b000')
    },
    a6ae: function(e, a, l) {
      'use strict'
      var t = l('50c2'),
        n = l('64f5'),
        m = l('85ca'),
        o = l('2412').f,
        s = l('ee9c'),
        i =
          !s ||
          n(function() {
            o(1)
          })
      t(
        { target: 'Object', stat: !0, forced: i, sham: !s },
        {
          getOwnPropertyDescriptor: function(e, a) {
            return o(m(e), a)
          }
        }
      )
    },
    a6e4: function(e, a, l) {
      'use strict'
      l('e4aa')
    },
    a864: function(e, a, l) {
      'use strict'
      l('4989')
    },
    a8d9: function(e, a, l) {
      e.exports = l.p + 'img/BR.20e66c9b.png'
    },
    ad65: function(e, a, l) {},
    b000: function(e, a, l) {},
    b604: function(e, a, l) {
      'use strict'
      l('e2f2')
    },
    b8f8: function(e, a, l) {
      'use strict'
      var t = l('ee9c'),
        n = l('e189').EXISTS,
        m = l('60ed'),
        o = l('87f2'),
        s = Function.prototype,
        i = m(s.toString),
        r = /function\b(?:\s|\/\*[\S\s]*?\*\/|\/\/[^\n\r]*[\n\r]+)*([^\s(/]*)/,
        u = m(r.exec),
        c = 'name'
      t &&
        !n &&
        o(s, c, {
          configurable: !0,
          get: function() {
            try {
              return u(r, i(this))[1]
            } catch (e) {
              return ''
            }
          }
        })
    },
    c6de0: function(e, a, l) {
      e.exports = l.p + 'img/CH.c1a22f74.png'
    },
    d054: function(e, a, l) {
      'use strict'
      l('da11')
    },
    da11: function(e, a, l) {},
    e2f2: function(e, a, l) {},
    e4aa: function(e, a, l) {},
    e620: function(e, a, l) {},
    edb5: function(e, a, l) {
      'use strict'
      l.d(a, 'a', function() {
        return m
      })
      l('1ecb'), l('ad63'), l('97a5'), l('5dd6'), l('a6ae'), l('fb9e'), l('670b'), l('2674')
      var t = l('4ba1')
      function n(e, a) {
        var l = Object.keys(e)
        if (Object.getOwnPropertySymbols) {
          var t = Object.getOwnPropertySymbols(e)
          a &&
            (t = t.filter(function(a) {
              return Object.getOwnPropertyDescriptor(e, a).enumerable
            })),
            l.push.apply(l, t)
        }
        return l
      }
      function m(e) {
        for (var a = 1; a < arguments.length; a++) {
          var l = null != arguments[a] ? arguments[a] : {}
          a % 2
            ? n(Object(l), !0).forEach(function(a) {
                Object(t['a'])(e, a, l[a])
              })
            : Object.getOwnPropertyDescriptors
            ? Object.defineProperties(e, Object.getOwnPropertyDescriptors(l))
            : n(Object(l)).forEach(function(a) {
                Object.defineProperty(e, a, Object.getOwnPropertyDescriptor(l, a))
              })
        }
        return e
      }
    },
    fa04: function(e, a, l) {}
  }
])
